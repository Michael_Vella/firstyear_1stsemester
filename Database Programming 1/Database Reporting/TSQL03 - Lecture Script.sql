/*
	MCAST IICT
	IICT6001 - Database Programming I
	T-SQL03 - JOINS
*/

USE [MCAST];
GO

-- List categories with respective products
SELECT	cat.category_name, prd.product_name
FROM	[TSQL03].[Category] cat
		JOIN [TSQL03].[Product] prd
		ON (prd.category_id=cat.category_id);
GO

-- Revise the previous to list all categories, even those without products
SELECT	cat.category_name, prd.product_name
FROM	[TSQL03].[Category] cat
		LEFT OUTER JOIN [TSQL03].[Product] prd
		ON (prd.category_id=cat.category_id);
GO

-- Revise the previous statement but invert the sequence of tables
SELECT	cat.category_name, prd.product_name
FROM	[TSQL03].[Product] prd
		RIGHT OUTER JOIN [TSQL03].[Category] cat
		ON (prd.category_id=cat.category_id);
GO

-- List all products bought by persons
SELECT	prd.product_name, uac.account_username
FROM	[TSQL03].[UserAccount] uac JOIN [TSQL03].[Order] ord ON (uac.account_id=ord.account_id)
		JOIN [TSQL03].[OrderItem] otm ON (ord.order_id=otm.order_id)
		JOIN [TSQL03].[Product] prd ON (otm.product_id=prd.product_id);
GO

-- Revise the previous to list all persons even those who did not place an order
SELECT	prd.product_name, uac.account_username
FROM	[TSQL03].[UserAccount] uac LEFT OUTER JOIN [TSQL03].[Order] ord ON (uac.account_id=ord.account_id)
		LEFT OUTER JOIN [TSQL03].[OrderItem] otm ON (ord.order_id=otm.order_id)
		LEFT OUTER JOIN [TSQL03].[Product] prd ON (otm.product_id=prd.product_id);
GO

-- Revise the previous such that only 1 outer join is used
SELECT	prd.product_name, uac.account_username
FROM	[TSQL03].[Product] prd JOIN [TSQL03].[OrderItem] otm ON (prd.product_id=otm.product_id)
		JOIN [TSQL03].[Order] ord ON (otm.order_id=ord.order_id)
		RIGHT OUTER JOIN [TSQL03].[UserAccount] uac ON (ord.account_id=uac.account_id);
GO

-- Revise the previous such that all persons and all products are listed
SELECT	prd.product_name, uac.account_username
FROM	[TSQL03].[Product] prd LEFT OUTER JOIN [TSQL03].[OrderItem] otm ON (prd.product_id=otm.product_id)
		LEFT OUTER JOIN [TSQL03].[Order] ord ON (otm.order_id=ord.order_id)
		FULL OUTER JOIN [TSQL03].[UserAccount] uac ON (ord.account_id=uac.account_id);
GO

-- List all categories and respective parent categories
SELECT	cat.category_id AS 'Child Category ID', cat.category_name AS 'Child Category'
		, cat.parent_category_id AS 'Child Parent Category ID', par.category_id AS 'Parent ID'
		, par.category_name AS 'Parent Category'
FROM	[TSQL03].[Category] cat JOIN [TSQL03].[Category] par ON (cat.parent_category_id=par.category_id);
GO

-- Revise the previous statement to list all categories as children
SELECT	cat.category_id AS 'Child Category ID', cat.category_name AS 'Child Category'
		, cat.parent_category_id AS 'Child Parent Category ID', par.category_id AS 'Parent ID'
		, par.category_name AS 'Parent Category'
FROM	[TSQL03].[Category] cat LEFT OUTER JOIN [TSQL03].[Category] par ON (cat.parent_category_id=par.category_id);
GO

-- Revise the previous statement to list all categories as parents
SELECT	cat.category_id AS 'Child Category ID', cat.category_name AS 'Child Category'
		, cat.parent_category_id AS 'Child Parent Category ID', par.category_id AS 'Parent ID'
		, par.category_name AS 'Parent Category'
FROM	[TSQL03].[Category] cat RIGHT OUTER JOIN [TSQL03].[Category] par ON (cat.parent_category_id=par.category_id);
GO

-- Revise the previous to list all categories as children and parents
SELECT	cat.category_id AS 'Child Category ID', cat.category_name AS 'Child Category'
		, cat.parent_category_id AS 'Child Parent Category ID', par.category_id AS 'Parent ID'
		, par.category_name AS 'Parent Category'
FROM	[TSQL03].[Category] cat FULL OUTER JOIN [TSQL03].[Category] par ON (cat.parent_category_id=par.category_id);
GO

-- List the percentage discount entitled per order
SELECT	ord.order_total, dsc.minimum_amount, dsc.maximum_amount, dsc.discount_percentage
FROM	[TSQL03].[Order] ord JOIN [TSQL03].[Discount] dsc ON (ord.order_total BETWEEN dsc.minimum_amount AND dsc.maximum_amount);
GO

-- Pair each product with each category
SELECT	prd.product_name, prd.category_id AS 'Product category id', cat.category_id AS 'Category ID', cat.category_name
FROM	[TSQL03].[Product] prd CROSS JOIN [TSQL03].[Category] cat;
GO