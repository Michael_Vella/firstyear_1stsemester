/*
	MCAST IICT
	IICT6001 - Database Programming I
	T-SQL07 - Creating and altering a table
*/

USE [master];
GO

CREATE DATABASE [MCAST];
GO

USE [MCAST];
GO

-- Clean-up
DROP TABLE [TSQL07].[userHistory];
DROP TABLE [TSQL07].[job];
DROP TABLE [TSQL07].[department];
DROP TABLE [TSQL07].[userProfile];
DROP TABLE [TSQL07].[userAccount];
GO

DROP SCHEMA [TSQL07];
GO

-- Creation
CREATE SCHEMA [TSQL07];
GO

CREATE TABLE [TSQL07].[department] (
	departmentId UNIQUEIDENTIFIER
		CONSTRAINT dep_pk PRIMARY KEY
		DEFAULT NEWID()
	, departmentName NVARCHAR(256) NOT NULL
		CONSTRAINT dep_dnm_un UNIQUE
);

CREATE TABLE [TSQL07].[job] (
	jobId UNIQUEIDENTIFIER
		CONSTRAINT job_pk PRIMARY KEY
		DEFAULT NEWID()
	, jobTitle NVARCHAR(256) NOT NULL
		CONSTRAINT job_tle_un UNIQUE
	, departmentId UNIQUEIDENTIFIER NOT NULL
		CONSTRAINT job_did_fk REFERENCES [TSQL07].[department] (departmentID)
);

CREATE TABLE [TSQL07].[userAccount] (
	accountId UNIQUEIDENTIFIER
		CONSTRAINT uac_pk PRIMARY KEY
		DEFAULT NEWID()
	, accountUsername NVARCHAR(256) NOT NULL
		CONSTRAINT uac_unm_un UNIQUE
	, accountPassword NVARCHAR(256) NOT NULL
	, recoveryEmail NVARCHAR(256) NOT NULL
);

CREATE TABLE [TSQL07].[userProfile] (
	profileId UNIQUEIDENTIFIER
		CONSTRAINT upr_pk PRIMARY KEY
		DEFAULT NEWID()
	, firstName NVARCHAR(256) NOT NULL
	, lastName NVARCHAR(256) NOT NULL
	, gender CHAR(1) NOT NULL
	, dateOfBirth DATE NOT NULL
	, avatar NVARCHAR(3058) DEFAULT NULL
	, accountId UNIQUEIDENTIFIER NOT NULL
		CONSTRAINT upr_aid_fk REFERENCES [TSQL07].[userAccount] (accountId)
		CONSTRAINT upr_aid_un UNIQUE
);

CREATE TABLE [TSQL07].[userHistory] (
	profileId UNIQUEIDENTIFIER
		CONSTRAINT ujh_pid_fk REFERENCES [TSQL07].[userProfile] (profileId)
	, jobId UNIQUEIDENTIFIER
		CONSTRAINT ujh_jid_fk REFERENCES [TSQL07].[job] (jobId)
	, fromDate DATE NOT NULL
	, toDate DATE DEFAULT NULL
	, CONSTRAINT ujh_pk PRIMARY KEY (profileId,jobId)
);

-- Alteration
ALTER TABLE [TSQL07].[userAccount]
ADD CONSTRAINT uac_eml_un UNIQUE (recoveryEmail);
GO

-- Population
INSERT INTO [TSQL07].[department] (departmentName)
VALUES ('IT'), ('HR');
GO

INSERT INTO [TSQL07].[job] (jobTitle, departmentId)
VALUES ('Developer', 
			(SELECT	departmentId
			FROM	[TSQL07].[department]
			WHERE	departmentName='IT'))
		, ('IT Administrator', 
			(SELECT	departmentId
			FROM	[TSQL07].[department]
			WHERE	departmentName='IT'))
		, ('HR Clerk', 
			(SELECT	departmentId
			FROM	[TSQL07].[department]
			WHERE	departmentName='HR'))
		, ('HR Manager', 
			(SELECT	departmentId
			FROM	[TSQL07].[department]
			WHERE	departmentName='HR'));
GO

-- Reporting
SELECT	*
FROM	[TSQL07].[department];
GO

SELECT	departmentId
FROM	[TSQL07].[department]
WHERE	departmentName='IT';
GO

SELECT	dep.departmentName, job.jobTitle
FROM	[TSQL07].[department] dep JOIN [TSQL07].[job] job
		ON (job.departmentId=dep.departmentId)
ORDER BY 1 ASC, 2 ASC;