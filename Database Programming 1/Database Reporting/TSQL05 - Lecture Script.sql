/*
	MCAST IICT
	IICT6001 - Database Programming I
	T-SQL 05 - Aggregate Data
*/

USE [AdventureWorks2014];
GO

-- List all persons
SELECT	*
FROM	[Person].[Person];
GO -- 19,972 rows

-- Display the total number of persons
SELECT	COUNT(*)
FROM	[Person].[Person];
GO

-- Revise the previous to use the PK instead of *
SELECT	COUNT(*)
		, COUNT(BusinessEntityID)
FROM	[Person].[Person];
GO

-- Also list the total number of middle names
SELECT	COUNT(*)
		, COUNT(BusinessEntityID)
		, COUNT(MiddleName)
FROM	[Person].[Person];
GO

-- Revise the previous to display the total number of unique middle names
SELECT	COUNT(*)
		, COUNT(BusinessEntityID)
		, COUNT(MiddleName)
		, COUNT(DISTINCT MiddleName)
FROM	[Person].[Person];
GO

-- Display the total number of persons for each surname
SELECT	LastName
FROM	[Person].[Person]
ORDER BY 1 ASC;
GO

SELECT	LastName
		, COUNT(*) AS 'Total Persons'
FROM	[Person].[Person]
GROUP BY LastName 
ORDER BY 1 ASC;
GO

-- Before aggregating data, filter the list to consider only surnames Abel, Adams, Barker, Beck
SELECT	LastName
		, COUNT(*) AS 'Total Persons'
FROM	[Person].[Person]
WHERE	LastName IN ('Abel', 'Adams', 'Barker', 'Beck')
GROUP BY LastName 
ORDER BY 1 ASC;
GO

-- After aggregating data filter the list to consider only those surnames with more than 10 persons
SELECT	LastName
		, COUNT(*) AS 'Total Persons'
FROM	[Person].[Person]
WHERE	LastName IN ('Abel', 'Adams', 'Barker', 'Beck')
GROUP BY LastName 
HAVING	COUNT(*) > 10
ORDER BY 1 ASC;
GO

-- Calculate the total sales for the last year per territory group and country region code
SELECT	[Group], [CountryRegionCode], SalesLastYear
FROM	[Sales].[SalesTerritory]
ORDER BY 1,2,3;
GO

SELECT	[Group], [CountryRegionCode], SUM(SalesLastYear) AS 'Total Sales Last Year'
FROM	[Sales].[SalesTerritory]
GROUP BY [Group], [CountryRegionCode];
GO

-- List the total sales for each group, then the global total
SELECT	[Group], SUM(SalesLastYear) AS 'Total Sales Last Year'
FROM	[Sales].[SalesTerritory]
GROUP BY [Group];
GO

SELECT	SUM(SalesLastYear) AS 'Total Sales Last Year'
FROM	[Sales].[SalesTerritory];
GO

-- Merge the last three reports together
SELECT	[Group], [CountryRegionCode], SUM(SalesLastYear) AS 'Total Sales Last Year'
FROM	[Sales].[SalesTerritory]
GROUP BY ROLLUP([Group], [CountryRegionCode]);
GO

-- Replace the previous report with the power set
SELECT	[Group], [CountryRegionCode], SUM(SalesLastYear) AS 'Total Sales Last Year'
FROM	[Sales].[SalesTerritory]
GROUP BY CUBE([Group], [CountryRegionCode]);
GO

-- Replace the previous and provide groupings for Group then country only
SELECT	[Group], [CountryRegionCode], SUM(SalesLastYear) AS 'Total Sales Last Year'
FROM	[Sales].[SalesTerritory]
GROUP BY GROUPING SETS(([Group]), ([CountryRegionCode]));
GO

-- Add an indication on which attribute is being used for grouping
SELECT	[Group], [CountryRegionCode], SUM(SalesLastYear) AS 'Total Sales Last Year'
		, GROUPING([Group]) AS 'Group', GROUPING([CountryRegionCode]) AS 'CountryRegionCode'
FROM	[Sales].[SalesTerritory]
GROUP BY ROLLUP([Group], [CountryRegionCode]);
GO

-- Revise to use grouping id
SELECT	[Group], [CountryRegionCode], SUM(SalesLastYear) AS 'Total Sales Last Year'
		, GROUPING_ID([Group], [CountryRegionCode]) AS 'Grouping'
FROM	[Sales].[SalesTerritory]
GROUP BY ROLLUP([Group], [CountryRegionCode]);
GO

SELECT	[Group], [CountryRegionCode], SUM(SalesLastYear) AS 'Total Sales Last Year'
		, GROUPING_ID([Group], [CountryRegionCode]) AS 'Grouping'
FROM	[Sales].[SalesTerritory]
GROUP BY CUBE([Group], [CountryRegionCode]);
GO