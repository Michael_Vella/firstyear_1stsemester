/*
	MCAST IICT
	IICT6001 - Database Programming I
	T-SQL 06 - Advanced Aggregation and Ranking Functions
*/

USE [AdventureWorks2014];
GO

-- List all first names
SELECT	FirstName
FROM	[Person].[Person]
ORDER BY 1 ASC;
GO

-- Revise the previous to retrieve only the first 50 rows
SELECT	TOP 50 FirstName
FROM	[Person].[Person]
ORDER BY 1 ASC;
GO

-- Revise the previous to retrieve all subsequent entries that match the last, 50th row.
SELECT	TOP 50 WITH TIES FirstName
FROM	[Person].[Person]
ORDER BY 1 ASC;
GO

SELECT	TOP 60 FirstName
FROM	[Person].[Person]
ORDER BY 1 ASC;
GO

-- Wrap the previous in a CTE
WITH AllNames (FirstName)
AS (SELECT	TOP 60 FirstName
	FROM	[Person].[Person])
SELECT	*
FROM	AllNames;
GO

-- List all products and their list price, sort in descending order via list price
SELECT	Name, ListPrice
FROM	[Production].[Product]
ORDER BY ListPrice DESC;
GO

-- Add a new column with the row number based on the list price in descending order
SELECT	Name, ListPrice,
		ROW_NUMBER() OVER (ORDER BY ListPrice DESC)
FROM	[Production].[Product]
ORDER BY ListPrice DESC;
GO

-- Revise the previous to sort in ascending order instead
SELECT	Name, ListPrice,
		ROW_NUMBER() OVER (ORDER BY ListPrice DESC)
FROM	[Production].[Product]
ORDER BY ListPrice ASC, 3 DESC;
GO

-- Instead display the ranking per product based on list price
SELECT	Name, ListPrice,
		RANK() OVER (ORDER BY ListPrice DESC)
FROM	[Production].[Product]
ORDER BY ListPrice DESC;
GO

-- Revise the previous so not to skip any rank position
SELECT	Name, ListPrice,
		DENSE_RANK() OVER (ORDER BY ListPrice DESC)
FROM	[Production].[Product]
ORDER BY ListPrice DESC;
GO

-- Group all products in 5 equal groups based on list price
SELECT	Name, ListPrice,
		NTILE(5) OVER (ORDER BY ListPrice DESC)
FROM	[Production].[Product]
ORDER BY ListPrice DESC;
GO

-- Wrap the previous in a CTE
WITH ProductPrices (ProductName, ProductPrice, RowNum)
AS (SELECT	Name, ListPrice,
			ROW_NUMBER() OVER (ORDER BY ListPrice DESC)
	FROM	[Production].[Product])
SELECT	ProductName, ProductPrice, RowNum
FROM	ProductPrices
ORDER BY RowNum ASC;
GO

-- Revise the previous to list the second page of a 5 row limit per page report
WITH ProductPrices (ProductName, ProductPrice, RowNum)
AS (SELECT	Name, ListPrice,
			ROW_NUMBER() OVER (ORDER BY ListPrice DESC)
	FROM	[Production].[Product])
SELECT	ProductName, ProductPrice, RowNum
FROM	ProductPrices
ORDER BY RowNum ASC
		OFFSET 5 ROWS FETCH NEXT 5 ROWS ONLY;
GO

-- List all vendors, the year when a purchase was made and, order quantity per purchase
SELECT	vnd.name, YEAR(poh.OrderDate), pod.OrderQty
FROM	[Purchasing].[Vendor] vnd
		JOIN [Purchasing].[PurchaseOrderHeader] poh
		ON (vnd.BusinessEntityID=poh.VendorID)
		JOIN [Purchasing].[PurchaseOrderDetail] pod
		ON (poh.PurchaseOrderID=pod.PurchaseOrderID)
ORDER BY 1, 2, 3;
GO

-- Wrap the previous in a CTE and display the different purchase years
WITH VendorPurchases (VendorName, YearOfPurchase, OrderQty)
AS (SELECT	vnd.name, YEAR(poh.OrderDate), pod.OrderQty
	FROM	[Purchasing].[Vendor] vnd
			JOIN [Purchasing].[PurchaseOrderHeader] poh
			ON (vnd.BusinessEntityID=poh.VendorID)
			JOIN [Purchasing].[PurchaseOrderDetail] pod
			ON (poh.PurchaseOrderID=pod.PurchaseOrderID))
SELECT	DISTINCT YearOfPurchase
FROM	VendorPurchases;
GO

-- Pivot the previous two reports
WITH VendorPurchases (VendorName, YearOfPurchase, OrderQty)
AS (SELECT	vnd.name, YEAR(poh.OrderDate), pod.OrderQty
	FROM	[Purchasing].[Vendor] vnd
			JOIN [Purchasing].[PurchaseOrderHeader] poh
			ON (vnd.BusinessEntityID=poh.VendorID)
			JOIN [Purchasing].[PurchaseOrderDetail] pod
			ON (poh.PurchaseOrderID=pod.PurchaseOrderID))
SELECT	*
FROM	VendorPurchases
		PIVOT (SUM(OrderQty) FOR YearOfPurchase IN ([2011],[2012],[2013], [2014])) AS pvt
ORDER BY 1 ASC;
GO

-- Wrap the pivot report in a cte
WITH VendorPurchases (VendorName, YearOfPurchase, OrderQty)
AS (SELECT	vnd.name, YEAR(poh.OrderDate), pod.OrderQty
	FROM	[Purchasing].[Vendor] vnd
			JOIN [Purchasing].[PurchaseOrderHeader] poh
			ON (vnd.BusinessEntityID=poh.VendorID)
			JOIN [Purchasing].[PurchaseOrderDetail] pod
			ON (poh.PurchaseOrderID=pod.PurchaseOrderID))
, PivotReport (VendorName, [2011], [2012], [2013], [2014])
AS (SELECT	*
	FROM	VendorPurchases
			PIVOT (SUM(OrderQty) FOR YearOfPurchase IN ([2011],[2012],[2013], [2014])) AS pvt)
SELECT	*
FROM	PivotReport;

-- Unpivot the last report
WITH VendorPurchases (VendorName, YearOfPurchase, OrderQty)
AS (SELECT	vnd.name, YEAR(poh.OrderDate), pod.OrderQty
	FROM	[Purchasing].[Vendor] vnd
			JOIN [Purchasing].[PurchaseOrderHeader] poh
			ON (vnd.BusinessEntityID=poh.VendorID)
			JOIN [Purchasing].[PurchaseOrderDetail] pod
			ON (poh.PurchaseOrderID=pod.PurchaseOrderID))
, PivotReport (VendorName, [2011], [2012], [2013], [2014])
AS (SELECT	*
	FROM	VendorPurchases
			PIVOT (SUM(OrderQty) FOR YearOfPurchase IN ([2011],[2012],[2013], [2014])) AS pvt)
SELECT	*
FROM	PivotReport
		UNPIVOT(UnitBought FOR YearOfPurchase IN ([2011],[2012],[2013],[2014])) AS unpvt
ORDER BY 1, 3;
GO