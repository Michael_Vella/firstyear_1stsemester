/*
	MCAST IICT
	IICT6001 - Database Programming I
	T-SQL 08 - Insert, Update, DELETE and Merge data
*/

USE [MCAST];
GO

-- Population
INSERT INTO [TSQL08].[person] (FirstName, LastName, IdCard)
VALUES	('Joe', 'Borg', '1M')
		, ('Lisa', 'Abela', '2M')
		, ('Tony', 'Galea', '3M')
		, ('Ian', 'Zammit', '4M')
		, ('Sarah', 'Tanti', '5M');
GO

INSERT INTO [TSQL08].[Book] (BookTitle, BookISBN)
VALUES ('Animal Farm' , '1')
		, ('Da Vinci Code', '2')
		, ('The Lost Symbol', '3')
		, ('The Lost World', '4')
		, ('Air Force One', '5')
		, ('The Girl With The Dragon Tatoo', '6');
GO

INSERT INTO [TSQL08].[BookTransactionHistory]
	(TakenDate, DueDate, ReturnDate, PersonId, BookId)
VALUES 
	(DATEADD(day,-7, GETDATE()), DATEADD(day,-5, GETDATE()), DATEADD(day,-5, GETDATE()),
	(SELECT personId FROM [TSQL08].[Person] WHERE IdCard='1M'),
	(SELECT bookId FROM [TSQL08].[Book] WHERE BookISBN='1')),
	(DATEADD(day,-7, GETDATE()), DATEADD(day,-5, GETDATE()), DATEADD(day,-5, GETDATE()),
	(SELECT personId FROM [TSQL08].[Person] WHERE IdCard='1M'),
	(SELECT bookId FROM [TSQL08].[Book] WHERE BookISBN='2')),
	(DATEADD(day,-5, GETDATE()), DATEADD(day,-3, GETDATE()), DATEADD(day,-3, GETDATE()),
	(SELECT personId FROM [TSQL08].[Person] WHERE IdCard='2M'),
	(SELECT bookId FROM [TSQL08].[Book] WHERE BookISBN='1')),
	(DATEADD(day,-1, GETDATE()), DATEADD(day,1, GETDATE()), NULL,
	(SELECT personId FROM [TSQL08].[Person] WHERE IdCard='3M'),
	(SELECT bookId FROM [TSQL08].[Book] WHERE BookISBN='2')),
	(DATEADD(day,-1, GETDATE()), DATEADD(day,1, GETDATE()), NULL,
	(SELECT personId FROM [TSQL08].[Person] WHERE IdCard='3M'),
	(SELECT bookId FROM [TSQL08].[Book] WHERE BookISBN='3'));
GO

INSERT INTO [TSQL08].[RentalInbox]
	(BookISBN, PersonIDCard, IsReturn, TakenDate, DueDate, ReturnDate)
VALUES 
	('2', '3M', 1, DATEADD(day,-1, GETDATE()), DATEADD(day,1, GETDATE()), GETDATE())
	, ('4', '4M', 0, GETDATE(), DATEADD(day, 2, GETDATE()), NULL);
GO

-- Merge
WITH FullRentalInbox (BookId,BookIsbn,PersonId, IDcard, TakenDate, DueDate, ReturnDate, IsReturn)
AS (SELECT	bok.BookID, bok.BookISBN,
			per.personId, per.IDCard,
			rin.TakenDate, rin.DueDate, rin.ReturnDate, rin.IsReturn
	FROM	[TSQL08].[Book] bok
			JOIN [TSQL08].[RentalInbox] rin
			ON (bok.BookISBN=rin.BookISBN)
			JOIN [TSQL08].[Person] per
			ON (rin.PersonIDCard=per.IDCard))

MERGE	[TSQL08].[BookTransactionHistory] trh
		USING FullRentalInbox rin
		ON (trh.personId=rin.PersonId AND trh.BookId=rin.Bookid
			AND trh.ReturnDate IS NULL AND rin.IsReturn=1)
		WHEN MATCHED THEN
			UPDATE SET trh.ReturnDate=rin.ReturnDate
		WHEN NOT MATCHED AND rin.IsReturn=0 THEN
			INSERT (BookId, PersonId, TakenDate, DueDate)
			VALUES (rin.BookId, rin.PersonId, rin.TakenDate, rin.DueDate);
GO

DELETE FROM [TSQL08].[RentalInbox];
GO

UPDATE [TSQL08].[BookTransactionHistory]
SET ReturnDate=GETDATE()
WHERE PersonID = (SELECT PerSonId FROM [TSQL08].[Person] WHERE IdCard='4M');
GO

-- Reporting
SELECT	*
FROM	[TSQL08].[Person];
GO

SELECT	*
FROM	[TSQL08].[Book];
GO

SELECT	GETDATE()
		, DATEADD(day, 2, GETDATE());
GO

SELECT	bok.BookTitle, bok.BookISBN,
		bth.TakenDate, bth.DueDate, bth.ReturnDate, 
		per.LastName, per.FirstName, per.IDCard
FROM	[TSQL08].[Person] per 
		JOIN [TSQL08].[BookTransactionHistory] bth
		ON (per.PersonID=bth.PersonID)
		JOIN [TSQL08].[Book] bok
		ON (bth.BookID=bok.BookID)
ORDER BY bth.TakenDate ASC, bth.DueDate ASC, bok.BookTitle, per.LastName;
GO

SELECT	bok.BookID, bok.BookISBN,
		per.personId, per.IDCard,
		rin.TakenDate, rin.DueDate, rin.ReturnDate
FROM	[TSQL08].[Book] bok
		JOIN [TSQL08].[RentalInbox] rin
		ON (bok.BookISBN=rin.BookISBN)
		JOIN [TSQL08].[Person] per
		ON (rin.PersonIDCard=per.IDCard);
GO

SELECT * FROM [TSQL08].[RentalInbox];
GO