/*
	MCAST IICT
	IICT6001 - Database Programming I
	T-SQL04 - Built In Functions
*/

USE [AdventureWorks2014];
GO

-- MSDN example for upper
SELECT UPPER(RTRIM(LastName)) + ', ' + FirstName AS Name
FROM Person.Person
ORDER BY LastName;
GO

-- List all names in lower and upper case
SELECT	FirstName AS 'Original', LOWER(FirstName) AS 'Lower', UPPER(FirstName) AS 'Upper'
FROM	[Person].[Person];
GO

-- MSDN example for left
SELECT LEFT('abcdefg',2)
GO

-- Extract the first and last 2 characters for all names
SELECT	FirstName AS 'Original', LEFT(FirstName, 2) AS 'Left', RIGHT(FirstName, 2) AS 'Right'
FROM	[Person].[Person];
GO

-- Reverse the sequence of all names
SELECT	FirstName AS 'Original', REVERSE(FirstName) AS 'Once', REVERSE(REVERSE(FirstName)) AS 'Twice'
FROM	[Person].[Person];
GO

-- Concatenate the name, middle and last names
SELECT	FirstName, MiddleName, LastName,
		FirstName + ' ' + MiddleName + ' ' + LastName AS 'No Functions',
		CONCAT(FirstName, ' ', MiddleName, ' ', LastName) AS 'Function'
FROM	[Person].[Person];
GO

-- Revise the previous to remove double spaces
SELECT	FirstName, MiddleName, LastName,
		FirstName + ' ' + MiddleName + ' ' + LastName AS 'No Functions',
		CONCAT(FirstName, ' ', MiddleName, ' ', LastName) AS 'Function',
		REPLACE(CONCAT(FirstName, ' ', MiddleName, ' ', LastName), '  ', ' ') AS 'Cleaned'
FROM	[Person].[Person];
GO

-- MSDN example for ceiling
SELECT CEILING($123.45), CEILING($123.55), CEILING($-123.45), CEILING($-123.55), CEILING($0.0)
GO

-- Raise 5 to the power of 2, 3 then 4
SELECT	POWER(5, 2) AS 'Squared', POWER(5,3) AS 'Cubed', POWER(5, 4) AS 'Power 4';
GO

-- Check the sign of various values
SELECT	SIGN(-50) AS 'Negative', SIGN(0) AS 'Zero', SIGN(50) AS 'Positive';
GO

-- Round some values
SELECT	ROUND(4592.45, 2), ROUND(4592.45, 1), ROUND(4592.45, 0), ROUND(4592.45, -1), ROUND(4592.45, -2);
GO

SELECT	ROUND(4592.45, 2, 1), ROUND(4592.45, 1, 1), ROUND(4592.45, 0, 1), ROUND(4592.45, -1, 1), ROUND(4592.45, -2, 1);
GO

-- Display the current system time, then with offset, then UTC time
SELECT	SYSDATETIME(), SYSDATETIMEOFFSET(), SYSUTCDATETIME();
GO

-- Extract various parts of the order date in sales order header
SELECT	OrderDate, DATEPART(year, OrderDate), YEAR(OrderDate)
		, DATEPART(quarter, OrderDate), DATEPART(month, OrderDate)
		, MONTH(OrderDate), DATEPART(day, OrderDate)
		, DAY(OrderDate)
FROM	[Sales].[SalesOrderHeader];
GO

-- Calculate the number of days from order date to ship date
SELECT	OrderDate, ShipDate, DATEDIFF(day, OrderDate, ShipDate), DATEDIFF(day, ShipDate, OrderDate)
FROM	[Sales].[SalesOrderHeader];
GO

-- Display the different user's answers
SELECT	CHOOSE(1, 'Yes', 'No', 'Maybe'), CHOOSE(2, 'Yes', 'No', 'Maybe'), CHOOSE(3, 'Yes', 'No', 'Maybe');
GO