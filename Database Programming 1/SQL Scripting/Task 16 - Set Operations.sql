/*
	IICT6001 - Database Programming 1
	Task 16 - Set Operations
*/
USE [AdventureWorks2014];
GO

--1
WITH NotOrdered (Name) AS 
( 
	SELECT Name
	FROM [Production].[Product]
	WHERE ProductID NOT IN 
					   (
						SELECT DISTINCT ProductID 
						FROM [Sales].[SalesOrderDetail]
						)
)
,AvgRate(ProdID, Rating) AS 
(
	 SELECT ProductID,
			AVG(Rating)
	 FROM [Production].[ProductReview]
	 GROUP BY ProductID
)

SELECT Name
FROM AvgRate avr
	JOIN [Production].[Product]
	ON (avr.ProdID = Product.ProductID)
WHERE avr.Rating < 5
UNION
SELECT Name
FROM NotOrdered

--2 
