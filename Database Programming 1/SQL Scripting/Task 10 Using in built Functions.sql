/*
	Task 10 - Built In Funcitons 
	
		
*/
USE [AdventureWorks2014]; 
GO
--01 
SELECT FirstName
FROM [Person].[Person] per
WHERE LEN(per.FirstName) > 5;
GO 
--02
SELECT DISTINCT
		REPLACE(FirstName,'-', ' ') as 'First Name',
		LastName as 'Last Name'
FROM 
		[Person].[Person] per
WHERE 
		CHARINDEX('-', FirstName, 1) > 0;
GO
--03
SELECT DISTINCT
		per.FirstName,
		SUBSTRING ( FirstName,1,(CHARINDEX('-', FirstName, 1) -1))AS 'first name',
		SUBSTRING ( FirstName,(CHARINDEX('-', FirstName, 1) +1), LEN(FirstName))'MIDDLE'
FROM 
		[Person].[Person] per
WHERE 
		CHARINDEX('-', FirstName, 1) > 0;
GO
--04
SELECT
		SYSDATETIME(),
		EOMONTH(SYSDATETIME());	
GO 
--05
SELECT 
		CONVERT(datetimeoffset,'yyyy-mm-ddThh:mi:ss.mmmZ');
--06
SELECT
		
FROM 
		[Sales].[SalesOrderHeader]
GO
--07

--08

--Extra Questions

--01

--02
