/*
	IICT6001 - Database Programming 
	T-SQL09 Subqueries

*/

USE [AdventureWorks2014];
GO

-- List all employees that work in the executive department
SELECT departmentId 
FROM [HumanResources].[Department]
WHERE Name = 'Executive';
GO

SELECT emp.*
FROM [HumanResources].[Employee] emp 
	 JOIN [HumanResources].[EmployeeDepartmentHistory] his
	 ON (emp.BusinessEntityID = his.BusinessEntityID)
WHERE his.DepartmentID = (
								SELECT departmentId
								FROM [HumanResources].[Department]
								WHERE Name = 'Executive'
							 );
GO -- Scalar Subquery 

--List the average list price of the products 
SELECT(AVG(pro.ListPrice))
FROM [Production].[Product] pro;
GO 
-- List those products which are priced higher 
-- than the average price
SELECT pro.*
FROM [Production].[Product] pro
WHERE pro.ListPrice > = 
		(
		SELECT(AVG(pro.ListPrice))
		FROM [Production].[Product] pro
		)
GO

--Revise the AVG query to list the average listprice for each product
-- sub categoryID 
SELECT pro.ProductSubcategoryID , AVG(pro.ListPrice)
FROM [Production].[Product] pro
GROUP BY pro.ProductSubcategoryID
ORDER BY 2 DESC;
GO

--List all the products which are priced higher than the 
-- average price of every sub category
SELECT pro.*
 FROM [Production].[Product] pro
 WHERE pro.ListPrice  > (
						SELECT pro.ProductSubcategoryID , AVG(pro.ListPrice)
						FROM [Production].[Product] pro
						GROUP BY pro.ProductSubcategoryID
						ORDER BY 2 DESC
						) 
--The above gives an error, using a scalar comparision with a mulit-row subquery

--The correct way is below

SELECT *
FROM [Production].[Product]
WHERE ListPrice >ALL 
                       (
						SELECT AVG(ListPrice)
						FROM [Production].[Product]
						GROUP BY ProductSubcategoryID
						)
ORDER BY ListPrice ASC;
GO 
--Identify the lowest average price for a sub categorty 
SELECT *
FROM [Production].[Product]
WHERE ListPrice > ALL 
					(
					SELECT AVG(ListPrice)
					FROM [Production].[Product]
					GROUP BY ProductSubcategoryID
					)
ORDER BY ListPrice ASC;
GO 
-- Then revise the previosu to display all products 
-- priced higher than any average price for a sub category 
SELECT *
FROM [Production].[Product]
WHERE ListPrice > ANY 
					(
					SELECT AVG(ListPrice)
					FROM [Production].[Product]
					GROUP BY ProductSubcategoryID
					)
ORDER BY ListPrice ASC;
GO

--List all products then list all sales order detail
SELECT *
FROM [Production].[Product];
GO 

SELECT * 
FROM [Sales].[SalesOrderDetail];
GO

-- List all products that havent been sold
SELECT * 
FROM [Production].[Product] mq
WHERE NOT EXISTS  
					(
					SELECT * 
					FROM [Sales].[SalesOrderDetail] sq
					WHERE mq.ProductID = sq.ProductID
					);
GO
--PLEASE NOTE THAT THE ABOVE CAN ACTUALLY CRASH A SYSTEM IF USED 
