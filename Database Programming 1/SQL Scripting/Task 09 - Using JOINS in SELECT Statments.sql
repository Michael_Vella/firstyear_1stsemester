/*

	IICT6001 - Database Programming I 
	Task 09 - Utilising Joins in a SELECT Statment

*/

USE [MCAST];
GO

-- Question 01
SELECT 
		
		emp.first_name,
		emp.last_name,
		jb.job_title	
FROM 
	    [Task09].[Job] jb
	    JOIN [Task09].[Employee] emp
	    ON (jb.job_id = emp.job_id);
GO

--Question 02
SELECT 
		jb.job_title,
		emp.first_name,
		emp.last_name	
FROM 
	    [Task09].[Job] jb
	    LEFT JOIN [Task09].[Employee] emp
	    ON (jb.job_id = emp.job_id);
GO

--Question 03
SELECT 
	   emp.first_name,
	   emp.last_name,
	   dep.department_name
FROM 
	   [Task09].[Employee] emp
	   LEFT OUTER JOIN [Task09].[Department] dep
	   ON (dep.manager_id = emp.employee_id)
ORDER BY
		dep.department_name DESC;
GO

--Question 04

SELECT 
	   emp.employee_id AS 'Employee ID',
	   emp.first_name AS 'Employee Name',
	   emp.last_name AS 'Employee Surname',
	   emp.manager_id AS 'Emp Mngr ID',
	   mngr.employee_id AS 'Manager ID',
	   mngr.first_name AS 'Manager Name',
	   mngr.last_name AS 'Manager Surname'
FROM 
	   [Task09].[Employee] emp
	   LEFT JOIN [Task09].[Employee] mngr
	   ON (emp.manager_id = mngr.employee_id);
	   --Using the foreign key first in order to get the details from
	   --the primary due to the recursive nature
GO

--Question 05
--Need to use the non equi join

SELECT
		emp.first_name AS 'First Name',
		emp.last_name AS 'Surname',
		emp.salary AS 'Salary',
		slry.salary_grade AS 'Pay Grade',
		slry.minimum_salary AS 'Minimum Grade Amount',
		slry.maximum_salary AS 'Maxium Grade Amount'

FROM 
		[Task09].[Employee] emp
		JOIN [Task09].[SalaryGrade] slry
		ON (emp.salary 
		BETWEEN slry.minimum_salary
		AND slry.maximum_salary); 
GO 

--Question 06
--Need to use CROSS JOIN

SELECT 
		dep.department_id AS 'Department ID',
        dep.department_name AS 'Department Name',
		job.job_title AS 'Job',
		job.department_id AS 'Job DID'

FROM 
	    [Task09].[Department] dep
	    CROSS JOIN [Task09].[Job] job
ORDER BY 
		dep.department_name ASC;	  
GO

--Extra Questions 

--01
SELECT
		emp.first_name,
		emp.last_name,
		dept.department_name,
		job.job_title
FROM 
		[Task09].[Employee] emp
		RIGHT OUTER JOIN [Task09].[Job] job
		ON (emp.job_id = job.job_id)
		FULL OUTER JOIN [Task09].[Department] dept
		on (job.department_id = dept.department_id)
ORDER BY
		dept.department_name ASC;

GO

--02
SELECT  
		 emp.first_name AS 'Employee Name'
		,col.first_name AS 'Colleauge Name'
FROM  
		[Task09].[Employee] emp
		JOIN [Task09].[Job] ejob
		ON (emp.job_id = ejob.job_id)
		JOIN [Task09].[Department] dept
		ON (ejob.department_id = dept.department_id)
		JOIN [Task09].[Job] cjob
		ON (dept.department_id = cjob.department_id)
		JOIN [Task09].[Employee] col
		ON (cjob.job_id = col.job_id 
		AND col.employee_id != emp.employee_id);
GO
--Optimised
SELECT  
		 emp.first_name AS 'Employee Name'
		,col.first_name AS 'Colleauge Name'
FROM  
		[Task09].[Employee] emp
		JOIN [Task09].[Job] ejob
		ON (emp.job_id = ejob.job_id)
		JOIN [Task09].[Job] cjob
		ON (ejob.department_id = cjob.department_id)
		JOIN [Task09].[Employee] col
		ON (cjob.job_id = col.job_id 
		AND col.employee_id != emp.employee_id);
GO

--03

--TO DO: INCOMPLETE
SELECT 
		CONCAT (emp.first_name,' ', emp.last_name) AS 'All Employees',
		CONCAT (mngr.first_name, ' ', mngr.last_name) AS 'Manager'
		--NOTE: The word 'CONCAT' combines 2 fields into 1
FROM  
		[Task09].[Employee] emp
		FULL OUTER JOIN 
		[Task09].[Employee] mngr
		ON(emp.manager_id = mngr.employee_id);
GO

-- Difference (Query Results the same)
SELECT 
		emp.first_name +' '+ emp.last_name AS 'All Employees',
		mngr.first_name + ' ' + mngr.last_name AS 'Manager'
		--NOTE: The word 'CONCAT' combines 2 fields into 1
FROM  
		[Task09].[Employee] emp
		FULL OUTER JOIN 
		[Task09].[Employee] mngr
		ON(emp.manager_id = mngr.employee_id);
GO

--04
SELECT DISTINCT 
			    dept.department_name
				,slry.salary_grade
FROM 
	 [Task09].[SalaryGrade] slry
	 JOIN 
	 [Task09].[Employee] emp
	 ON (emp.salary BETWEEN 
	     slrY.minimum_salary
		 AND slry.maximum_salary)
	JOIN [Task09].[Job] job
	ON (emp.job_id = job.job_id)
	JOIN [Task09].[Department] dept
	ON (job.department_id = dept.department_id);
GO