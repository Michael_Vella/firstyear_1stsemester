/*
	IICT6001 - Database Programming 1 
	Task 11 - Aggregate Data
*/

USE [AdventureWorks2014];
GO

--01
SELECT
	  dep.Name,
	  Count(emp.DepartmentID)
FROM 
	[HumanResources].[Department] dep
	JOIN [HumanResources].[EmployeeDepartmentHistory] emp
	ON (dep.DepartmentID = emp.DepartmentID)
GROUP BY 
		 dep.Name;
GO
--02
SELECT
	  dep.Name,
	  Count(emp.DepartmentID)
FROM 
	[HumanResources].[Department] dep
	JOIN [HumanResources].[EmployeeDepartmentHistory] emp
	ON (dep.DepartmentID = emp.DepartmentID)
GROUP BY 
		 dep.Name
HAVING 
		COUNT(emp.DepartmentID) >= 10;
GO
--03
SELECT
	dep.GroupName AS 'Division',
	dep.Name AS 'Department',
	SUM(emp.DepartmentID)
FROM 
	[HumanResources].[Department] dep
	JOIN [HumanResources].[EmployeeDepartmentHistory] emp
	ON (dep.DepartmentID = emp.DepartmentID)
GROUP BY ROLLUP
		(dep.GroupName, dep.name);
GO
--04
SELECT
	dep.GroupName AS 'Division',
	dep.Name AS 'Department',
	COUNT(emp.DepartmentID),
	GROUPING_ID(dep.GroupName, dep.Name)
FROM 
	[HumanResources].[Department] dep
	JOIN [HumanResources].[EmployeeDepartmentHistory] emp
	ON (dep.DepartmentID = emp.DepartmentID)
GROUP BY CUBE
		(dep.GroupName, dep.name)
ORDER BY
		4 asc, 2 asc, 3 asc;
GO
--05
SELECT
	  dep.GroupName AS 'Division',
	  dep.Name AS 'Department', 
	  COUNT(emp.DepartmentID) AS 'Employee Amount',
	  GROUPING_ID(dep.Name,dep.GroupName) AS 'Grouping ID'
FROM
	[HumanResources].[Department] dep
	JOIN [HumanResources].[EmployeeDepartmentHistory] emp
	ON (dep.DepartmentID = emp.DepartmentID)
GROUP BY 
	GROUPING SETS
	(dep.Name, dep.GroupName)
ORDER BY
	4 ASC;
GO
--06
SE
--07

--08

--09

--Extra Question 

--01

--02
