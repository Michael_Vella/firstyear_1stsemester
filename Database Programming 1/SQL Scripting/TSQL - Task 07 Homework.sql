/*
	MCAST IICT
	IICT6001 - DATABASE PROGRAMMING I
	TASK 07
*/
-- Question 01 
USE [AdventureWorks2014];
GO

--Question 02
SELECT *
FROM [Production].[Product];
GO

--Question 03
SELECT ProductNumber,
	   Name,
	   StandardCost,
	   ListPrice 
FROM [Production].[Product];
GO

--Question 04
SELECT ProductNumber,
	   Name,
	   StandardCost,
	   ListPrice 
FROM [Production].[Product]
ORDER BY StandardCost DESC
		 ,ListPrice DESC
		 ,Name ASC;
GO

--Question 05
SELECT ProductNumber,
	   Name,
	   StandardCost,
	   ListPrice,
	   ListPrice-StandardCost AS 'PROFIT'
FROM [Production].[Product]
ORDER BY StandardCost DESC
		 ,ListPrice DESC
		 ,Name ASC;
GO

--Question 06
SELECT ProductNumber,
	   Name,
	   StandardCost,
	   ListPrice,
	   ListPrice-StandardCost AS 'PROFIT',
	   ListPrice * 0.1 AS 'Discount',
	   ListPrice * 0.9 AS 'Discounted Price'
FROM [Production].[Product]
ORDER BY StandardCost DESC
		 ,ListPrice DESC
		 ,Name ASC;
GO

--Question 07
SELECT ProductNumber,
	   Name,
	   ROUND (StandardCost,2),
	   ROUND (ListPrice,2),
	   ROUND (ListPrice-StandardCost,2) AS 'PROFIT',
	   FORMAT (ROUND (ListPrice * 0.1,2),'0.00') AS 'Discount',
	   FORMAT (ROUND (ListPrice * 0.9,2), '0.00') AS 'Discounted Price'
FROM [Production].[Product]
ORDER BY StandardCost DESC
		 ,ListPrice DESC
		 ,Name ASC;
GO

--Question 09
SELECT *
FROM INFORMATION_SCHEMA.COLUMNS
WHERE table_schema ='Person' and 
	   table_name = 'Person';
GO