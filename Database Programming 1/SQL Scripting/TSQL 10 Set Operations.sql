/*
	MCAST IICT
	IICT6001 - Database Programming
	TSQL - 10 - Set Operations
*/
USE [MCAST];
GO

-- Familiarise yourself with the data
SELECT *
FROM [TSQL10].[OldTowns];

SELECT * 
FROM [TSQL10].[NewTowns];
GO
/*
 Set operations dont give a fuck about attributes 
*/

--UNION 
--List all entries in both sets 
SELECT TID, TName
FROM [TSQL10].[OldTowns]
UNION 
SELECT TownID,TownName
FROM [TSQL10].[NewTowns]

/*
	Duplicates are removed from the set operations
	Set Operations dont give a fuck about primary keys 

 */

 --Revise the previous and swap the sequence 
SELECT TownID,TownName
FROM [TSQL10].[NewTowns]
UNION
SELECT TID, TName
FROM [TSQL10].[OldTowns]

--Revise the previous to list all town names from both sets
SELECT TownName
FROM [TSQL10].[NewTowns]
UNION
SELECT TName
FROM [TSQL10].[OldTowns]

--revise the previous to display dupilicates 
SELECT TownName
FROM [TSQL10].[NewTowns]
UNION ALL
SELECT TName
FROM [TSQL10].[OldTowns]

/*
	List all towns that have not changed their name
	Essientially displays duplicates 
*/
SELECT TName
FROM [TSQL10].[OldTowns]
INTERSECT 
SELECT TownName
FROM [TSQL10].[NewTowns];
GO

--List those old town names that are not used anymore 
SELECT TName 
FROM [TSQL10].[OldTowns] 
EXCEPT 
SELECT TownName
FROM [TSQL10].[NewTowns]

--List only new town names
SELECT TownName  
FROM  [TSQL10].[NewTowns]
EXCEPT 
SELECT TName
FROM [TSQL10].[OldTowns]