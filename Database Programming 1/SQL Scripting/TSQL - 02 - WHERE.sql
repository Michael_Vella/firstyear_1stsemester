/*
	IICT6001 - DATABASE PROGRAMING I
	TSQL - 02 WHERE
*/

USE [AdventureWorks2014]
GO

--Flitering 
SELECT FirstName,
	   LastName
FROM [Person].[Person]
ORDER BY FirstName ASC;
GO

SELECT FirstName,
	   LastName
FROM [Person].[Person]
WHERE LastName = 'Smith'
ORDER BY FirstName ASC;
GO

--trying to use aliases in filters
SELECT FirstName AS'Name',
	   LastName AS 'Surname'
FROM [Person].[Person]
WHERE [Surname] = 'Smith'
ORDER BY FirstName ASC;
GO

/*An error since WHERE is logically
  excuted before SELECT*/

--List all persons whose is not Aaron. Order by name in acsending
SELECT FirstName,
	   LastName
FROM [Person].[Person]
WHERE FirstName != 'Aaron'
ORDER BY FirstName ASC;
GO

SELECT FirstName,
	   LastName
FROM [Person].[Person]
WHERE FirstName <> 'Aaron'
ORDER BY FirstName ASC;
GO

--List all products with a list
--Price greater than 9.5-
--sort by list price in acsedending 

SELECT * 
FROM [Production].[Product]
WHERE ListPrice > 9.5
ORDER BY ListPrice ASC;
GO

--Revise the previous to list all
--products with a list price 
-- greater or equal to 9.5
SELECT  * 
FROM [Production].[Product]
WHERE ListPrice >=9.5
ORDER BY ListPrice asc;
GO

--List all products priced less than 20.24
SELECT  * 
FROM [Production].[Product]
WHERE ListPrice < 20.24
ORDER BY ListPrice DESC;
GO

--Revise previous to priced less than or equal to 20.24
SELECT  * 
FROM [Production].[Product]
WHERE ListPrice <= 20.24
ORDER BY ListPrice DESC;
GO

--List the name and surname of all person
-- whos surname is either Adams or Smith
SELECT FirstName AS 'NAME',
	   LastName AS 'SURNAME'
FROM [Person].Person
WHERE LastName = 'Adams' OR 
	  LastName = 'Smith'
ORDER BY LastName ASC;
GO

--Revise the previous statement to use the 'IN' operator instead
SELECT FirstName AS 'NAME',
	   LastName AS 'SURNAME'
FROM [Person].Person
WHERE LastName IN('Adams','Smith')
ORDER BY Person.BusinessEntityID ASC;
GO

--Negate the previous statment
SELECT FirstName AS 'NAME',
	   LastName AS 'SURNAME'
FROM [Person].Person
WHERE NOT (LastName IN('Adams','Smith'))
ORDER BY Person.BusinessEntityID ASC;
GO

SELECT FirstName AS 'NAME',
	   LastName AS 'SURNAME'
FROM [Person].Person
WHERE LastName NOT IN('Adams','Smith')
ORDER BY Person.BusinessEntityID ASC;
GO

--List all products which list price
-- is greater or equal to 9.5 and 
-- less or equal to 20.24
SELECT Name,
	   ListPrice
FROM Production.Product
WHERE ListPrice >= 9.5 AND ListPrice <= 20.24;
GO

--Reviose the previous stat,emt to use the 
-- 'BETWEEN' operator
SELECT NAME,
	   ListPrice
FROM [Production].[Product]
WHERE ListPrice BETWEEN 9.5 AND 20.24;
GO

--NEGATE THE PREVIOUS STATMENT
SELECT NAME,
	   ListPrice
FROM [Production].[Product]
WHERE NOT (ListPrice BETWEEN 9.5 AND 20.24);
GO

SELECT NAME,
	   ListPrice
FROM [Production].[Product]
WHERE ListPrice NOT BETWEEN 9.5 AND 20.24;
GO

--List the persons who have the letter 'a'
-- in their surname
SELECT FirstName,
       LastName
FROM [Person].[Person]
WHERE LastName LIKE '%a%';
GO

--Revise the previous statment such that only the surnames 
-- whos 3rd letter is a
SELECT FirstName,
       LastName
FROM [Person].[Person]
WHERE LastName LIKE '__a%';
GO

--Revise the previous statment to list surnames that contain
-- the letter 'a' or 'e' or 'f'
SELECT FirstName,
       LastName
FROM [Person].[Person]
WHERE LastName LIKE '%[aef]%';
GO

--Revise the previous statment to list surnames that contain
-- the letter 'a' or 'e' or 'f' as their second letter
SELECT FirstName,
       LastName
FROM [Person].[Person]
WHERE LastName LIKE '_[aef]%';
GO

--Revise the previous statment to list surnames such that
--the second letter is an 'a' or an 'e'
SELECT FirstName,
       LastName
FROM [Person].[Person]
WHERE LastName LIKE '_[ae]%';
GO

--Revise the previous statment to list surnames such that
--the second letter is not an 'a' or an 'e'
SELECT FirstName,
       LastName
FROM [Person].[Person]
WHERE LastName LIKE '_[^ae]%';
GO

--revise the previous statment to list all persons 
--whose second letter is any from a till e
SELECT  FirstName,
	    LastName
FROM [Person].[Person]
WHERE LastName LIKE '_[a-e]%';
GO

--List all persons who have a - in their first name
SELECT  FirstName,
		LastName
FROM [Person].[Person]
WHERE FirstName Like '%-%' ESCAPE '\';
GO

--List all persons who do not have a 
--a middle name 
SELECT FirstName,
	   MiddleName,
	   LastName
FROM [Person].[Person]
WHERE MiddleName = NULL; -- WRONG
GO

SELECT FirstName,
	   MiddleName,
	   LastName
FROM [Person].[Person]
WHERE MiddleName IS NULL; -- CORRECT
GO

--Negate the previous statement
SELECT FirstName,
	   MiddleName,
	   LastName
FROM [Person].[Person]
WHERE MiddleName IS NOT NULL; 
GO

SELECT FirstName,
	   MiddleName,
	   LastName
FROM [Person].[Person]
WHERE NOT (MiddleName IS NULL); 
GO