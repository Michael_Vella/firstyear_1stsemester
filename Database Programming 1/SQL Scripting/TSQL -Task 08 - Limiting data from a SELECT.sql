/*
	IICT6001 - Database Programming I
	TASK 08 - Limiting data from a SELECT statment 

*/

USE AdventureWorks2014
GO

--1
SELECT *
FROM [HumanResources].[Department]
WHERE GroupName LIKE 'Research and Development';
GO

--2
SELECT *
FROM [HumanResources].[Department]
WHERE NOT (GroupName = 'Sales and Marketing');
GO

--3
SELECT *
FROM [Sales].[SalesOrderDetail]
WHERE OrderQty > 2;
GO

--4
SELECT *
FROM [Sales].[SalesOrderDetail]
WHERE LineTotal > 200 AND LineTotal < 400;
GO

--5
SELECT *
FROM [Sales].[SalesOrderDetail]
WHERE UnitPriceDiscount BETWEEN 0.15 AND 0.40;
GO
--6
SELECT SalesOrderID,
	   SalesOrderDetailID,
	   UnitPrice * UnitPriceDiscount AS 'discount price'
FROM [Sales].[SalesOrderDetail]
WHERE (UnitPrice * UnitPriceDiscount) >= 200 AND 
	  (UnitPrice * UnitPriceDiscount) < 300;
GO

--7
SELECT CountryRegionCode AS'Region Code',
	   [SalesTerritory].[Group] AS 'Continent'
FROM [Sales].[SalesTerritory]
WHERE [SalesTerritory].[Group] Like 'North America' OR [SalesTerritory].[Group] LIKE 'Europe'
ORDER BY 2 ASC, 1 ASC;
GO

SELECT *
FROM [Sales].SalesTerritory;
GO
--8
SELECT [SalesTerritory].[Group] as 'Continent',
	   Name AS 'Region',
	   CountryRegionCode AS 'Territory',
	   SalesLastYear AS 'Sales'
FROM [Sales].[SalesTerritory]
WHERE CountryRegionCode NOT IN ('GB','DE')
ORDER BY 1 ASC, 2 ASC, 3 ASC;
GO 

--9
SELECT [SalesTerritory].[Group] as 'Continent',
	   Name AS 'Territory',
	   CountryRegionCode AS 'Region',
	   SalesLastYear AS 'Sales'
FROM [Sales].[SalesTerritory]
WHERE CountryRegionCode = 'US' AND 
	  [Name] LIKE 'North%'
ORDER BY 1 ASC, 2 ASC, 3 ASC;
GO 

--10
SELECT *
FROM [AdventureWorks2014].[Production].[Document]
WHERE [FileName] LIKE '%[1-3].%';
GO 
--11
SELECT *
FROM [AdventureWorks2014].[Production].[Document]
WHERE [Document].[DocumentSummary] IS NULL ;
GO
--EXTRA QUESTIONS

--1
SELECT * 
FROM [Person].[Person]
WHERE ModifiedDate
	  BETWEEN '20080101' AND '20081231';
GO

--or 
SELECT * 
FROM [Person].[Person]
WHERE YEAR(ModifiedDate) = 2008;
GO

--2 
SELECT DISTINCT 
				addr.City AS 'City',
				stat.Name AS 'State'
FROM 
     [AdventureWorks2014].[Person].[Address] addr,
	 [AdventureWorks2014].[Person].[StateProvince] stat
WHERE 
	 stat.StateProvinceID = --A.PK
	 addr.StateProvinceID	-- B.FK
ORDER BY 
		'State' ASC, 'City' ASC; 
GO