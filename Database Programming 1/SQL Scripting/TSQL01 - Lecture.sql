/*
	MCAST IICT 
	IICT6001 - Database Programming 1
	TSQL01 - SELECT
*/

USE [AdventureWorks2014] /*Making sure we use the adventure works DB*/
GO

-- Retrieve all person data 
SELECT *
FROM [Person].[PERSON];
GO

--Display few attributes 
SELECT BusinessEntityId,
	   FirstName,
	   LastName
FROM [Person].[Person];
GO

--Before typing attributes, complete the FROM clause
SELECT BusinessEntityID,
	   FirstName,
	   LastName
FROM [Person].[Person];
GO

--USe Aliases
SELECT BusinessEntityID AS 'ID',
	   FirstName AS 'Name',
	   LastName AS 'Surname'
FROM [Person].[Person]; 
GO

--Sorting
SELECT BusinessEntityID AS 'ID',
	   FirstName AS 'Name',
	   LastName AS 'Surname'
FROM [Person].[Person] 
ORDER BY FirstName ASC;
GO

SELECT BusinessEntityID AS 'ID',
	   FirstName AS 'Name',
	   LastName AS 'Surname'
FROM [Person].[Person]
ORDER BY 'Name' ASC;
GO

--Sorting with a non visible column
SELECT BusinessEntityID AS 'ID'
	   ,FirstName AS 'NAME'
	   ,LastName AS 'Surname'
FROM [Person].[Person]
ORDER BY MiddleName;
GO

--Sorting in multiple orders 
SELECT BusinessEntityID AS 'ID',
	   FirstName AS 'Name',
	   LastName AS 'Surname'
FROM [Person].[Person]
ORDER BY FirstName ASC,
		 LastName DESC;
GO

--Arithmetic Operations
SELECT 50 AS 'Default Value'
	   ,50 + 30 AS 'Addition'
	   ,50 - 30 AS 'Subtraction'
	   ,50 * 30 AS 'Multiplication'
	   ,50 / 30 AS 'Division'
	   ,50 % 30 As 'Modulus';
GO

--Handling Duplicates 
SELECT FirstName,
	   LastName
FROM [Person].[Person]
ORDER BY 1 ASC, 2 ASC ;
GO

SELECT DISTINCT FirstName,
	   LastName
FROM [Person].[Person]
ORDER BY 1 ASC, 2 ASC ;
GO