/*
	MCAST IICT
	IICT 60001 -  Database Programming I
	T-SQL06 - Adavanced and ranking functions 
*/
USE [AdventureWorks2014];
GO

/*
Display all vendors from Purchasing.Vendor
The year when each product was due 
from Purchase Order Header and the 
order quantity from Purchase Order Detail.
SCHEMA: Purchasing 

e.g  VENDOR,	Year of Purchase,	Units Bought
	 ABC		2011				5000
	 ABC		2011				6000
*/
SELECT 
		vnd.Name,
		YEAR(poh.OrderDate) AS 'YEAR',
		pod.OrderQty
FROM 
	[Purchasing].[Vendor] vnd
	JOIN [Purchasing].[PurchaseOrderHeader] poh
	ON (vnd.BusinessEntityID = poh.VendorID)
	JOIN [Purchasing].[PurchaseOrderDetail] pod
	ON (poh.PurchaseOrderID = pod.PurchaseOrderID);
GO

--Wrap the previous in a CTE
WITH VendorPurchases
	 (VendorName, PurchaseYear, TotalPurchases)
	 AS
	 (SELECT 
		vnd.Name,
		YEAR(poh.OrderDate),
		pod.OrderQty	
	  FROM 
		[Purchasing].[Vendor] vnd
		JOIN [Purchasing].[PurchaseOrderHeader] poh
		ON (vnd.BusinessEntityID = poh.VendorID)
		JOIN [Purchasing].[PurchaseOrderDetail] pod
		ON (poh.PurchaseOrderID = pod.PurchaseOrderID))
SELECT
	 * 
FROM 
	VendorPurchases;
GO
--Revise the previous to list all 
--unique years from the VendorPurchases CTE
WITH VendorPurchases
	 (VendorName, PurchaseYear, TotalPurchases)
	 AS
	 (SELECT 
		vnd.Name,
		YEAR(poh.OrderDate),
		pod.OrderQty	
	  FROM 
		[Purchasing].[Vendor] vnd
		JOIN [Purchasing].[PurchaseOrderHeader] poh
		ON (vnd.BusinessEntityID = poh.VendorID)
		JOIN [Purchasing].[PurchaseOrderDetail] pod
		ON (poh.PurchaseOrderID = pod.PurchaseOrderID))
SELECT DISTINCT 
	 PurchaseYear
FROM 
	VendorPurchases
ORDER BY
       1 ASC;
GO

--Pivot the CTE to display the total purchase per vendor for
--each unique year 
WITH VendorPurchases
	 (VendorName, PurchaseYear, TotalPurchases)
	 AS
	 (SELECT 
		vnd.Name,
		YEAR(poh.OrderDate),
		pod.OrderQty	
	  FROM 
		[Purchasing].[Vendor] vnd
		JOIN [Purchasing].[PurchaseOrderHeader] poh
		ON (vnd.BusinessEntityID = poh.VendorID)
		JOIN [Purchasing].[PurchaseOrderDetail] pod
		ON (poh.PurchaseOrderID = pod.PurchaseOrderID))
SELECT 
	 VendorName,
	 ISNULL([2011],0) as '2011',
	 ISNULL([2012],0) as '2012',
	 ISNULL([2013],0) as '2013',
	 ISNULL([2014],0) as '2014'
FROM 
	 VendorPurchases
	 PIVOT (SUM(TotalPurchases) FOR
	 PurchaseYear 
	 IN ([2011], [2012], [2013], [2014]))
	 AS pvt
ORDER BY 
	 1 ASC;
GO

--Select the first 50 names
SELECT TOP 
		50 FirstName
FROM 
		[Person].[Person]
ORDER BY 
		FirstName asc;
GO

SELECT TOP 
		50 WITH TIES FirstName
FROM 
		[Person].[Person]
ORDER BY 
		FirstName ASC;
GO

--List the first 10 percent of all names
SELECT TOP 
		10 PERCENT FirstName
FROM  
	[Person].[Person]
ORDER BY 
	FirstName ASC;
GO

--Rank all products in sub category 1 based on their listprice
SELECT 
	Name,
	ListPrice,
	ROW_NUMBER() OVER(ORDER BY ListPrice DESC) AS 'Row Number',
	RANK() OVER(Order By ListPrice DESC) AS 'Rank',
	DENSE_RANK() OVER(Order By ListPrice DESC) AS 'Dense Rank',
	NTILE(4) OVER(Order by ListPrice DESC) AS 'NTILE'	
FROM 
	[Production].[Product]
WHERE 
	ProductSubcategoryID = 1
ORDER BY 
	ListPrice DESC;
GO
--Revise the previous to include only name, list price and row number 
--in a CTE
WITH ProductPrices
	(RowNum, PrdName, PrdPrice)
	AS(
		SELECT 
			  ROW_NUMBER() OVER (ORDER BY ListPrice Desc),
			  Name, 
			  ListPrice
		FROM  [Production].[Product])
SELECT *
FROM ProductPrices;
GO
--Revise the previous to display the second page of the report 
--with a 10 ROWS per page LIMIT 
WITH ProductPrices
	(RowNum, PrdName, PrdPrice)
	AS(
		SELECT 
			  ROW_NUMBER() OVER (ORDER BY ListPrice Desc),
			  Name, 
			  ListPrice
		FROM  [Production].[Product])
SELECT 
	   *
FROM 
	   ProductPrices
ORDER BY 
	   1
	   OFFSET 20 ROWS FETCH NEXT 10 ROWS ONLY ;
GO