/*
	MCAST IICT
	IICT6001 - Database Programming 1
	Task 09 - Setup Scripts
*/

USE [MCAST];

ALTER TABLE [Task09].[Department]
	DROP CONSTRAINT dept_mid_fk;

DROP TABLE [Task09].[Employee];
DROP TABLE [Task09].[Job];
DROP TABLE [Task09].[Department];
DROP TABLE [Task09].[SalaryGrade];
DROP SCHEMA [Task09];
GO
CREATE SCHEMA [Task09];
GO

CREATE TABLE [Task09].[SalaryGrade] (
	grade_id INTEGER PRIMARY KEY,
	minimum_salary FLOAT NOT NULL,
	maximum_salary FLOAT NOT NULL,
	salary_grade NVARCHAR(10) UNIQUE NOT NULL
);

CREATE TABLE [Task09].[Department] (
	department_id INTEGER PRIMARY KEY,
	department_name NVARCHAR(45) UNIQUE NOT NULL,
	manager_id INTEGER NOT NULL
);

CREATE TABLE [Task09].[Job] (
	job_id INTEGER PRIMARY KEY,
	job_title NVARCHAR(45) UNIQUE NOT NULL,
	department_id INTEGER NOT NULL
		REFERENCES [Task09].[Department](department_id) ON DELETE CASCADE
);

CREATE TABLE [Task09].[Employee] (
	employee_id INTEGER PRIMARY KEY,
	first_name NVARCHAR(45) NOT NULL,
	last_name NVARCHAR(45) NOT NULL,
	salary FLOAT NOT NULL,
	job_id INTEGER NOT NULL
		REFERENCES [Task09].[Job](job_id),
	manager_id INTEGER
		REFERENCES [Task09].[Employee](employee_id)
);

BEGIN TRANSACTION
	INSERT INTO [Task09].[SalaryGrade]
	VALUES (1, 10000,14999.99, 'A');

	INSERT INTO [Task09].[SalaryGrade]
	VALUES (2, 15000,19999.99, 'B');

	INSERT INTO [Task09].[SalaryGrade]
	VALUES (3, 20000,24999.99, 'C');

	INSERT INTO [Task09].[SalaryGrade]
	VALUES (4, 25000,29999.99, 'D');

	INSERT INTO [Task09].[Department]
	VALUES (10, 'IT', 2);

	INSERT INTO [Task09].[Department]
	VALUES (20, 'HR', 3);

	INSERT INTO [Task09].[Department]
	VALUES (30, 'Chairman Office', 1);

	INSERT INTO [Task09].[Job]
	VALUES (1, 'IT Manager', 10);

	INSERT INTO [Task09].[Job]
	VALUES (2, 'IT Developer', 10);

	INSERT INTO [Task09].[Job]
	VALUES (3, 'IT Technician', 10);

	INSERT INTO [Task09].[Job]
	VALUES (4, 'Systems Administrator', 10);

	INSERT INTO [Task09].[Job]
	VALUES (5, 'HR Manager', 20);

	INSERT INTO [Task09].[Job]
	VALUES (6, 'HR Clerk', 20);

	INSERT INTO [Task09].[Job]
	VALUES (7, 'Chairman', 30);

	INSERT INTO [Task09].[Employee]
	VALUES (1, 'Joe', 'Borg', 27000, 7, NULL);

	INSERT INTO [Task09].[Employee]
	VALUES (2, 'Lisa', 'Abela', 24000, 1, 1);

	INSERT INTO [Task09].[Employee]
	VALUES (3, 'Tony', 'Galea', 22000, 5, 1);

	INSERT INTO [Task09].[Employee]
	VALUES (4, 'Sarah', 'Tanti', 16000, 6, 3);

	INSERT INTO [Task09].[Employee]
	VALUES (5, 'Ian', 'Zammit', 19000, 2, 2);

	INSERT INTO [Task09].[Employee]
	VALUES (6, 'Rita', 'Plum', 18000, 4, 2);

	INSERT INTO [Task09].[Employee]
	VALUES (7, 'Alex', 'Cini', 19500, 2, 2);

	ALTER TABLE [Task09].[Department]
	ADD CONSTRAINT dept_mid_fk 
		FOREIGN KEY (manager_id) 
		REFERENCES [Task09].[Employee](employee_id) 
		ON DELETE CASCADE;
COMMIT;