/*
	TSQL 05 Aggreagate Functions
	MCAST IICT 
	IICT60001 - Database Programming 1
*/
USE [AdventureWorks2014]
GO
--List all data in Person 
SELECT *
FROM [Person].[Person];
GO
--Display the total number of rows in person 
SELECT COUNT(*)
FROM  [Person].[Person];
GO
--Display the total numbere of rows using 
--the PK in person, the using the middle name 
SELECT	
		COUNT(*) as 'all',
		COUNT(per.BusinessEntityID) as 'pk',
		COUNT(per.MiddleName) as 'middle name'
FROM [Person].[Person] per;
GO

--Display the total number of different middle names
SELECT 
		COUNT(*) as 'all',
		COUNT(per.BusinessEntityID) as 'pk',
		COUNT(DISTINCT per.MiddleName) as 'Different Middle Names'
FROM 
		[Person].[Person] per;
GO
-- List the total number of persons for each last name 
SELECT 
		per.LastName,
		Count(*)		 
FROM 

		[Person].[Person] per
		
GROUP BY 
		per.LastName;
GO

--Revise the previous statment such that you list only surnames 
--Abel,Adam,Adina,Akers,Beck
SELECT 
		per.LastName,
		Count(*)		 
FROM 

		[Person].[Person] per
WHERE  
		per.LastName IN ('Abel','Adams', 'Adina','Akers','Beck')
GROUP BY 
		per.LastName;
GO
--Revise the pervious such that only those with more than 5 persons with the same
--surname are listed
SELECT 
		per.LastName,
		Count(*)		 
FROM 

		[Person].[Person] per
WHERE  
		per.LastName IN ('Abel','Adams', 'Adina','Akers','Beck')
GROUP BY 
		per.LastName
HAVING 
		COUNT(*) > 5;
GO 

--List the minimum,maximum, and average price per product
--Revise the previous to display the minimum,naximum, and average
--also the total number of products per sub categaroy ID 
SELECT  
		COUNT(pro.ProductSubcategoryID),
		MIN(pro.ListPrice) as 'MIN',
		MAX(pro.ListPrice) as 'MAX',
		AVG(pro.ListPrice) AS 'AVG'
FROM	
		[Production].[Product] pro
GROUP BY 
		pro.ProductSubcategoryID;
GO
--Revise the previous to limit the report to sub category IDs 4, 5, 10 and 12
SELECT  pro.ProductSubcategoryID AS 'SUBCAT',
		COUNT(pro.ProductSubcategoryID)'TOTAL SUBCAT',
		MIN(pro.ListPrice) as 'MIN',
		MAX(pro.ListPrice) as 'MAX',
		AVG(pro.ListPrice) AS 'AVG'
FROM	
		[Production].[Product] pro
WHERE 
		pro.ProductSubcategoryID IN (4,5,10,12)
GROUP BY 
		pro.ProductSubcategoryID;
GO

--Revise the previous to list the only entries with more than 10 products
SELECT  pro.ProductSubcategoryID AS 'SUBCAT',
		COUNT(pro.ProductSubcategoryID)'TOTAL SUBCAT',
		MIN(pro.ListPrice) as 'MIN',
		MAX(pro.ListPrice) as 'MAX',
		AVG(pro.ListPrice) AS 'AVG'
FROM	
		[Production].[Product] pro
WHERE 
		pro.ProductSubcategoryID IN (4,5,10,12)
GROUP BY 
		pro.ProductSubcategoryID
HAVING 
		COUNT(*) >10;
GO
--List the territory group, country region code and last year sales from 
--sales.salesterritory table
SELECT
		[terr].[Group],
		[terr].[CountryRegionCode],
		[terr].[SalesLastYear]
FROM 
		[Sales].[SalesTerritory] terr;
GO

--Revise the previous to calculate the total sale per group
-- and country region code
SELECT
		[terr].[Group],
		[terr].[CountryRegionCode],
		SUM([terr].[SalesLastYear]) AS ' Total Sales'
FROM 
		[Sales].[SalesTerritory] terr
GROUP BY
		[terr].[Group], [terr].[CountryRegionCode];
GO

--Revise the previous to remove the country and find the total sales per
--group
SELECT
		[terr].[Group],
		SUM([terr].[SalesLastYear]) AS ' Total Sales'
FROM 
		[Sales].[SalesTerritory] terr
GROUP BY
		[terr].[Group];
GO
--Revise the previous to find the total global sales 
SELECT
	 SUM([terr].[SalesLastYear]) AS 'Global Sales'
FROM 
	 [Sales].[SalesTerritory] terr;
GO
--Revise the previous 3 statements in 1 
SELECT
		terr.[Group],
		terr.CountryRegionCode,
		SUM(terr.SalesLastYear) As 'TOTAL Sales'
FROM
	    [Sales].[SalesTerritory] terr
GROUP BY ROLLUP (terr.[Group], terr.CountryRegionCode);
GO

--Revise the previous to dispaly the power set 
SELECT
		terr.[Group],
		terr.CountryRegionCode,
		SUM(terr.SalesLastYear) As 'TOTAL Sales'
FROM
	 [Sales].[SalesTerritory] terr
GROUP BY CUBE (terr.[Group], terr.CountryRegionCode);
GO
--Revise the previous to list only 
--grouping by group and country region code individually 
SELECT
		ter.[Group],
		ter.CountryRegionCode,
		SUM(SalesLastYear) AS 'TOTAL Sales'
FROM 
	 [Sales].[SalesTerritory] ter 
GROUP BY 
		 GROUPING SETS
		 (([ter].[Group]),
		 (ter.CountryRegionCode) );
GO

--Revise the CUBE Report and add a coloumn for each grouping attribute 
SELECT
		terr.[Group],
		terr.CountryRegionCode,
		SUM(terr.SalesLastYear) As 'TOTAL Sales',
		GROUPING (terr.[Group]) AS 'group',
		GROUPING (terr.CountryRegionCode) as 'cOUNTRY'
FROM
	    [Sales].[SalesTerritory] terr
GROUP BY CUBE 
		(terr.[Group], terr.CountryRegionCode);
GO
--Revise the previous to use grouping id instead
SELECT
		terr.[Group],
		terr.CountryRegionCode,
		SUM(terr.SalesLastYear) As 'TOTAL Sales',
		GROUPING_ID (terr.[Group],terr.CountryRegionCode) AS 'group_id'
FROM
	    [Sales].[SalesTerritory] terr
GROUP BY CUBE 
		(terr.[Group], terr.CountryRegionCode);
GO