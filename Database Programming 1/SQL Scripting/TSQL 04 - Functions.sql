/*	MCAST IICT
	IICT6001 - Database programmming 
	TSQL 04 - Functions 
*/

USE [AdventureWorks2014];
GO

SELECT UPPER(RTRIM(LastName)) + ', ' + FirstName AS Name
FROM Person.Person
ORDER BY LastName;
GO

--List all names in UPPER and LOWER case
SELECT 
		FirstName AS 'Orignal',
		LOWER(FirstName) AS 'lower',
		UPPER(FirstName) AS 'UPPER'
FROM 
		[Person].[Person];
GO

--Using LEFT function
SELECT LEFT('abcdefg', 2) ;
GO

--Display the first name of all person then in 
--seperate coloumns display the first two and the last 
--two letters
SELECT 
		FirstName,
		Left(FirstName,2),
		RIGHT(FirstName,2) 
FROM [Person].[Person]
GO 

--Display the first name of all persons 
--followed bt the inverted version
---of the same name. Figure out how to
--use the same function again to optain 
--the orginal value
--e.g. Orginal	Inverted	Nesteted 
--e.g. Frankie	 eiknarF	Frankie
SELECT 
		FirstName AS 'Orginal',
		REVERSE(FirstName) AS 'Inverted' ,
		REVERSE(REVERSE(FirstName)) AS 'Nested'
FROM  
		[Person].[Person]
GO;

-- Concatenate the name, middle name and surname with space,
--first using the +symbol, followed by the concat function
SELECT  
	    FirstName, MiddleName, LastName,
		FirstName + ' ' + MiddleName + ' ' + LastName AS 'Using + ',
		CONCAT (FirstName, ' ', MiddleName, ' ', LastName) AS 'Using function'
FROM [Person].[Person];
GO

--Revise the pervious to remove any double spaces 
SELECT REPLACE('abcdefghicde','cde','xxx');
GO
SELECT  
		
		REPLACE(
		CONCAT (FirstName, ' ', MiddleName, ' ', LastName),
		'  ',
		' '
		) AS 'Cleaned'
FROM [Person].[Person];
GO

--MSDN example for CEILING
SELECT 
		CEILING($123.45),
		CEILING($-123.45),
		CEILING($0.0)
GO

--Display an indication on whether the values 50, 0, -50 are
--postive or negative

SELECT 
	   SIGN(50) AS 'Postive',
	   SIGN(0) AS ' Zero',
	   SIGN(-50) AS 'NEGATIVE'
GO

--Calculate 5 squared, 
--          5 cubed,
--			5 to the power of 4

SELECT POWER(5, 2) AS 'Squared',
	   POWER(5, 3) AS 'Cubed',
	   POWER(5, 4) AS 'Power of 4'

GO

--Round and truncate some values 

SELECT 
		ROUND(4592.45,2),
		ROUND(4592.45,1),
		ROUND(4592.45,0),
		ROUND(4592.45,-1),
		ROUND(4592.45,-2)
GO

SELECT 
		ROUND(4592.45,2,1),
		ROUND(4592.45,1,1),
		ROUND(4592.45,0,1),
		ROUND(4592.45,-1,1),
		ROUND(4592.45,-2,1)
GO

--Displayt the current time 
SELECT 
	   SYSDATETIME(),
	   SYSDATETIMEOFFSET(),
	   SYSUTCDATETIME()
GO

--Extract the year, mont and day 
--from the current, date, time 
--in sepreate coloumns
SELECT	
		SYSDATETIME() AS 'ORGINAL'
		,YEAR (SYSDATETIME())
		,DATEPART(YEAR, SYSDATETIME())
		,MONTH(SYSDATETIME())
		,DATEPART(MONTH,SYSDATETIME())
		,DAY(SYSDATETIME())
		,DATEPART(DAY,SYSDATETIME())
GO