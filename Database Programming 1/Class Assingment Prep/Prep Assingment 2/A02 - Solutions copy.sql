/*
	MCAST IICT
	IICT6001 - A02S01
*/

-- KU4
USE [MCAST];

-- Question 01
SELECT		cty.country_name AS 'Country'
			, upr.first_name AS 'First Name'
			, upr.last_name AS 'Last Name'
FROM		[Messaging].[Country] cty
			LEFT OUTER JOIN [Messaging].[UserProfile] upr
			ON (upr.country_id=cty.country_id)
ORDER BY	1 ASC, 3 ASC, 2 ASC;

-- Question 02
SELECT		uac.account_username AS 'Group Owner'
			, ugr.group_name AS 'Group name'
			, mac.account_username AS 'Group member'
FROM		[Messaging].[UserAccount] uac
			JOIN [Messaging].[UserGroup] ugr
			ON (uac.account_id=ugr.owner_account_id)
			JOIN [Messaging].[GroupMember] gme
			ON (ugr.group_id=gme.group_id)
			JOIN [Messaging].[UserAccount] mac
			ON (gme.member_account_id=mac.account_id)
ORDER BY	1 ASC, 2 ASC, 3 ASC;

-- Question 03
SELECT		upr.first_name AS 'First Name'
			, upr.last_name AS 'Last Name'
			, ast.ranking AS 'Ranking'
FROM		[Messaging].[AccountStats] ast
			JOIN [Messaging].[UserProfile] upr
			ON (ast.account_id=upr.account_id)
ORDER BY	3 DESC, 2 ASC, 1 ASC;

-- Question 04
SELECT		uac.account_username AS 'Sender'
			, mes.message_subject AS 'Subject'
			, rac.account_username AS 'Recipient'
FROM		[Messaging].[Message] mes
			JOIN [Messaging].[UserAccount] uac
			ON (mes.account_id=uac.account_id)
			LEFT OUTER JOIN [Messaging].[MessageRecipient] rec
			ON (rec.message_id=mes.message_id)
			LEFT OUTER JOIN [Messaging].[UserAccount] rac
			ON (rec.account_id=rac.account_id)
ORDER BY	mes.sent_date ASC, 2 ASC, 3 ASC;

-- KU6
-- Question 05
SELECT		uac.account_username AS 'Username'
			, COUNT(cle.owner_account_id) AS 'Contacts'
FROM		[Messaging].[UserAccount] uac
			LEFT OUTER JOIN [Messaging].[ContactListEntry] cle
			ON (uac.account_id=cle.owner_account_id)
GROUP BY	uac.account_username
ORDER BY	1 ASC;

-- Question 06
SELECT		uac.account_username AS 'Username'
			, ISNULL(COUNT(ugr.owner_account_id),0) AS 'Number of groups'
FROM		[Messaging].[UserAccount] uac
			LEFT OUTER JOIN [Messaging].[UserGroup] ugr
			ON (uac.account_id=ugr.owner_account_id)
GROUP BY	uac.account_username
ORDER BY	2 DESC, 1 ASC;

-- Question 07
SELECT		uac.account_username AS 'Username'
			, ISNULL(COUNT(DISTINCT gme.group_id),0) AS 'Listed as group member'
FROM		[Messaging].[UserAccount] uac
			LEFT OUTER JOIN [Messaging].[GroupMember] gme
			ON (uac.account_id=gme.member_account_id)
GROUP BY	uac.account_username
ORDER BY	2 DESC, 1 ASC;

-- Question 08
SELECT		cty.country_name AS 'Country'
			, ISNULL(COUNT(mes.message_id),0) AS 'Messages sent'
FROM		[Messaging].[Message] mes
			JOIN [Messaging].[UserAccount] uac
			ON (mes.account_id=uac.account_id)
			JOIN [Messaging].[UserProfile] upr
			ON (uac.account_id=upr.account_id)
			RIGHT OUTER JOIN [Messaging].[Country] cty
			ON (upr.country_id=cty.country_id)
GROUP BY	cty.country_name
ORDER BY	1 ASC;

-- KU7
-- Question 09
WITH CountryPopulation (CountryName, AccountsPerCountry)
AS (SELECT		cty.country_name
				, ISNULL(COUNT(upr.account_id),0)
	FROM		[Messaging].[Country] cty
				LEFT OUTER JOIN [Messaging].[UserProfile] upr
				ON (cty.country_id=upr.country_id)
	GROUP BY	cty.country_name)
SELECT		CountryName AS 'Country'
			, AccountsPerCountry AS 'Number of Accounts'
			, DENSE_RANK() OVER(ORDER BY AccountsPerCountry DESC) AS 'Rank'
FROM		CountryPopulation
ORDER BY	3 ASC, 1 ASC;

-- Question 10
WITH MessageRepliesCount(MessageSubject, NoOfReplies)
AS (SELECT		mes.message_subject
				, COUNT(cms.message_id)
	FROM		[Messaging].[Message] mes
				LEFT OUTER JOIN [Messaging].[Message] cms
				ON (cms.parent_message_id=
					mes.message_id)
	GROUP BY	mes.message_subject)
SELECT		MessageSubject AS 'Subject'
			, NoOfReplies AS 'Replies Count'
			, DENSE_RANK() OVER(ORDER BY NoOfReplies DESC) AS 'Rank'
FROM		MessageRepliesCount
ORDER BY	3 ASC, 1 ASC;

-- Question 11
WITH YearOfBirthCTE (FirstName, YearOfBirth)
AS (SELECT	first_name
			, YEAR(date_of_birth)
	FROM	[Messaging].[UserProfile])
SELECT	ISNULL([1997],0) AS '1997'
		, ISNULL([1998],0) AS '1998'
		, ISNULL([1999],0) AS '1999'
		, ISNULL([2000],0) AS '2000'
		, ISNULL([2001],0) AS '2001'
		, ISNULL([2002],0) AS '2002'
FROM	YearOfBirthCTE
		PIVOT (COUNT(FirstName) FOR YearOfBirth IN 
			([1997], [1998],[1999],[2000],[2001],[2002])) AS pvt;

-- Question 12
WITH PersonGenderCTE (FirstName, Gender)
AS (SELECT	first_name
			, Gender
	FROM	[Messaging].[UserProfile])
SELECT	[M] AS 'Males'
		, [F] AS 'Females'
FROM	PersonGenderCTE
		PIVOT (COUNT(FirstName) FOR Gender IN 
			([M], [F])) AS pvt;

-- AA2
-- Question 13
SELECT		mes.message_subject AS 'Subject'
			, uac.account_username AS 'Sender'
			,	CASE	WHEN mes.parent_message_id IS NULL THEN
						'New Conversation'
						ELSE CONCAT('Reply of message with id: ', mes.parent_message_id)
				END AS 'New or Reply'
FROM		[Messaging].[Message] mes
			JOIN [Messaging].[UserAccount] uac
			ON (mes.account_id=uac.account_id)
ORDER BY	1 ASC

-- Question 14
WITH MessagesSent (AccountUsername, MessageCount)
AS	(SELECT		uac.account_username
				, COUNT(mes.message_id)
	FROM		[Messaging].[Message] mes
				RIGHT OUTER JOIN [Messaging].[UserAccount] uac
				ON (mes.account_id=uac.account_id)
	GROUP BY	uac.account_username)
SELECT		AccountUsername
			, CASE	WHEN MessageCount > 1 THEN 'Very Active'
					WHEN MessageCount = 1 THEN 'Barely Active'
					ELSE 'Inactive'
			END AS 'Activity'
FROM		MessagesSent
ORDER BY	1 ASC;

-- AA5
-- Question 15
SELECT		cty.country_name AS 'Country'
			, upr.gender AS 'Gender'
			, ISNULL(COUNT(upr.account_id),0) AS 'Number of Accounts'
			, GROUPING_ID(cty.country_name, upr.gender) AS 'Grouping'
FROM		[Messaging].[UserProfile] upr
			RIGHT OUTER JOIN [Messaging].[Country] cty
			ON (upr.country_id=cty.country_id)
GROUP BY	ROLLUP(cty.country_name, upr.gender);

-- Question 16
SELECT		YEAR(date_of_birth) AS 'Year Of Birth'
			, gender AS 'Gender'
			, COUNT(account_id) AS 'Number of Accounts'
			, GROUPING_ID(YEAR(date_of_birth), gender) AS 'Grouping'
FROM		[Messaging].[UserProfile] upr
GROUP BY	GROUPING SETS (	(YEAR(date_of_birth), gender),
							(gender),
							())
ORDER BY	4 ASC, 3 DESC, 1 ASC, 2 ASC;

-- SE2
-- Question 17
SELECT		country_name
FROM		[Messaging].[Country]
WHERE		country_id IN	(SELECT DISTINCT country_id
							FROM	[Messaging].[UserProfile]
							WHERE	Year(date_of_birth) IN (1999, 2000))
ORDER BY	1 ASC;

-- Question 18
SELECT	account_username AS 'Username'
FROM	[Messaging].[UserAccount]
WHERE	account_id IN (SELECT		member_account_id
						FROM		[Messaging].[GroupMember]
						GROUP BY	member_account_id
						HAVING		COUNT(group_id) > 1);