/*
	MCAST IICT
	IICT6001 - Database Programming I

	A02S01 Setup Script 
*/

-- Use the MCAST database and create the messaging schema
USE [MCAST];
GO

-- Prepare the delete and drop statements
DELETE FROM [Messaging].[MessageRecipient];
DELETE FROM [Messaging].[Message];
DELETE FROM [Messaging].[GroupMember];
DELETE FROM [Messaging].[UserGroup];
DELETE FROM [Messaging].[ContactListEntry];
DELETE FROM [Messaging].[UserProfile];
DELETE FROM [Messaging].[Country];
DELETE FROM [Messaging].[UserAccount];
DELETE FROM [Messaging].[AccountStats];

DROP TABLE [Messaging].[MessageRecipient];
DROP TABLE [Messaging].[Message];
DROP TABLE [Messaging].[UserProfile];
DROP TABLE [Messaging].[Country];
DROP TABLE [Messaging].[ContactListEntry];
DROP TABLE [Messaging].[GroupMember];
DROP TABLE [Messaging].[UserGroup];
DROP TABLE [Messaging].[UserAccount];
DROP TABLE [Messaging].[AccountStats];
GO

DROP SCHEMA [Messaging];
GO

USE [master];
GO

DROP DATABASE [MCAST];
GO

CREATE DATABASE [MCAST];
GO

USE [MCAST];
GO

CREATE SCHEMA [Messaging];
GO

-- Create Structure
CREATE TABLE [Messaging].[UserAccount] (
	account_id INTEGER
		CONSTRAINT uac_pk PRIMARY KEY IDENTITY(1,1)
	, account_username NVARCHAR(356)
		CONSTRAINT uac_unm_un UNIQUE
		NOT NULL
	, account_password NVARCHAR(512) NOT NULL
	, recovery_email NVARCHAR(356) NOT NULL
	, contact_number INTEGER NOT NULL
);

CREATE TABLE [Messaging].[Country] (
	country_id SMALLINT
		CONSTRAINT cty_pk PRIMARY KEY IDENTITY(1,1)
	, country_name NVARCHAR(45)
		CONSTRAINT cty_cnm_un UNIQUE
		NOT NULL
);

CREATE TABLE [Messaging].[UserProfile] (
	account_id INTEGER
		CONSTRAINT upr_aid_fk
			REFERENCES [Messaging].[UserAccount] (account_id)
		CONSTRAINT upr_pk
			PRIMARY KEY (account_id)
	, first_name NVARCHAR(45) NOT NULl
	, last_name NVARCHAR(45) NOT NULL
	, gender CHAR NOT NULL
	, date_of_birth DATE NOT NULL
	, avatar NVARCHAR(3058) DEFAULT NULL
	, country_id SMALLINT
		CONSTRAINT upr_cid_fk
			REFERENCES [Messaging].[Country] (country_id)
		NOT NULL
);

CREATE TABLE [Messaging].[ContactListEntry] (
	owner_account_id INTEGER
		CONSTRAINT cle_oid_fk
			REFERENCES [Messaging].[UserAccount] (account_id)
	, contact_account_id INTEGER
		CONSTRAINT cle_aid_fk
			REFERENCES [Messaging].[UserAccount] (account_id)
	, CONSTRAINT cle_pk PRIMARY KEY (owner_account_id, contact_account_id)
);

CREATE TABLE [Messaging].[UserGroup] (
	group_id INTEGER 
		CONSTRAINT grp_pk PRIMARY KEY IDENTITY(1,1)
	, group_name NVARCHAR(45) NOT NULL
	, owner_account_id INTEGER
		CONSTRAINT grp_aid_fk
			REFERENCES [Messaging].[UserAccount] (account_id) 
		NOT NULL
);

CREATE TABLE [Messaging].[GroupMember] (
	group_id INTEGER 
		CONSTRAINT grm_gid_fk
			REFERENCES [Messaging].[UserGroup] (group_id)
	, member_account_id INTEGER
		CONSTRAINT grm_aid_fk
			REFERENCES [Messaging].[UserAccount] (account_id)
	, CONSTRAINT grm_pk PRIMARY KEY (group_id, member_account_id)
);

CREATE TABLE [Messaging].[Message] (
	message_id INTEGER
		CONSTRAINT msg_pk PRIMARY KEY
	, message_subject NVARCHAR(45) NOT NULL
	, message_content NVARCHAR(2048) NOT NULL
	, written_date DATETIME NOT NULL
	, account_id INTEGER
		CONSTRAINT msg_aid_fk
			REFERENCES [Messaging].[UserAccount] (account_id)
		NOT NULL
	, parent_message_id INTEGER
		CONSTRAINT msg_pid_fk
			REFERENCES [Messaging].[Message] (message_id)
);

CREATE TABLE [Messaging].[MessageRecipient] (
	message_id INTEGER
		CONSTRAINT mrt_mid_fk
			REFERENCES [Messaging].[Message] (message_id)
	, account_id INTEGER
		CONSTRAINT mrt_aid_fk
			REFERENCES [Messaging].[UserAccount] (account_id)
	, CONSTRAINT mrt_pk PRIMARY KEY (message_id, account_id)
);

CREATE TABLE [Messaging].[AccountStats] (
	account_id INTEGER
		CONSTRAINT ast_pk PRIMARY KEY
	, account_username VARCHAR(356)
		CONSTRAINT ast_unm_un UNIQUE
		NOT NULL
	, messages_count SMALLINT NOT NULL
	, ranking SMALLINT NOT NULL
);
GO

-- Inserts
INSERT INTO [Messaging].[Country] (country_name)
VALUES ('Malta')
		, ('Italy')
		, ('France')
		, ('United Kingdom')
		, ('United States of America')
		, ('Spain')
		, ('India')
		, ('Bulgaria')
		, ('Norway')
		, ('Sweden')
		, ('Iceland')
		, ('Germany')
		, ('Russia')
		, ('Australia')
		, ('China')
		, ('Japan');

INSERT INTO [Messaging].[UserAccount] (account_username, account_password, 
										recovery_email, contact_number)
VALUES ('jborg', '###', 'joe.borg@gmail.com', 12345678)
		, ('labela', '###', 'lisa.abela@gmail.com', 23456781)
		, ('tzammit', '###', 'tony.zammit@gmail.com', 34567812)
		, ('pabdilla', '###', 'peter.abdilla@gmail.com', 45678123)
		, ('stanti', '###', 'sarah.tanti@gmail.com', 56781234)
		, ('agalea', '###', 'alex.galea@gmail.com', 67812345)
		, ('iabela', '###', 'ian.abela@gmail.com', 78123456);

INSERT INTO [Messaging].[UserProfile] (account_id, first_name, last_name, gender, 
										date_of_birth, country_id)
VALUES ((SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='jborg'),
		'Joe', 'Borg', 'M', '20000516', 
		(SELECT country_id FROM [Messaging].[Country] WHERE country_name='Malta'))
		, ((SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='labela'),
		'Lisa', 'Abela', 'F', '20010201', 
		 (SELECT country_id FROM [Messaging].[Country] WHERE country_name='Malta'))
		, ((SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='tzammit'),
		'Tony', 'Zammit', 'M', '19970526', 
		(SELECT country_id FROM [Messaging].[Country] WHERE country_name='Malta'))
		, ((SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='pabdilla'),
		'Peter', 'Abdilla', 'M', '19991210', 
		(SELECT country_id FROM [Messaging].[Country] WHERE country_name='Malta'))
		, ((SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='stanti'),
		'Sarah', 'Tanti', 'F', '20020102', 
		(SELECT country_id FROM [Messaging].[Country] WHERE country_name='Italy'))
		, ((SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='agalea'),
		'Alex', 'Galea', 'M', '20010627', 
		(SELECT country_id FROM [Messaging].[Country] WHERE country_name='Italy'))
		, ((SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='iabela'),
		'Ian', 'Abela', 'M', '19990201', 
		(SELECT country_id FROM [Messaging].[Country] WHERE country_name='France'));

INSERT INTO [Messaging].[ContactListEntry] (owner_account_id, contact_account_id)
VALUES ((SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='jborg') ,
		(SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='labela'))
		, ((SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='jborg') ,
		(SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='tzammit'))
		,((SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='jborg') ,
		(SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='stanti'))
		, ((SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='labela') ,
		(SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='jborg'))
		,((SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='labela') ,
		(SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='stanti'))
		, ((SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='labela') ,
		(SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='agalea'))
		, ((SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='tzammit') ,
		(SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='jborg'))
		, ((SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='tzammit') ,
		(SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='agalea'))
		, ((SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='tzammit') ,
		(SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='stanti'))
		, ((SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='tzammit') ,
		(SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='iabela'))
		, ((SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='stanti') ,
		(SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='jborg'))
		, ((SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='stanti') ,
		(SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='labela'))
		, ((SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='stanti') ,
		(SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='tzammit'))
		, ((SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='agalea') ,
		(SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='labela'))
		, ((SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='iabela') ,
		(SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='agalea'));

INSERT INTO [Messaging].[UserGroup] (group_name, owner_account_id)
VALUES ('Friends', (SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='jborg'))
		, ('Family', (SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='jborg'))
		, ('Family', (SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='stanti'));

INSERT INTO [Messaging].[GroupMember] (group_id, member_account_id)
VALUES ((SELECT group_id FROM [Messaging].[UserGroup] WHERE group_name='Friends'
		AND owner_account_id = (SELECT account_id FROM [Messaging].[UserAccount]
								WHERE account_username='jborg'))
		, (SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='labela'))
		, ((SELECT group_id FROM [Messaging].[UserGroup] WHERE group_name='Friends'
		AND owner_account_id = (SELECT account_id FROM [Messaging].[UserAccount]
								WHERE account_username='jborg'))
		, (SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='tzammit'))
		, ((SELECT group_id FROM [Messaging].[UserGroup] WHERE group_name='Family'
		AND owner_account_id = (SELECT account_id FROM [Messaging].[UserAccount]
								WHERE account_username='jborg'))
		, (SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='stanti'))
		, ((SELECT group_id FROM [Messaging].[UserGroup] WHERE group_name='Family'
		AND owner_account_id = (SELECT account_id FROM [Messaging].[UserAccount]
								WHERE account_username='stanti'))
		, (SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='labela'));

INSERT INTO [Messaging].[Message] (message_id, message_subject, message_content, written_date,
									account_id, parent_message_id)
VALUES (1, 'DevOps Challenge', 'Hi, will you be going to the Challenge?',
		'20151118 10:34:09 AM', 
		(SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='jborg'), null)
		, (2, 'PingFin 7', 'I have enrolled in the PingFin challenge, interested?',
		'20151118 11:20:10 AM', 
		(SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='labela'), null)
		, (3, 'Re: DevOps Challenge', 'Yes, shall we form a group?',
		'20151118 11:25:20 AM', 
		(SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='labela'), 1)
		, (4, 'Microsoft Imagine Cup', 'Lets form a group and participate in this challenge!',
		'20151118 11:30:10 AM', 
		(SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='tzammit'), null)
		, (5, 'Re: Microsoft Imagine Cup', 'Sure, great idea.',
		'20151118 11:35:10 AM', 
		(SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='iabela'), 4);


INSERT INTO [Messaging].[MessageRecipient] (message_id, account_id)
VALUES (1, (SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='labela'))
		, (1, (SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='tzammit'))
		, (2, (SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='stanti'))
		, (3, (SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='jborg'))
		, (4, (SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='iabela'))
		, (5, (SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='tzammit'));
GO	

WITH UserStats (account_id, account_username, message_count)
AS
	(SELECT		uac.account_id
				, uac.account_username
				, COUNT(mes.message_id)
	FROM		[Messaging].[UserAccount] uac
				JOIN [Messaging].[Message] mes
				ON (uac.account_id=mes.account_id)
	GROUP BY	uac.account_id, uac.account_username)
INSERT INTO [Messaging].[AccountStats] (account_id, account_username, messages_count, ranking)
(SELECT		account_id
			, account_username
			, message_count
			, DENSE_RANK() OVER(ORDER BY message_count DESC) AS 'Rank'
FROM		UserStats);
GO

-- Updates
INSERT INTO [Messaging].[Message] (message_id, message_subject, message_content, written_date,
									account_id, parent_message_id)
VALUES (6, 'Coding Days', 'Will you be attending coding days?',
		'20151119 12:40:10 PM', 
		(SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='jborg'), null)
		, (7, 'Re: Coding Days', 'Yes I will be coming?',
		'20151119 12:45:30 PM', 
		(SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='labela'), 6)
		, (8, 'Re: Coding Days', 'Maybe?',
		'20151119 13:40:20 PM', 
		(SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='tzammit'), 6);


INSERT INTO [Messaging].[MessageRecipient] (message_id, account_id)
VALUES (6, (SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='labela'))
		, (6, (SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='tzammit'))
		, (7, (SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='jborg'))
		, (8, (SELECT account_id FROM [Messaging].[UserAccount] WHERE account_username='jborg'));
GO	

WITH UserStats (account_id, account_username, message_count)
AS
	(SELECT		uac.account_id
				, uac.account_username
				, COUNT(mes.message_id)
	FROM		[Messaging].[UserAccount] uac
				LEFT OUTER JOIN [Messaging].[Message] mes
				ON (uac.account_id=mes.account_id)
	GROUP BY	uac.account_id, uac.account_username)
, UserStatRanking (account_id, account_username, message_count, ranking)
AS 
	(SELECT		account_id
				, account_username
				, message_count
				, DENSE_RANK() OVER (ORDER BY message_count DESC)
	FROM		[UserStats])
MERGE	[Messaging].[AccountStats] ast
		USING [UserStatRanking] usr
		ON (ast.account_id=usr.account_id)
		WHEN MATCHED THEN
			UPDATE SET	ast.messages_count=usr.message_count
						, ast.ranking=usr.ranking
		WHEN NOT MATCHED THEN
			INSERT (account_id, account_username, messages_count, ranking)
			VALUES (usr.account_id, usr.account_username, usr.message_count, usr.ranking);
GO