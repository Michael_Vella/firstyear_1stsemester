/*
	A02 ATTEMPT (REVISION)
*/

--using the proper db
USE [MCAST];
GO 


/**********************************************
				SECTION A
***********************************************/

--1 
SELECT
	  cnt.country_name,
	  pro.first_name,
	  pro.last_name
FROM 
	 Messaging.Country cnt
	 LEFT JOIN Messaging.UserProfile pro
	 ON	(cnt.country_id = pro.country_id)
ORDER BY
		cnt.country_name ASC,
		pro.last_name ASC,
		pro.first_name ASC;
GO

--2
SELECT
	acc.account_username as 'Group owner',
	grp.group_name as 'Group name',
	member.account_username as 'Group member'
FROM 
	 Messaging.UserAccount acc
	 JOIN Messaging.UserGroup grp
	 ON(acc.account_id = grp.owner_account_id)
	 JOIN Messaging.GroupMember grpM
	 ON (grp.group_id = grpM.group_id)
	 JOIN Messaging.UserAccount member
	 ON (grpM.member_account_id = member.account_id)
WHERE
	grpM.member_account_id = member.account_id
ORDER BY
	acc.account_username asc,z
	grp.group_id asc

/**********************************************
				SECTION B
***********************************************/

--3
SELECT
	acc.account_username AS 'Username',
	Count(cont.contact_account_id) AS 'Contacts'
FROM Messaging.UserAccount acc
	 JOIN Messaging.ContactListEntry cont
	 ON (acc.account_id = cont.owner_account_id)
GROUP BY 
	acc.account_username
ORDER BY
	acc.account_username ASC;
GO

--4
SELECT acc.account_username as 'Username',
	   COUNT(mem.member_account_id) as 'Listed as Group Member'
FROM Messaging.UserAccount acc
	 left JOIN Messaging.GroupMember mem
	 ON (acc.account_id = mem.member_account_id)
GROUP BY acc.account_username, mem.member_account_id
ORDER BY 
		mem.member_account_id desc,  
		acc.account_username 
GO

/**********************************************
				SECTION C
***********************************************/

--5
WITH AccountByCountry (Name, AccountAmmount)
	AS(
	SELECT cnt.country_name,
		   Count(pro.account_id)
	FROM [Messaging].[Country] cnt
		 LEFT OUTER JOIN [Messaging].[UserProfile] pro
		 ON (cnt.country_id = pro.country_id)
	GROUP BY cnt.country_name
	)

SELECT *,
	   RANK ()  OVER (PARTITION BY abc.AccountAmmount ORDER BY abc.AccountAmmount DESC) AS 'Ranks'
FROM AccountByCountry abc

