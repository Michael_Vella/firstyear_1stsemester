/*
	IICT6001 - Database Programming 
	Assignment 1 - Database Implementation [2016]


	NOTICE: SECTION D AND F ARE COMBINED
*/

--Section A (KU1 && AA1) - Database Creation 

--CREATE DATABASE + SCHEMA AND USE DATABASE 
USE [master];
GO

CREATE DATABASE [IMDB];
GO

USE [IMDB];
GO

CREATE SCHEMA [IMDB];
GO

--DELETE ALL TABLES, DELETE SCHEMA, DELETE DATABASE (IE Clean Up)
DROP TABLE [IMDB].[filmAward];
DROP TABLE [IMDB].[filmWishlist];
DROP TABLE [IMDB].[comment];
DROP TABLE [IMDB].[filmReview];
DROP TABLE [IMDB].[userAccount];
DROP TABLE [IMDB].[filmGenreAllocation];
DROP TABLE [IMDB].[filmRole];
DROP TABLE [IMDB].[profile];
DROP TABLE [IMDB].[role];
DROP TABLE [IMDB].[filmGenre];
DROP TABLE [IMDB].[award];
DROP TABLE [IMDB].[film];
DROP TABLE [IMDB].[filmStat];
DROP TABLE [IMDB].[country];
DROP TABLE [IMDB].[wishlist];
DROP TABLE [IMDB].[userProfile];
GO

DROP SCHEMA [IMDB];
GO

DROP DATABASE [IMDB];
GO

/*
	CREATE ALL TABLES 
*/
--CREATE profile
CREATE TABLE [IMDB].[profile]
	(
		profileID UNIQUEIDENTIFIER
			CONSTRAINT pro_pk PRIMARY KEY
			DEFAULT NEWID(),
		firstName NVARCHAR (256) NOT NULL,
		lastName NVARCHAR (256) NOT NULL,
		dateOfBirth DATE NOT NULL,
		gender CHAR(1) NOT NULL,
		profilePic NVARCHAR(3058) --Maximum length of a URL address
	);
GO
--CREATE role
CREATE TABLE [IMDB].[role]
	(
		roleID UNIQUEIDENTIFIER
			CONSTRAINT rol_pk PRIMARY KEY
			DEFAULT NEWID(),
		roleName NVARCHAR (256) NOT NULL
			CONSTRAINT rol_rnm_un UNIQUE
	);
GO
--CREATE filmGenre
CREATE TABLE [IMDB].[filmGenre]
	(
		genreID UNIQUEIDENTIFIER 
			CONSTRAINT gen_pk PRIMARY KEY 
			DEFAULT NEWID(),
		genreName NVARCHAR(256) NOT NULL
			CONSTRAINT gen_gnm_un UNIQUE
	);
GO
--CREATE film
CREATE TABLE [IMDB].[film]
	(
		filmID UNIQUEIDENTIFIER 
			CONSTRAINT flm_pk PRIMARY KEY
			DEFAULT NEWID(),
		filmName NVARCHAR(256) NOT NULL,
		filmYear NUMERIC(4,0) NOT NULL,
		filmPoster NVARCHAR (3058) NULL, --allows it to be null
		filmSynopsis NVARCHAR (500) NULL
	);
GO
--CREATE userProfile
CREATE TABLE [IMDB].[userProfile]
	(
		profileID UNIQUEIDENTIFIER
			CONSTRAINT usr_pk PRIMARY KEY
			DEFAULT NEWID(),
		firstName NVARCHAR(256) NOT NULL,
		lastName NVARCHAR(256) NOT NULL,
		displayName NVARCHAR(256) NOT NULL
			CONSTRAINT usr_dnm_un UNIQUE,
		avatar NVARCHAR (3058) NULL
	);
GO
--CREATE filmStat
CREATE TABLE [IMDB].[filmStat]
	(
		filmID UNIQUEIDENTIFIER 
			CONSTRAINT fStat_pk PRIMARY KEY 
			DEFAULT NEWID(),
		filmName NVARCHAR (256) NOT NULL,
		filmYear NUMERIC(4,0) NOT NULL,
		totalRating FLOAT NOT NULL,
		averageRating FLOAT NULL,
		submittedTimestamp DATETIME NOT NULL, 
		CONSTRAINT stat_flnm_flyr_un UNIQUE (filmName,filmYear)
	);
GO
--CREATE filmRole
CREATE TABLE [IMDB].[filmRole] 
	(
		profileID UNIQUEIDENTIFIER
			CONSTRAINT frl_pid_fk FOREIGN KEY
			REFERENCES [IMDB].[profile] (profileID),
		filmID UNIQUEIDENTIFIER
		CONSTRAINT frl_fid_fk FOREIGN KEY
			REFERENCES [IMDB].[film] (filmID),
		roleID UNIQUEIDENTIFIER 
		CONSTRAINT frl_rid_fk FOREIGN KEY
			REFERENCES [IMDB].[role] (roleID),
		ismainDirector CHAR (1) NOT NULL,
		ismainCast CHAR (1) NOT NULL,
		castName NVARCHAR (258) NULL,
		
		CONSTRAINT frl_pfr_pk
			PRIMARY KEY CLUSTERED (profileID, filmID, roleID)
	);
GO
--CREATE filmGenreAllocation
CREATE TABLE [IMDB].[filmGenreAllocation]
	(
		filmID UNIQUEIDENTIFIER 
			CONSTRAINT fga_fid_fk FOREIGN KEY
			REFERENCES [IMDB].[film] (filmID),
		genreID UNIQUEIDENTIFIER
			CONSTRAINT fga_gid_fk FOREIGN KEY 
			REFERENCES [IMDB].[filmGenre] (GenreID),
		CONSTRAINT fga_fg_pk 
			PRIMARY KEY CLUSTERED(filmID, genreID)
	);
GO
--TABLE userAccount
CREATE TABLE [IMDB].[userAccount] 
	(
		accountID UNIQUEIDENTIFIER 
			CONSTRAINT ua_accid_pk PRIMARY KEY
			DEFAULT NEWID(),
		accountEmail NVARCHAR (256) NOT NULL
			CONSTRAINT ua_acce_un UNIQUE,
		profileID UNIQUEIDENTIFIER NOT NULL
			CONSTRAINT ua_pid_fk FOREIGN KEY
				REFERENCES [IMDB].[userProfile] (profileID)
	);
GO
--TABLE filmReview
CREATE TABLE [IMDB].[filmReview] 
	(
		reviewID UNIQUEIDENTIFIER
			CONSTRAINT fr_rid_pk PRIMARY KEY 
			DEFAULT NEWID(),
		reviewContent NVARCHAR (500) NOT NULL,
		rateValue FLOAT NOT NULL,
		submittedTimestamp datetime  NOT NULL,
		filmID UNIQUEIDENTIFIER NOT NULL 
			CONSTRAINT fr_fid_fk FOREIGN KEY
				REFERENCES [IMDB].[film] (filmID),
		profileID UNIQUEIDENTIFIER NOT NULL 
			CONSTRAINT fr_pid_fk FOREIGN KEY 
				REFERENCES [IMDB].[userProfile] (profileID)
	);
GO
--TABLE comment
CREATE TABLE [IMDB].[comment] 
	(
		commentID UNIQUEIDENTIFIER 
		CONSTRAINT comm_cid_PK PRIMARY KEY
			DEFAULT NEWID(),
		commentContent NVARCHAR(500) NOT NULL,
		submittedTimestamp DATETIME NOT NULL,
		reviewID UNIQUEIDENTIFIER NOT NULL
			CONSTRAINT comm_rid_fk FOREIGN KEY
			REFERENCES [IMDB].[filmReview] (reviewID),
		profileID UNIQUEIDENTIFIER NOT NULL
			CONSTRAINT comm_pid_fk FOREIGN KEY
			REFERENCES [IMDB].[userProfile] (profileID),
		parentCommentID UNIQUEIDENTIFIER NULL
			CONSTRAINT comm_par_fk FOREIGN KEY
			REFERENCES [IMDB].[comment] (commentID)
	);
GO

-- Section B (KU2): DATABASE ALTERATION 

-- 1: Customise the 'film' table without removing it to set the
--    combination of film and year to be unique
ALTER TABLE [IMDB].[film]
ADD CONSTRAINT flm_flyr_un UNIQUE (filmName, filmYear);
GO


-- 2: Add a new table 'countries' with two attributes, the id and name.
--	  Customise the profile table (actors/directors) to have a FK attribute
--	  to the countries table

CREATE TABLE [IMDB].[country]
	(
		countryID UNIQUEIDENTIFIER 
			CONSTRAINT con_cid_pk PRIMARY KEY
			DEFAULT NEWID(),
		countryName NVARCHAR(256) NOT NULL, --Make Unique ??
	);
GO

--Altering table and add attribute to table
ALTER TABLE [IMDB].[profile]
ADD countryID UNIQUEIDENTIFIER NOT NULL 
				CONSTRAINT pro_cid_fk FOREIGN KEY
				REFERENCES [IMDB].[country] (countryID);
GO

--SECTION C (SE1): Database Improvment 

--Implement newly designed sections of the database  
-- TABLE wishlist
CREATE TABLE [IMDB].[wishlist]
(
	wishlistID UNIQUEIDENTIFIER 
		CONSTRAINT wls_wid_pk PRIMARY KEY
		DEFAULT NEWID(),
	wishlistName NVARCHAR(256) NOT NULL,
	userprofileID UNIQUEIDENTIFIER 
		CONSTRAINT wls_uid_fk FOREIGN KEY 
		REFERENCES [IMDB].[userprofile] (profileID)
);
GO
--TABLE filmWishList
CREATE TABLE [IMDB].[filmWishlist]
(
	wishlistID UNIQUEIDENTIFIER 
		CONSTRAINT fwl_wid_fk FOREIGN KEY 
		REFERENCES [IMDB].[wishlist] (wishlistID),
	filmID UNIQUEIDENTIFIER 
		CONSTRAINT fwl_fid_fk FOREIGN KEY
		REFERENCES [IMDB].[film] (filmID),

	CONSTRAINT fls_pk
		PRIMARY KEY CLUSTERED (wishlistID, filmID)
);
GO
--TABLE award
CREATE TABLE [IMDB].[award]
(
	awardID UNIQUEIDENTIFIER 
		CONSTRAINT awd_aid_pk PRIMARY KEY
		DEFAULT NEWID(),
	awardName NVARCHAR (256) NOT NULL
);
GO
--TABLE filmAward
CREATE TABLE [IMDB].[filmAward] 
(
	awardID UNIQUEIDENTIFIER 
		CONSTRAINT faw_aid_fk FOREIGN KEY 
		REFERENCES [IMDB].[award] (awardID),
	filmID UNIQUEIDENTIFIER  
		CONSTRAINT faw_fid_fk FOREIGN KEY 
		REFERENCES [IMDB].[film] (filmID),
	yearAwarded NUMERIC(4,0) NOT NULL,

	CONSTRAINT faw_pk
		PRIMARY KEY CLUSTERED (awardID, filmID)  
);
GO
--Section D (KU5,AA3) Database Population 

--1 Adding genres
INSERT INTO [IMDB].[filmGenre] (genreName)
	VALUES ('Action'), ('Comedy'), ('Crime'),
			('Drama'), ('Family'), ('Mystery');
GO 

--Display genre
SELECT genreName
FROM IMDB.filmGenre
ORDER BY 1 ASC;
GO

--2 Adding roles
INSERT INTO [IMDB].[role] (roleName)
	VALUES ('Director'), ('Actor');
GO

--Display Role
SELECT roleName
FROM [IMDB].[role]
ORDER BY 1 DESC;
GO

--3 Countries 
INSERT INTO [IMDB].[country] (countryName)
	VALUES ('Denmark'), ('England'), ('Sweden'),
			('United States of America');
GO

--Display Countries
Select countryName
FROM [IMDB].[country]
ORDER BY 1;
GO

--4 Profiles
INSERT INTO [IMDB].[profile] 
			(firstName,lastName,dateOfBirth,gender,countryID)
			VALUES ('Niels Arden', 'Oplev','1961-03-26','M',
					 (SELECT countryID 
					  FROM [IMDB].[country]
					  WHERE countryName = 'Denmark'
					 )
					),
					('Steven', 'Berkoff', '1937-08-03', 'M',
					  (SELECT countryID
					   FROM [IMDB].[country]
					   WHERE countryName = 'England'
					  )
					),
					('David', 'Fincher', '1962-08-28', 'M',
					  (SELECT countryID
					   FROM [IMDB].[country]
					   WHERE countryName = 'United States of America'
					  )
					),
					('Zach','Galifianakis','1969-10-01','M',
					  (SELECT countryID
					   FROM [IMDB].[country]
					   WHERE countryName = 'United States of America'
					  )
					),
					('Bradley','Cooper','1975-01-05','M',
					  (SELECT countryID
					   FROM [IMDB].[country]
					   WHERE countryName = 'United States of America'
					  )
					),
					('Rooney', 'Mara', '1985-04-17', 'F',
					  (SELECT countryID
					   FROM [IMDB].[country]
					   WHERE countryName = 'United States of America'
					  )
					 ),
					 ('Noomi','Rapace','1979-12-28','F',
					   (SELECT countryID 
					    FROM [IMDB].[country]
					    WHERE countryName = 'Sweden'
					   )
					  ),
					  ('Daniel', 'Craig', '1968-03-02', 'M',
						( SELECT countryID 
						  FROM [IMDB].[country]
						  WHERE countryName = 'England'
						 )
					  );
GO

--Display Profiles
--INCOMPLETE
SELECT pro.firstName,
	   pro.lastName,
	   pro.dateOfBirth,
	   pro.gender,
	   co.countryName
FROM  [IMDB].[profile] pro
	  JOIN [IMDB].[country] co
	  ON (pro.countryID = co.countryID)
ORDER BY
	   5 ASC, 3 ASC , 1 DESC, 2 DESC; 
GO

--5 Film Genre Allocation 

--Insert data into film table
INSERT INTO [IMDB].[film]
			(filmName,filmYear)
			VALUES
			(
			  'The Hangover', 2009
			),
			(
			  'The Hangover Part II', 2011
			),
			(
			  'The Hangover Part III', 2013
			),
			(
			  'The Girl with the Dragon Tattoo', 2009
			),
			(
			  'The Girl with the Dragon Tattoo', 2011
			);
GO

--[NOT NECESSARY] Display ALL 
SELECT *
FROM [IMDB].[film];
GO

--add genre to film name 
INSERT INTO [IMDB].[filmGenreAllocation] 
				(filmID,genreID)
				VALUES
				(
				 (SELECT filmID
				  FROM [IMDB].[film]
				  WHERE filmName ='The Hangover Part III' 
						AND filmYear = 2013 ),
				  (SELECT genreID 
				   FROM  [IMDB].[filmGenre]
				   WHERE genreName = 'Comedy'
				   )
				);
GO
INSERT INTO [IMDB].[filmGenreAllocation] 
				(filmID,genreID)
				VALUES
				(
				 (SELECT filmID
				  FROM [IMDB].[film]
				  WHERE filmName ='The Hangover Part II' 
						AND filmYear = 2011 ),
				  (SELECT genreID 
				   FROM  [IMDB].[filmGenre]
				   WHERE genreName = 'Comedy'
				   )
				);
GO
INSERT INTO [IMDB].[filmGenreAllocation] 
				(filmID,genreID)
				VALUES
				(
					(SELECT filmID
					 FROM [IMDB].[film]
					 WHERE filmName = 'The Hangover'
						   AND filmYear = 2009),
					(SELECT genreID 
					 FROM [IMDB].[filmGenre]
					 WHERE genreName = 'Comedy')
				);
GO
INSERT INTO [IMDB].[filmGenreAllocation] 
				(filmID,genreID)
				VALUES
				(
					(SELECT filmID
					 FROM [IMDB].[film]
					 WHERE filmName = 'The Hangover Part III'
						   AND filmYear = 2013),
					(SELECT genreID
					 FROM  [IMDB].[filmGenre]
					 WHERE genreName = 'Crime')
				);
GO
INSERT INTO [IMDB].[filmGenreAllocation] 
				(filmID,genreID)
				VALUES
				(
					(SELECT filmID
					 FROM [IMDB].[film]
					 WHERE filmName = 'The Girl with the Dragon Tattoo'
						   AND filmYear = 2011),
					(SELECT genreID
					FROM [IMDB].[filmGenre]
					WHERE genreName = 'Crime')
				);
GO
INSERT INTO [IMDB].[filmGenreAllocation] 
				(filmID,genreID)
				VALUES
				(
					(SELECT filmID
					 FROM [IMDB].[film]
					 WHERE filmName = 'The Girl with the Dragon Tattoo'
						   AND filmYear = 2009),
					(SELECT genreID
					FROM [IMDB].[filmGenre]
					WHERE genreName = 'Crime')
				);
GO
INSERT INTO [IMDB].[filmGenreAllocation] 
				(filmID,genreID)
				VALUES
				(
					(SELECT filmID
					 FROM [IMDB].[film]
					 WHERE filmName = 'The Girl with the Dragon Tattoo'
						   AND filmYear = 2011),
					(SELECT genreID 
					 FROM [IMDB].[filmGenre]
					 WHERE genreName = 'Drama')
				);
GO
INSERT INTO [IMDB].[filmGenreAllocation] 
				(filmID,genreID)
				VALUES
				(
					(SELECT filmID
					 FROM [IMDB].[film]
					 WHERE filmName = 'The Girl with the Dragon Tattoo'
						   AND filmYear = 2009),
					(SELECT genreID 
					 FROM [IMDB].[filmGenre]
					 WHERE genreName = 'Drama')
				);
GO
INSERT INTO [IMDB].[filmGenreAllocation] 
				(filmID,genreID)
				VALUES
				(
					(SELECT filmID
					 FROM [IMDB].[film]
					 WHERE filmName = 'The Girl with the Dragon Tattoo'
						   AND filmYear = 2011),
					(SELECT genreID
					 FROM [IMDB].[filmGenre]
					 WHERE genreName = 'Mystery')
				);
GO				
INSERT INTO [IMDB].[filmGenreAllocation] 
				(filmID,genreID)
				VALUES
				(
					(SELECT filmID
					 FROM [IMDB].[film]
					 WHERE filmName = 'The Hangover Part II'
						    AND filmYear = 2011),
					(SELECT genreID
					 FROM [IMDB].[filmGenre]
					 WHERE genreName = 'Mystery')
				);
GO
INSERT INTO [IMDB].[filmGenreAllocation] 
				(filmID,genreID)
				VALUES
				(
					(SELECT filmID
					 FROM [IMDB].[film]
					 WHERE filmName = 'The Girl with the Dragon Tattoo'
						   AND filmYear = 2009),
					(SELECT genreID
					 FROM  [IMDB].[filmGenre]
					 WHERE genreName = 'Mystery')
				);
GO

--Display films and genres
SELECT flm.filmName,
	   flm.filmYear,
	   gnr.genreName 
FROM [IMDB].[film] flm
	 JOIN [IMDB].[filmGenreAllocation]flmg
	 ON (flm.filmID = flmg.filmID)
	 RIGHT OUTER JOIN [IMDB].[filmGenre] gnr
	 ON (flmg.genreID = gnr.genreID)
ORDER BY  3 ASC, 2 desc;
GO

--6 Film Cast

--Inserting Data in to the filmRole table
INSERT INTO [IMDB].[filmRole]
			(filmID, profileID,  roleID, ismainDirector,ismainCast,castName)
			VALUES
			(
				(
				 SELECT filmID
				 FROM [IMDB].[film]
				 WHERE filmName = 'The Girl with the Dragon Tattoo'
					   AND filmYear = 2009
				 ),
				(
				  SELECT profileID
				  FROM [IMDB].[profile]
				  WHERE firstName = 'Niels Arden'
						AND lastName = 'Oplev' 
				),
				(
				  SELECT roleID 
				  FROM [IMDB].[role]
				  WHERE roleName = 'Director'
				),
				1,
				0,
				NULL			
			),
			(
			   (
				 SELECT filmID
				 FROM [IMDB].[film]
				 WHERE filmName = 'The Girl with the Dragon Tattoo'
					   AND filmYear = 2009
				 ),
				(
				  SELECT profileID
				  FROM [IMDB].[profile]
				  WHERE firstName = 'Noomi'
						AND lastName = 'Rapace' 
				),
				(
				  SELECT roleID 
				  FROM [IMDB].[role]
				  WHERE roleName = 'Actor'
				),
				0,
				1,
				'Lisbeth Salander'	
			),
			(
			   (
				 SELECT filmID
				 FROM [IMDB].[film]
				 WHERE filmName = 'The Hangover'
					   AND filmYear = 2009
				 ),
				(
				  SELECT profileID
				  FROM [IMDB].[profile]
				  WHERE firstName = 'Bradley'
						AND lastName = 'Cooper' 
				),
				(
				  SELECT roleID 
				  FROM [IMDB].[role]
				  WHERE roleName = 'Actor'
				),
				0,
				1,
				'Phil'
			),
			(
			   (
				 SELECT filmID
				 FROM [IMDB].[film]
				 WHERE filmName = 'The Hangover'
					   AND filmYear = 2009
				 ),
				(
				  SELECT profileID
				  FROM [IMDB].[profile]
				  WHERE firstName = 'Zach'
						AND lastName = 'Galifianakis' 
				),
				(
				  SELECT roleID 
				  FROM [IMDB].[role]
				  WHERE roleName = 'Actor'
				),
				0,
				1,
				'Alan'
			),
			(
			   (
				 SELECT filmID
				 FROM [IMDB].[film]
				 WHERE filmName = 'The Girl with the Dragon Tattoo'
					   AND filmYear = 2011
				 ),
				(
				  SELECT profileID
				  FROM [IMDB].[profile]
				  WHERE firstName = 'David'
						AND lastName = 'Fincher' 
				),
				(
				  SELECT roleID 
				  FROM [IMDB].[role]
				  WHERE roleName = 'Director'
				),
				1,
				0,
				Null
			),
			(
			   (
				 SELECT filmID
				 FROM [IMDB].[film]
				 WHERE filmName = 'The Girl with the Dragon Tattoo'
					   AND filmYear = 2011
				 ),
				(
				  SELECT profileID
				  FROM [IMDB].[profile]
				  WHERE firstName = 'Steven'
						AND lastName = 'Berkoff' 
				),
				(
				  SELECT roleID 
				  FROM [IMDB].[role]
				  WHERE roleName = 'Actor'
				),
				0,
				1,
				'Frode'
			),
			(
			   (
				 SELECT filmID
				 FROM [IMDB].[film]
				 WHERE filmName = 'The Girl with the Dragon Tattoo'
					   AND filmYear = 2011
				 ),
				(
				  SELECT profileID
				  FROM [IMDB].[profile]
				  WHERE firstName = 'Daniel'
						AND lastName = 'Craig' 
				),
				(
				  SELECT roleID 
				  FROM [IMDB].[role]
				  WHERE roleName = 'Actor'
				),
				0,
				1,
				'Mikael Blomvist'
			),
			(
			   (
				 SELECT filmID
				 FROM [IMDB].[film]
				 WHERE filmName = 'The Girl with the Dragon Tattoo'
					   AND filmYear = 2011
				 ),
				(
				  SELECT profileID
				  FROM [IMDB].[profile]
				  WHERE firstName = 'Rooney'
						AND lastName = 'Mara' 
				),
				(
				  SELECT roleID 
				  FROM [IMDB].[role]
				  WHERE roleName = 'Actor'
				),
				0,
				1,
				'Lisbeth Salander'
			),
			(
			   (
				 SELECT filmID
				 FROM [IMDB].[film]
				 WHERE filmName = 'The Hangover Part II'
					   AND filmYear = 2011
				 ),
				(
				  SELECT profileID
				  FROM [IMDB].[profile]
				  WHERE firstName = 'Bradley'
						AND lastName = 'Cooper' 
				),
				(
				  SELECT roleID 
				  FROM [IMDB].[role]
				  WHERE roleName = 'Actor'
				),
				0,
				1,
				'Phil'
			),
			(
			   (
				 SELECT filmID
				 FROM [IMDB].[film]
				 WHERE filmName = 'The Hangover Part II'
					   AND filmYear = 2011
				 ),
				(
				  SELECT profileID
				  FROM [IMDB].[profile]
				  WHERE firstName = 'Zach'
						AND lastName = 'Galifianakis' 
				),
				(
				  SELECT roleID 
				  FROM [IMDB].[role]
				  WHERE roleName = 'Actor'
				),
				0,
				1,
				'Alan'
			),
			(
			   (
				 SELECT filmID
				 FROM [IMDB].[film]
				 WHERE filmName = 'The Hangover Part III'
					   AND filmYear = 2013
				 ),
				(
				  SELECT profileID
				  FROM [IMDB].[profile]
				  WHERE firstName = 'Bradley'
						AND lastName = 'Cooper' 
				),
				(
				  SELECT roleID 
				  FROM [IMDB].[role]
				  WHERE roleName = 'Actor'
				),
				0,
				1,
				'Phil'
			),
			(
			   (
				 SELECT filmID
				 FROM [IMDB].[film]
				 WHERE filmName = 'The Hangover Part III'
					   AND filmYear = 2013
				 ),
				(
				  SELECT profileID
				  FROM [IMDB].[profile]
				  WHERE firstName = 'Zach'
						AND lastName = 'Galifianakis' 
				),
				(
				  SELECT roleID 
				  FROM [IMDB].[role]
				  WHERE roleName = 'Actor'
				),
				0,
				1,
				'Alan'
			);
GO

--Display Film Cast
--INCOMPLETE
SELECT flm.filmName,
	   flm.filmYear,
	   pro.firstName,
	   pro.lastName,
	   rl.roleName,
	   ismainDirector,
	   ismainCast,
	   castName
FROM [IMDB].[filmRole] flmr
	 JOIN [IMDB].[Profile] pro
	 ON (flmr .profileID = pro.profileID)
	 JOIN [IMDB].[film] flm
	 ON (flmr.filmID = flm.filmID)
	 JOIN [IMDB].[role] rl
	 ON (flmr.roleID = rl.roleID)
ORDER BY  2 ASC, 6 DESC, 4 DESC, 8 ASC ,3 DESC;
GO

--7 User Accounts

--Inserting data into the userProfile table
INSERT INTO [IMDB].[userProfile]
			(firstName, lastName, displayName)
			VALUES 
				('Lisa', 'Galea', 'lisag'),
				('Tony', 'Borg' ,'tonyb'),
				('Joe', 'Borg', 'joeb'); 

GO
--Inserting data into the userAccounts table
INSERT INTO [IMDB].[userAccount] 
			(profileID, accountEmail )
			VALUES (
					 (SELECT profileID
					  FROM [IMDB].userProfile
					  WHERE displayName = 'lisag'
					 ),
					 'lisag@gmail.com'
					),
					(
					 (SELECT profileID
					  FROM [IMDB].userProfile
					  WHERE displayName = 'tonyb'
					 ),
					 'tonyborg@gmail.com'
					 ),
					(
					  (
					  SELECT profileID
					  FROM [IMDB].userProfile
					  WHERE displayName = 'joeb'
					  ),
					 'joeborg@gmail.com'
					 );
GO
-- Display User Accounts
SELECT usr.firstName,
       usr.lastName,
	   usr.displayName,
	   acc.accountEmail
FROM [IMDB].[userProfile] usr
	 JOIN [IMDB].[userAccount] acc
	 ON (usr.profileID = acc.profileID)
ORDER BY 2 DESC;
GO

--8 Reviews
INSERT INTO [IMDB].[filmReview]
		    (reviewContent, rateValue, submittedTimestamp,filmID,profileID)
			VALUES
			(
				'Looking forwardd for the sequel', 
				5.0, 
				CURRENT_TIMESTAMP, 
				(SELECT filmID 
				 FROM [IMDB].[film]
				 WHERE filmName = 'The Girl with the Dragon Tattoo'
					   AND filmYear = 2011 
				 ),
				(SELECT profileID 
				 FROM [IMDB].[userProfile]
				 WHERE displayName = 'lisag'
				)
			),
			(
				'Could not stop laughing', 
				5.0, 
				CURRENT_TIMESTAMP, 
				(SELECT filmID 
				 FROM [IMDB].[film]
				 WHERE filmName = 'The Hangover'
					   AND filmYear = 2009 
				 ),
				(SELECT profileID 
				 FROM [IMDB].[userProfile]
				 WHERE displayName = 'lisag'
				)
			),
			(
				'Better than the first movie', 
				4.0, 
				CURRENT_TIMESTAMP, 
				(SELECT filmID 
				 FROM [IMDB].[film]
				 WHERE filmName = 'The Girl with the Dragon Tattoo'
					   AND filmYear = 2011 
				 ),
				(SELECT profileID 
				 FROM [IMDB].[userProfile]
				 WHERE displayName = 'tonyb'
				)
			),
			(
				'Epic', 
				5.0, 
				CURRENT_TIMESTAMP, 
				(SELECT filmID 
				 FROM [IMDB].[film]
				 WHERE filmName = 'The Hangover'
					   AND filmYear = 2009 
				 ),
				(SELECT profileID 
				 FROM [IMDB].[userProfile]
				 WHERE displayName = 'joeb'
				)
			);
GO

-- Display Reviews
SELECT flm.filmName,
       flm.filmYear,
	   flr.reviewContent,
	   flr.rateValue,
	   usr.displayName
FROM [IMDB].[filmReview] flr
	 JOIN [IMDB].[film] flm
	 ON (flr.filmID = flm.filmID)
	 JOIN [IMDB].[userProfile] usr
	 ON (flr.profileID = usr.profileID);
GO

--9 Comments
INSERT INTO [IMDB].[comment]
			(commentContent, submittedTimestamp, reviewID, profileID, parentCommentID) 
			VALUES 
			('I disagree', 
			  CURRENT_TIMESTAMP,
			  (
			    SELECT reviewID
				FROM [IMDB].[filmReview]
				WHERE reviewContent = 'Better than the first movie' 
			   ),
			  (
			   SELECT profileID
			   FROM [IMDB].[userProfile]
			   WHERE displayName = 'joeb'
			   ),
			  (NULL)
			  );
GO

INSERT INTO [IMDB].[comment]
			(commentContent, submittedTimestamp, reviewID, profileID, parentCommentID) 
			VALUES 
			('Me too', 
			  CURRENT_TIMESTAMP,
			  (
			    SELECT reviewID
				FROM [IMDB].[filmReview]
				WHERE reviewContent = 'Could not stop laughing' 
			   ),
			  (
			   SELECT profileID
			   FROM [IMDB].[userProfile]
			   WHERE displayName = 'joeb'
			   ),
			  (NULL)
			  );
GO

INSERT INTO [IMDB].[comment]
			(commentContent, submittedTimestamp, reviewID, profileID, parentCommentID) 
			VALUES 
			('Even the credits were funny', 
			  CURRENT_TIMESTAMP,
			  (
			    SELECT reviewID
				FROM [IMDB].[filmReview]
				WHERE reviewContent = 'Could not stop laughing' 
			   ),
			  (
			   SELECT profileID
			   FROM [IMDB].[userProfile]
			   WHERE displayName = 'tonyb'
			   ),
			  (
				SELECT commentID 
				FROM [IMDB].[comment]
				WHERE commentContent =  'Me too'
			  )
			 );
GO

--Display Comments
SELECT flmr.reviewContent AS 'Review', 
    comm.commentContent AS 'Comment', 
    usr.displayName AS 'Comment Author', 
    commp.commentContent AS 'Reply Comment',
    prof.displayName AS 'Reply Author'
FROM [IMDB].[filmReview] flmr
	   JOIN [IMDB].[comment] comm
	   ON (flmr.reviewID = comm.reviewID)
	   left JOIN [IMDB].[comment]commp
	   ON (commp.parentCommentID = comm.commentID)
	   left JOIN [IMDB].[userProfile] usr
	   ON (comm.profileID = usr.profileID)
	   left JOIN [IMDB].[userProfile] prof
	   ON (commp.profileID = prof.profileID)
ORDER BY 1 asc;
GO

--10 Film Stats  
/*
	 
	The below gave the same results as the merge was commented out for further review 

*/
--INSERT INTO [IMDB].[filmStat]
--			(filmID, filmName, filmYear,totalRating, averageRating, submittedTimestamp)
--			VALUES
--			( --1
--				(
--				 SELECT flm.filmID
--				 FROM [IMDB].[film] flm				 
--				 WHERE flm.filmName = 'The Hangover Part III'
--				        AND flm.filmYear = 2013
--				),
--				(
--				 SELECT flm.filmName
--				 FROM [IMDB].[film] flm
--				 WHERE flm.filmName = 'The Hangover Part III'
--				       AND flm.filmYear = 2013
--				),
--				(
--				 SELECT flm.filmYear
--				 FROM [IMDB].[film] flm
--				 WHERE flm.filmName = 'The Hangover Part III'
--				       AND flm.filmYear = 2013
--				),
--			    (
--				SELECT COUNT(flmr.rateValue) as 'total rating '
--				 FROM [IMDB].[filmReview] flmr
--					  JOIN [IMDB].[film] flm
--					  ON (flmr.filmID = flm.filmID)
--				 WHERE flm.filmName = 'The Hangover part III' 
--						AND flm.filmYear = 2013
--				),
--				(
--				 SELECT AVG(flmr.rateValue) as 'average rating '
--				 FROM [IMDB].[filmReview] flmr
--					  JOIN [IMDB].[film] flm
--					  ON (flmr.filmID = flm.filmID)
--				 WHERE flm.filmName = 'The Hangover part III' 
--						AND flm.filmYear = 2013
--				),
--				CURRENT_TIMESTAMP
--			),
--			( --2
--				(
--				 SELECT flm.filmID
--				 FROM [IMDB].[film] flm				 
--				 WHERE flm.filmName = 'The Girl with the Dragon Tattoo'
--				        AND flm.filmYear = 2011
--				),
--				(
--				 SELECT flm.filmName
--				 FROM [IMDB].[film] flm
--				 WHERE flm.filmName = 'The Girl with the Dragon Tattoo'
--				       AND flm.filmYear = 2011
--				),
--				(
--				 SELECT flm.filmYear
--				 FROM [IMDB].[film] flm
--				 WHERE flm.filmName = 'The Girl with the Dragon Tattoo'
--				       AND flm.filmYear = 2011
--				),
--			    (
--				 SELECT COUNT(flmr.rateValue) as 'total rating '
--				 FROM [IMDB].[filmReview] flmr
--					  JOIN [IMDB].[film] flm
--					  ON (flmr.filmID = flm.filmID)
--				 WHERE flm.filmName = 'The Girl with the Dragon Tattoo' 
--						AND flm.filmYear = 2011
--				),
--				(
--				 SELECT AVG(flmr.rateValue) as 'average rating '
--				 FROM [IMDB].[filmReview] flmr
--					  JOIN [IMDB].[film] flm
--					  ON (flmr.filmID = flm.filmID)
--				 WHERE flm.filmName = 'The Girl with the Dragon Tattoo' 
--						AND flm.filmYear = 2011
--				),
--				CURRENT_TIMESTAMP
--			),
--			( --3
--				(
--				 SELECT flm.filmID
--				 FROM [IMDB].[film] flm				 
--				 WHERE flm.filmName = 'The Hangover Part II'
--				        AND flm.filmYear = 2011
--				),
--				(
--				 SELECT flm.filmName
--				 FROM [IMDB].[film] flm
--				 WHERE flm.filmName = 'The Hangover Part II'
--				       AND flm.filmYear = 2011
--				),
--				(
--				 SELECT flm.filmYear
--				 FROM [IMDB].[film] flm
--				 WHERE flm.filmName = 'The Hangover Part II'
--				        AND flm.filmYear = 2011
--				),
--			    (
--				 SELECT COUNT(flmr.rateValue) as 'total rating '
--				 FROM [IMDB].[filmReview] flmr
--					  JOIN [IMDB].[film] flm
--					  ON (flmr.filmID = flm.filmID)
--				 WHERE flm.filmName = 'The Hangover Part II'
--				        AND flm.filmYear = 2011
--				),
--				(
--				 SELECT AVG(flmr.rateValue) as 'average rating '
--				 FROM [IMDB].[filmReview] flmr
--					  JOIN [IMDB].[film] flm
--					  ON (flmr.filmID = flm.filmID)
--				 WHERE flm.filmName = 'The Hangover Part II'
--				        AND flm.filmYear = 2011
--				),
--				CURRENT_TIMESTAMP
--			),
--			( --4
--				(
--				 SELECT flm.filmID
--				 FROM [IMDB].[film] flm				 
--				 WHERE flm.filmName = 'The Hangover'
--				        AND flm.filmYear = 2009
--				),
--				(
--				 SELECT flm.filmName
--				 FROM [IMDB].[film] flm
--				 WHERE flm.filmName = 'The Hangover'
--				       AND flm.filmYear = 2009
--				),
--				(
--				 SELECT flm.filmYear
--				 FROM [IMDB].[film] flm
--				 WHERE flm.filmName = 'The Hangover'
--				        AND flm.filmYear = 2009
--				),
--			    (
--				SELECT COUNT(flmr.rateValue) as 'total rating '
--				 FROM [IMDB].[filmReview] flmr
--					  JOIN [IMDB].[film] flm
--					  ON (flmr.filmID = flm.filmID)
--				 WHERE flm.filmName = 'The Hangover'
--				        AND flm.filmYear = 2009
--				),
--				(
--				 SELECT AVG(flmr.rateValue) as 'average rating '
--				 FROM [IMDB].[filmReview] flmr
--					  JOIN [IMDB].[film] flm
--					  ON (flmr.filmID = flm.filmID)
--				 WHERE flm.filmName = 'The Hangover'
--				        AND flm.filmYear = 2009
--				),
--				CURRENT_TIMESTAMP
--			),
--			( --5
--				(
--				 SELECT flm.filmID
--				 FROM [IMDB].[film] flm				 
--				 WHERE flm.filmName = 'The Girl with the Dragon Tattoo'
--				        AND flm.filmYear = 2009
--				),
--				(
--				 SELECT flm.filmName
--				 FROM [IMDB].[film] flm
--				 WHERE flm.filmName = 'The Girl with the Dragon Tattoo'
--				       AND flm.filmYear = 2009
--				),
--				(
--				 SELECT flm.filmYear
--				 FROM [IMDB].[film] flm
--				 WHERE flm.filmName = 'The Girl with the Dragon Tattoo'
--				       AND flm.filmYear = 2009
--				),
--			    (
--				 SELECT COUNT(flmr.rateValue) as 'total rating '
--				 FROM [IMDB].[filmReview] flmr
--					  JOIN [IMDB].[film] flm
--					  ON (flmr.filmID = flm.filmID)
--				 WHERE flm.filmName = 'The Girl with the Dragon Tattoo' 
--						AND flm.filmYear = 2009
--				),
--				(
--				 SELECT AVG(flmr.rateValue) as 'average rating '
--				 FROM [IMDB].[filmReview] flmr
--					  JOIN [IMDB].[film] flm
--					  ON (flmr.filmID = flm.filmID)
--				 WHERE flm.filmName = 'The Girl with the Dragon Tattoo' 
--						AND flm.filmYear = 2009
--				),
--				CURRENT_TIMESTAMP
--			);
--GO
INSERT INTO [IMDB].[filmStat]  -- The above was replaced with the below due to confusion and in order to simplfy the work
			(filmName, filmYear, totalRating, averageRating, submittedTimestamp)
			VALUES 
					(
					 'The Hangover Part III', 
					  2013, 
					  0, 
					  NULL,
					  CURRENT_TIMESTAMP
					),
					(
					 'The Girl with the Dragon Tattoo', 
					 2011, 
					 2, 
					 4.50, 
					 CURRENT_TIMESTAMP
					),
					(
					  'The Hangover Part II', 
					  2011,
					  0, 
					  NULL, 
					  CURRENT_TIMESTAMP
					),
				    ( 
					  'The Hangover', 
					  2009, 
					  2, 
					  5.00, 
					  CURRENT_TIMESTAMP
					),
					(
					  'The Girl with the Dragon Tattoo', 
					  2009, 
					  0, 
					  NULL, 
					  CURRENT_TIMESTAMP
					);
GO

--Display Film Stat

SELECT stat.filmName,
       stat.filmYear,
	   stat.totalRating,
	   stat.averageRating
FROM [IMDB].[filmStat] stat
ORDER BY 2 DESC,
		 3 DESC ;
GO 

-- DELETE ALL CREATED DATA
DELETE [IMDB].[comment];
DELETE [IMDB].[filmReview];
DELETE [IMDB].[filmGenreAllocation];
DELETE [IMDB].[filmGenre];
DELETE [IMDB].[filmRole];
DELETE [IMDB].[filmAward];
DELETE [IMDB].[filmStat];
DELETE [IMDB].[film];
DELETE [IMDB].[profile];
DELETE [IMDB].[filmWishlist];
DELETE [IMDB].[userAccount];
DELETE [IMDB].[userProfile];
DELETE [IMDB].[country];
DELETE [IMDB].[award];
DELETE [IMDB].[role];
GO

-- SECTION E (SE3) : Advanced Database Population 
-- 1 Film Stats = SECTION D.10 Film Stats 

-- 2 Additional Reviews 
-- There are 3 additional ones when compared to 
-- the previous report 

--Adding new reviews
INSERT INTO [IMDB].[filmReview]
			(reviewContent, rateValue, submittedTimestamp,filmID,profileID)
			VALUES 
			(
			  'Great movie',
			  4.0,
			  CURRENT_TIMESTAMP,
			  (
			  SELECT flm.filmID
			  FROM [IMDB].[film] flm
			  WHERE flm.filmName = 'The Girl with the Dragon Tattoo'
			        AND flm.filmYear = 2009
			  ),
			  (
			   SELECT usr.profileID 
			   FROM  [IMDB].[userProfile] usr
			   WHERE usr.displayName = 'lisag'
			  )
			),
		    (
			 'Faithful to books',
			 5.0,
			 CURRENT_TIMESTAMP,
			 (
			  SELECT flm.filmID
			  FROM [IMDB].[film] flm
			  WHERE flm.filmName = 'The Girl with the Dragon Tattoo'
					AND flm.filmYear = 2009
			 ),
			 (
			  SELECT usr.profileID 
			  FROM [IMDB].[userProfile] usr
			  WHERE usr.displayName = 'joeb'
			 )
			),
			(
			 'Very funny',
			 4.0,
			 CURRENT_TIMESTAMP,
			 (
			   SELECT flm.filmID
			   FROM [IMDB].[film] flm 
			   WHERE flm.filmName = 'The Hangover Part II'
					 AND flm.filmYear = 2011
			 ),
			 (
			   SELECT usr.profileID 
			   FROM  [IMDB].[userProfile] usr
			   WHERE usr.displayName = 'lisag'
			  )
			);
GO

--Display new reviews
SELECT flm.filmName,
       flm.filmYear,
	   flr.reviewContent,
	   flr.rateValue ,
	   usr.displayName
FROM [IMDB].[filmReview] flr
	 JOIN [IMDB].[film] flm
	 ON (flr.filmID = flm.filmID)
	 JOIN [IMDB].[userProfile] usr
	 ON (flr.profileID = usr.profileID)
ORDER BY 2 DESC, 1 ASC;
GO

-- 3 Merged Stats

--Creation of CTE
WITH FilmStats (filmID, filmName, filmYear, averageRating, totalRating)
 AS (
	SELECT flm.filmId AS FilmID, 
	  (
	   SELECT filmName
	   FROM [IMDB].[Film]
       WHERE FilmID = flm.filmID
	  ) AS'Film Name',
		flm.filmYear AS 'Film Year',
		AVG(fr.rateValue) AS 'Average Rating',
		COUNT(fr.rateValue) AS 'Total Rating'
	FROM [IMDB].[Film] flm
			 LEFT JOIN [IMDB].[FilmReview] fr
			 ON(flm.filmID = fr.filmID)
	GROUP BY flm.filmId, flm.filmYear
	)

MERGE [IMDB].[FilmStat] flmst
   USING FilmStats fs --CTE
	 ON (fs.filmName = flmst.filmName 
		 AND fs.filmYear = flmst.filmYear)
   WHEN MATCHED THEN 
        UPDATE SET flmst.averageRating = fs.averageRating,
				   flmst.totalRating = fs.totalRating
   WHEN NOT MATCHED THEN
INSERT (filmId, filmName, FilmYear, averageRating, totalRating)
	VALUES 
		(fs.FilmID, fs.FilmName, fs.FilmYear, fs.AverageRating,fs.totalRating);
GO

SELECT filmName AS 'Film Name',
       filmYear AS 'Film Year',
	   totalRating AS 'Total Rating',
	   averageRating AS 'Average Rating'
FROM [IMDB].[FilmStat]
ORDER BY 2 DESC, 3 DESC;
GO

--Section F (KU3) Reporting using Built-in functions using multiple tables

--1 Gerne Film Total
SELECT gnr.genreName,
	   ISNULL(COUNT (flmgr.filmID),0) AS 'Film Count'
FROM  [IMDB].[filmGenre] gnr
	   LEFT OUTER JOIN [IMDB].[filmGenreAllocation] flmgr 
	   ON (gnr.genreID = flmgr.genreID)
GROUP BY gnr.genreName
ORDER BY 2 DESC, 1 ASC;
GO

--2 User Activity Total  
-- INCOMPLETE
SELECT acc.accountEmail,
	   COUNT(rev.reviewID) AS 'Review count ',
	   COUNT(comm.commentID) AS 'Comment count'
FROM [IMDB].[filmReview] rev
	 LEFT JOIN [IMDB].[userAccount] acc
	 ON (rev.profileID = acc.profileID)
	 LEFT JOIN [IMDB].[comment] comm
	 ON (acc.profileID = comm.profileID)
GROUP BY acc.accountEmail;

-- Section G (AA4) - Advance Data Reporting
-- 1 Film Cast Page 2

-- ORDER BY based on SECTION D. 6 Film Cast 
-- INCOMPLETE
WITH ActMovie (filmYear,film, actorName, actorSurname, actorRole, castName, rowNum)
AS (
	SELECT 
	   flm.filmYear,
	   flm.filmName,
	   pro.firstName,
	   pro.lastName,
	   rl.roleName,
	   flmr.castName,
	   ROW_NUMBER() OVER (ORDER BY flm.filmYear ASC, rl.roleName DESC, pro.lastName DESC, flmr.castName ASC ,pro.firstName DESC )
FROM [IMDB].[filmRole] flmr
	 JOIN [IMDB].[Profile] pro
	 ON (flmr .profileID = pro.profileID)
	 JOIN [IMDB].[film] flm
	 ON (flmr.filmID = flm.filmID)
	 JOIN [IMDB].[role] rl
	 ON (flmr.roleID = rl.roleID)
)
SELECT filmYear,
	   film, 
	   actorName, 
	   actorSurname,
	   actorRole, 
	   castName,
	   rowNum
FROM ActMovie
ORDER BY rowNum ASC
		 OFFSET 5 ROWS FETCH NEXT 5 ROWS ONLY;

--2 Average Rating Per Genre
SELECT gnr.genreName,
	   CONVERT(numeric(3,2),ISNULL(AVG(rev.rateValue),0)) AS 'Average Rating'
FROM  [IMDB].[filmGenre] gnr
	   LEFT OUTER JOIN [IMDB].[filmGenreAllocation] flmgr 
	   ON (gnr.genreID = flmgr.genreID)
	   LEFT JOIN [IMDB].[filmReview] rev
	   ON (flmgr.filmID = rev.filmID)
GROUP BY gnr.genreName
ORDER BY 1 ASC;
GO