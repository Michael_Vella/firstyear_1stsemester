/*
	MCAST IICT
	IICT6001 - Database Programming 01
	Assignment 02 Setup Script
*/

-- Clean-up
USE [MCAST];
	
DROP TABLE [a02].[comments];
DROP TABLE [a02].[filmReviews];
DROP TABLE [a02].[userAccounts];
DROP TABLE [a02].[userProfiles];
DROP TABLE [a02].[filmGenreAllocations];
DROP TABLE [a02].[filmGenres];
DROP TABLE [a02].[filmRoleAllocations];
DROP TABLE [a02].[filmRoles];
DROP TABLE [a02].[films];
DROP TABLE [a02].[profiles];
DROP TABLE [a02].[countries];
GO

DROP SCHEMA [a02];
GO

-- Section A
USE [MCAST];
GO

CREATE SCHEMA [a02];
GO

CREATE TABLE [a02].[countries] (
	country_id UNIQUEIDENTIFIER PRIMARY KEY
		DEFAULT NEWID()
	, country_name NVARCHAR(256) UNIQUE NOT NULL
);
GO

CREATE TABLE [a02].[userProfiles] (
	profile_id UNIQUEIDENTIFIER PRIMARY KEY DEFAULT NEWID()
	, first_name NVARCHAR(256) NOT NULL
	, last_name NVARCHAR(256) NOT NULL
	, display_name NVARCHAR(256) UNIQUE NOT NULL
	, avatar NVARCHAR(3058) DEFAULT NULL
);

CREATE TABLE [a02].[userAccounts] (
	account_id UNIQUEIDENTIFIER PRIMARY KEY DEFAULT NEWID()
	, account_email NVARCHAR(356) UNIQUE NOT NULL
	, profile_id UNIQUEIDENTIFIER UNIQUE NOT NULL
		REFERENCES [a02].[userProfiles] (profile_id)
);

CREATE TABLE [a02].[films] (
	film_id UNIQUEIDENTIFIER PRIMARY KEY DEFAULT NEWID()
	, film_name NVARCHAR(256) NOT NULL
	, film_year NUMERIC(4,0) NOT NULL
	, film_poster NVARCHAR(3058) DEFAULT NULL
	, film_synopsis TEXT DEFAULT NULL
	, CONSTRAINT flm_fnm_un UNIQUE(film_name, film_year)
);

CREATE TABLE [a02].[filmGenres] (
	genre_id UNIQUEIDENTIFIER PRIMARY KEY DEFAULT NEWID()
	, genre_name NVARCHAR(256) UNIQUE NOT NULL
);

CREATE TABLE [a02].[filmGenreAllocations] (
	film_id UNIQUEIDENTIFIER REFERENCES [a02].[films] (film_id)
	, genre_id UNIQUEIDENTIFIER REFERENCES [a02].[filmGenres] (genre_id)
	, PRIMARY KEY (film_id, genre_id)
);

CREATE TABLE [a02].[filmReviews] (
	review_id UNIQUEIDENTIFIER PRIMARY KEY DEFAULT NEWID()
	, review_content NVARCHAR(MAX) NOT NULL
	, rate_value NUMERIC(3,2) NOT NULL
	, film_id UNIQUEIDENTIFIER NOT NULL REFERENCES [a02].[films] (film_id)
	, profile_id UNIQUEIDENTIFIER NOT NULL REFERENCES [a02].[userProfiles] (profile_id)
	, submitted_timestamp DATETIMEOFFSET DEFAULT CURRENT_TIMESTAMP NOT NULL
);

CREATE TABLE [a02].[comments] (
	comment_id UNIQUEIDENTIFIER PRIMARY KEY DEFAULT NEWID()
	, comment_content NVARCHAR(MAX) NOT NULL
	, review_id UNIQUEIDENTIFIER NOT NULL REFERENCES [a02].[filmReviews] (review_id)
	, profile_id UNIQUEIDENTIFIER NOT NULL REFERENCES [a02].[userProfiles] (profile_id)
	, parent_comment_id UNIQUEIDENTIFIER REFERENCES [a02].[comments] (comment_id)
	, submnitted_timestamp DATETIMEOFFSET DEFAULT CURRENT_TIMESTAMP NOT NULL
);

CREATE TABLE [a02].[profiles] (
	profile_id UNIQUEIDENTIFIER PRIMARY KEY DEFAULT NEWID()
	, first_name NVARCHAR(256) NOT NULL
	, last_name NVARCHAR(256) NOT NULL
	, date_of_birth DATE NOT NULL
	, gender CHAR(1) NOT NULL
	, avatar NVARCHAR(3058) DEFAULT NULL
	, country_id UNIQUEIDENTIFIER NOT NULL
		REFERENCES [a02].[countries] (country_id)
);

CREATE TABLE [a02].[filmRoles] (
	role_id UNIQUEIDENTIFIER PRIMARY KEY DEFAULT NEWID()
	, role_name NVARCHAR(256) UNIQUE NOT NULL
);

CREATE TABLE [a02].[filmRoleAllocations] (
	profile_id UNIQUEIDENTIFIER REFERENCES [a02].[profiles] (profile_id)
	, film_id UNIQUEIDENTIFIER REFERENCES [a02].[films] (film_id)
	, role_id UNIQUEIDENTIFIER REFERENCES [a02].[filmRoles] (role_id)
	, is_main_director BIT NOT NULL
	, is_main_cast BIT NOT NULL
	, cast_name NVARCHAR(256) DEFAULT NULL
	, PRIMARY KEY (film_id, profile_id, role_id)
);

INSERT INTO [a02].[filmGenres] (genre_name)
VALUES ('Action'), ('Crime'), ('Comedy'), ('Drama'), ('Family'), ('Mystery');

INSERT INTO [a02].[films] (film_name, film_year)
VALUES ('The Hangover', 2009)
		, ('The Hangover Part II', 2011)
		, ('The Hangover Part III', 2013)
		, ('The Girl with the Dragon Tattoo', 2009)
		, ('The Girl with the Dragon Tattoo', 2011);

INSERT INTO [a02].[filmGenreAllocations] (film_id, genre_id)
VALUES ((SELECT film_id FROM [a02].[films] WHERE film_name='The Hangover' AND film_year=2009)
		, (SELECT genre_id FROM [a02].[filmGenres] WHERE genre_name='Comedy'))
	, ((SELECT film_id FROM [a02].[films] WHERE film_name='The Hangover Part II' AND film_year=2011)
		, (SELECT genre_id FROM [a02].[filmGenres] WHERE genre_name='Comedy'))
	, ((SELECT film_id FROM [a02].[films] WHERE film_name='The Hangover Part II' AND film_year=2011)
		, (SELECT genre_id FROM [a02].[filmGenres] WHERE genre_name='Mystery'))
	, ((SELECT film_id FROM [a02].[films] WHERE film_name='The Hangover Part III' AND film_year=2013)
		, (SELECT genre_id FROM [a02].[filmGenres] WHERE genre_name='Comedy'))
	, ((SELECT film_id FROM [a02].[films] WHERE film_name='The Hangover Part III' AND film_year=2013)
		, (SELECT genre_id FROM [a02].[filmGenres] WHERE genre_name='Crime'))
	, ((SELECT film_id FROM [a02].[films] WHERE film_name='The Girl with the Dragon Tattoo' AND film_year=2009)
		, (SELECT genre_id FROM [a02].[filmGenres] WHERE genre_name='Crime'))
	, ((SELECT film_id FROM [a02].[films] WHERE film_name='The Girl with the Dragon Tattoo' AND film_year=2009)
		, (SELECT genre_id FROM [a02].[filmGenres] WHERE genre_name='Drama'))
	, ((SELECT film_id FROM [a02].[films] WHERE film_name='The Girl with the Dragon Tattoo' AND film_year=2009)
		, (SELECT genre_id FROM [a02].[filmGenres] WHERE genre_name='Mystery'))
	, ((SELECT film_id FROM [a02].[films] WHERE film_name='The Girl with the Dragon Tattoo' AND film_year=2011)
		, (SELECT genre_id FROM [a02].[filmGenres] WHERE genre_name='Crime'))
	, ((SELECT film_id FROM [a02].[films] WHERE film_name='The Girl with the Dragon Tattoo' AND film_year=2011)
		, (SELECT genre_id FROM [a02].[filmGenres] WHERE genre_name='Drama'))
	, ((SELECT film_id FROM [a02].[films] WHERE film_name='The Girl with the Dragon Tattoo' AND film_year=2011)
		, (SELECT genre_id FROM [a02].[filmGenres] WHERE genre_name='Mystery'));
GO

INSERT INTO [a02].[filmRoles] (role_name)
VALUES ('Actor'), ('Director');

INSERT INTO [a02].[countries] (country_name)
VALUES ('United States of America'), ('Denmark'), ('Sweden'), ('England');


INSERT INTO [a02].[profiles] (first_name, last_name, date_of_birth, gender, country_id)
VALUES ('Daniel', 'Craig', '19680302', 'M'
			, (SELECT country_id FROM [a02].[countries] WHERE country_name='England'))
		, ('Rooney', 'Mara', '19850417', 'F'
			, (SELECT country_id FROM [a02].[countries] WHERE country_name='United States of America'))
		, ('David', 'Fincher', '19620828', 'M'
			, (SELECT country_id FROM [a02].[countries] WHERE country_name='United States of America'))
		, ('Steven', 'Berkoff', '19370803', 'M'
			, (SELECT country_id FROM [a02].[countries] WHERE country_name='England'))
		, ('Niels Arden', 'Oplev', '19610326', 'M'
			, (SELECT country_id FROM [a02].[countries] WHERE country_name='Denmark'))
		, ('Noomi', 'Rapace', '19791228', 'F'
			, (SELECT country_id FROM [a02].[countries] WHERE country_name='Sweden'))
		, ('Bradley', 'Cooper', '19750105', 'M'
			, (SELECT country_id FROM [a02].[countries] WHERE country_name='United States of America'))
		, ('Zach', 'Galifianakis', '19691001','M'
			, (SELECT country_id FROM [a02].[countries] WHERE country_name='United States of America'));
GO

INSERT INTO [a02].[filmRoleAllocations] 
	(profile_id, film_id, role_id, is_main_director, is_main_cast, cast_name)
VALUES ((SELECT profile_id FROM [a02].[profiles] WHERE first_name='Daniel')
		, (SELECT film_id FROM [a02].[films] 
			WHERE film_name='The Girl with the Dragon Tattoo' AND film_year=2011)
		, (SELECT role_id FROM [a02].[filmRoles] WHERE role_name='Actor'),0,1,'Mikael Blomvist'),
		((SELECT profile_id FROM [a02].[profiles] WHERE first_name='Rooney')
		, (SELECT film_id FROM [a02].[films] 
			WHERE film_name='The Girl with the Dragon Tattoo' AND film_year=2011)
		, (SELECT role_id FROM [a02].[filmRoles] WHERE role_name='Actor'),0,1,'Lisbeth Salander'),
		((SELECT profile_id FROM [a02].[profiles] WHERE first_name='David')
		, (SELECT film_id FROM [a02].[films] 
			WHERE film_name='The Girl with the Dragon Tattoo' AND film_year=2011)
		, (SELECT role_id FROM [a02].[filmRoles] WHERE role_name='Director'),1,0,NULL),
		((SELECT profile_id FROM [a02].[profiles] WHERE first_name='Steven')
		, (SELECT film_id FROM [a02].[films] 
			WHERE film_name='The Girl with the Dragon Tattoo' AND film_year=2011)
		, (SELECT role_id FROM [a02].[filmRoles] WHERE role_name='Actor'),0,0,'Frode'),
		((SELECT profile_id FROM [a02].[profiles] WHERE first_name='Niels Arden')
		, (SELECT film_id FROM [a02].[films] 
			WHERE film_name='The Girl with the Dragon Tattoo' AND film_year=2009)
		, (SELECT role_id FROM [a02].[filmRoles] WHERE role_name='Director'),1,0,NULL),
		((SELECT profile_id FROM [a02].[profiles] WHERE first_name='Noomi')
		, (SELECT film_id FROM [a02].[films] 
			WHERE film_name='The Girl with the Dragon Tattoo' AND film_year=2009)
		, (SELECT role_id FROM [a02].[filmRoles] WHERE role_name='Actor'),0,1,'Lisbeth Salander'),
		((SELECT profile_id FROM [a02].[profiles] WHERE first_name='Bradley')
		, (SELECT film_id FROM [a02].[films] 
			WHERE film_name='The Hangover' AND film_year=2009)
		, (SELECT role_id FROM [a02].[filmRoles] WHERE role_name='Actor'),0,1,'Phil'),
		((SELECT profile_id FROM [a02].[profiles] WHERE first_name='Zach')
		, (SELECT film_id FROM [a02].[films] 
			WHERE film_name='The Hangover' AND film_year=2009)
		, (SELECT role_id FROM [a02].[filmRoles] WHERE role_name='Actor'),0,1,'Alan'),
		((SELECT profile_id FROM [a02].[profiles] WHERE first_name='Bradley')
		, (SELECT film_id FROM [a02].[films] 
			WHERE film_name='The Hangover Part II' AND film_year=2011)
		, (SELECT role_id FROM [a02].[filmRoles] WHERE role_name='Actor'),0,1,'Phil'),
		((SELECT profile_id FROM [a02].[profiles] WHERE first_name='Zach')
		, (SELECT film_id FROM [a02].[films] 
			WHERE film_name='The Hangover Part II' AND film_year=2011)
		, (SELECT role_id FROM [a02].[filmRoles] WHERE role_name='Actor'),0,1,'Alan'),
		((SELECT profile_id FROM [a02].[profiles] WHERE first_name='Bradley')
		, (SELECT film_id FROM [a02].[films] 
			WHERE film_name='The Hangover Part III' AND film_year=2013)
		, (SELECT role_id FROM [a02].[filmRoles] WHERE role_name='Actor'),0,1,'Phil'),
		((SELECT profile_id FROM [a02].[profiles] WHERE first_name='Zach')
		, (SELECT film_id FROM [a02].[films] 
			WHERE film_name='The Hangover Part III' AND film_year=2013)
		, (SELECT role_id FROM [a02].[filmRoles] WHERE role_name='Actor'),0,1,'Alan');
GO

INSERT INTO [a02].[userProfiles] (first_name, last_name, display_name)
VALUES ('Joe', 'Borg', 'joeb') , ('Lisa', 'Galea', 'lisag'), ('Tony', 'Borg', 'tonyb');

INSERT INTO [a02].[userAccounts] (account_email, profile_id)
VALUES ('joeborg@gmail.com', (SELECT profile_id FROM [a02].[userProfiles] WHERE display_name='joeb')),
		('lisag@gmail.com', (SELECT profile_id FROM [a02].[userProfiles] WHERE display_name='lisag')),
		('tonyborg@gmail.com', (SELECT profile_id FROM [a02].[userProfiles] WHERE display_name='tonyb'));
GO

INSERT INTO [a02].[filmReviews] (review_content, rate_value, film_id, profile_id)
VALUES ('Could not stop laughing', 5,
			(SELECT film_id FROM [a02].[films] 
			WHERE film_name='The Hangover' AND film_year=2009),
			(SELECT profile_id FROM [a02].[userProfiles]
			WHERE display_name='lisag')),
		('Epic', 5,
			(SELECT film_id FROM [a02].[films] 
			WHERE film_name='The Hangover' AND film_year=2009),
			(SELECT profile_id FROM [a02].[userProfiles]
			WHERE display_name='joeb')),
		('Really enjoyed it', 5,
			(SELECT film_id FROM [a02].[films] 
			WHERE film_name='The Girl with the Dragon Tattoo' AND film_year=2009),
			(SELECT profile_id FROM [a02].[userProfiles]
			WHERE display_name='joeb')),
		('Better than the first movie', 4,
			(SELECT film_id FROM [a02].[films] 
			WHERE film_name='The Girl with the Dragon Tattoo' AND film_year=2011),
			(SELECT profile_id FROM [a02].[userProfiles]
			WHERE display_name='tonyb')),
		('Looking forward for the sequel', 5,
			(SELECT film_id FROM [a02].[films] 
			WHERE film_name='The Girl with the Dragon Tattoo' AND film_year=2011),
			(SELECT profile_id FROM [a02].[userProfiles]
			WHERE display_name='lisag'));
GO

INSERT INTO [a02].[comments] (comment_content, review_id, profile_id, parent_comment_id)
VALUES ('Me too', 
		(SELECT review_id FROM [a02].[filmReviews] WHERE review_content='Could not stop laughing'),
		(SELECT profile_id FROM [a02].[userProfiles] WHERE display_name='joeb'), NULL),
		('I disagree', 
		(SELECT review_id FROM [a02].[filmReviews] WHERE review_content='Better than the first movie'),
		(SELECT profile_id FROM [a02].[userProfiles] WHERE display_name='joeb'), NULL);

INSERT INTO [a02].[comments] (comment_content, review_id, profile_id, parent_comment_id)
VALUES	('Even credits were funny', 
		(SELECT review_id FROM [a02].[filmReviews] WHERE review_content='Could not stop laughing'),
		(SELECT profile_id FROM [a02].[userProfiles] WHERE display_name='tonyb'), 
			(SELECT comment_id FROM [a02].[comments] WHERE comment_content='Me too'));
GO

INSERT INTO [a02].[filmReviews] (review_content, rate_value, film_id, profile_id)
VALUES ('Great movie', 4,
			(SELECT film_id FROM [a02].[films] 
			WHERE film_name='The Girl with the Dragon Tattoo' AND film_year=2009),
			(SELECT profile_id FROM [a02].[userProfiles]
			WHERE display_name='lisag')),
		('Faithful to books', 5,
			(SELECT film_id FROM [a02].[films] 
			WHERE film_name='The Girl with the Dragon Tattoo' AND film_year=2009),
			(SELECT profile_id FROM [a02].[userProfiles]
			WHERE display_name='joeb')),
		('Very funny', 4,
			(SELECT film_id FROM [a02].[films] 
			WHERE film_name='The Hangover Part II' AND film_year=2011),
			(SELECT profile_id FROM [a02].[userProfiles]
			WHERE display_name='lisag'));