﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff
{
    class Publication
    {
        protected string title;
        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        double price;
        public double Price
        {
                get { return price; }
                set { price = value; }
        }

        int copies;
        public int Copies
        {
            get { return copies; }
            set { copies = value; }
        }

        public Publication(string title, double price)
        {
            // TODO: Complete member initialization
            this.title = title;
            this.price = price;
        }

        public void SellCopies()
        {
            if (Copies <=0 )
            {
                Console.WriteLine("This publication is out of stock");
            }
        }

    }
}
