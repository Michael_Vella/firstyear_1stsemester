﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff
{
    class Magaizine : Publication
    {
        int orderQty;

        public int OrderQty
        {
            get { return orderQty; }
            set { orderQty = value; }
        }

        int currIssue;

        public int CurrIssue
        {
            get { return currIssue; }
            set { currIssue = value; }
        }

        public Magaizine(string title, double price)
            : base(title, price)
        { 
            
        }

        public void AdjQty(int newQty)
        {
            orderQty = newQty;
        }

        public void RecNewIssue()
        {
            currIssue++;
        }
    }
}
