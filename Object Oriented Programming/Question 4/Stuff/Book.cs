﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff
{
    class Book : Publication
    {
        string author;
        public string Author
        {
            get { return author; }
            set { author = value; }
        }

        public Book(string title, double price, string author): base(title,price)
        {
            this.author = author;
        }
        public void OrderCopies(int amountOfCopies)
        {
            Copies += amountOfCopies;
        }
    }
}
