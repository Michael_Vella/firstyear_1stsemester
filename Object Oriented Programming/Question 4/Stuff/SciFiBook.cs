﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff
{
    class SciFiBook : Book
    {
        public SciFiBook(string title, double price, string author)
            : base(title, price, author)
        {
            this.title = "";
        }
    }
}
