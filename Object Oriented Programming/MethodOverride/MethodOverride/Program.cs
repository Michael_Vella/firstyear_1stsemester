﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MethodOverride
{
    class Program
    {
        static void Main(string[] args)
        {
            test t = new test();
            test2 t2 = new test2();
            test2 t3 = new test2();
            
            Console.WriteLine("Base Class Method \n");
            t.GetStuff(2);

            Console.WriteLine("");
        }
    }
}
