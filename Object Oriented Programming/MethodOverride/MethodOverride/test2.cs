﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MethodOverride
{
    class test2 : test
    {
        string[] names = new string[5];

        public override void GetStuff(int id)
        {
           // base.GetStuff(id);

            if (id < 5)
            {
                Console.WriteLine("The item's name is " + names[id]);
            }
            else
            {
                Console.WriteLine("Invalid ID");
            }
        }
    }
}
