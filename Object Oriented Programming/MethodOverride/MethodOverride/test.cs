﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MethodOverride
{
    class test
    {
        public virtual void GetStuff(int id)
        {
            Console.WriteLine("The item ID: " + id + "has been retrieved.");
        }
    }
}
