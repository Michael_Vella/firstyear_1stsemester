﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Worksheet_4;

namespace Question2
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal animal1 = new Animal();
            Dog animal2 = new Dog();
            Cat animal3 = new Cat();
            Duck animal4 = new Duck();

            Animal[] animals = {animal1, animal2, animal3, animal4 };

            foreach (Animal animal in animals)
            {
                animal.Speak();
            }
            Console.ReadKey();
        }
    }
}
