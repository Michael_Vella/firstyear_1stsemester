﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Worksheet_4
{
    class Animal
    {
        public virtual void Speak() 
        {
            Console.WriteLine("I'm a generic animal");
        }

        public virtual string Eat()
        { 
             
        }
    }
}
