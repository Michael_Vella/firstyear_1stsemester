﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Worksheet_4
{
    class Cat : Animal
    {
        public override void Speak()
        {
            Console.WriteLine("I am a cat: meow");
        }
    }
}
