﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Question_2
{
    class Employee
    {
        string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        double basePay;
        public double BasePay 
        {
            get { return basePay; }
            set { basePay = value; }
        }

        public Employee(string name, double basePay)
        {
            this.name = name;
            this.basePay = BasePay;
        }

        public virtual double TotalPay()
        {
            return basePay;
        }
        
    }
}
