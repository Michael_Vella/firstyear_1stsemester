﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Question_2
{
    class SelfEmployee : Employee
    {
        double salesBonus;

        public double SalesBonus
        {
            get { return salesBonus; }
            set { salesBonus = value; }
        }

        public SelfEmployee(string name, double basePay, double saleBonus)
            : base(name, basePay)
        {
            this.salesBonus = salesBonus;
        }

        public override int TotalPay () 
        {
            return base.TotalPay()+salesBonus);
        }
    }
}
