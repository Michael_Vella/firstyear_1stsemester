﻿namespace Worksheet_2_Application
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbCar = new System.Windows.Forms.TabControl();
            this.drivePage = new System.Windows.Forms.TabPage();
            this.btnDrive = new System.Windows.Forms.Button();
            this.lblDestination = new System.Windows.Forms.Label();
            this.txtbxDestination = new System.Windows.Forms.TextBox();
            this.tbCarDetails = new System.Windows.Forms.TabPage();
            this.tbRefuel = new System.Windows.Forms.TabPage();
            this.rbHonda = new System.Windows.Forms.RadioButton();
            this.rbPunto = new System.Windows.Forms.RadioButton();
            this.lblCurrentCar = new System.Windows.Forms.Label();
            this.lblCarChoice = new System.Windows.Forms.Label();
            this.tbCar.SuspendLayout();
            this.drivePage.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbCar
            // 
            this.tbCar.Controls.Add(this.drivePage);
            this.tbCar.Controls.Add(this.tbCarDetails);
            this.tbCar.Controls.Add(this.tbRefuel);
            this.tbCar.Location = new System.Drawing.Point(13, 156);
            this.tbCar.Name = "tbCar";
            this.tbCar.SelectedIndex = 0;
            this.tbCar.Size = new System.Drawing.Size(488, 228);
            this.tbCar.TabIndex = 0;
            // 
            // drivePage
            // 
            this.drivePage.Controls.Add(this.btnDrive);
            this.drivePage.Controls.Add(this.lblDestination);
            this.drivePage.Controls.Add(this.txtbxDestination);
            this.drivePage.Location = new System.Drawing.Point(4, 22);
            this.drivePage.Name = "drivePage";
            this.drivePage.Padding = new System.Windows.Forms.Padding(3);
            this.drivePage.Size = new System.Drawing.Size(480, 202);
            this.drivePage.TabIndex = 0;
            this.drivePage.Text = "Drive";
            this.drivePage.UseVisualStyleBackColor = true;
            // 
            // btnDrive
            // 
            this.btnDrive.Location = new System.Drawing.Point(182, 113);
            this.btnDrive.Name = "btnDrive";
            this.btnDrive.Size = new System.Drawing.Size(90, 43);
            this.btnDrive.TabIndex = 2;
            this.btnDrive.Text = "Drive";
            this.btnDrive.UseVisualStyleBackColor = true;
            this.btnDrive.Click += new System.EventHandler(this.btnDrive_Click);
            // 
            // lblDestination
            // 
            this.lblDestination.AutoSize = true;
            this.lblDestination.Location = new System.Drawing.Point(112, 56);
            this.lblDestination.Name = "lblDestination";
            this.lblDestination.Size = new System.Drawing.Size(69, 13);
            this.lblDestination.TabIndex = 1;
            this.lblDestination.Text = "Destintation: ";
            // 
            // txtbxDestination
            // 
            this.txtbxDestination.Location = new System.Drawing.Point(212, 56);
            this.txtbxDestination.Name = "txtbxDestination";
            this.txtbxDestination.Size = new System.Drawing.Size(100, 20);
            this.txtbxDestination.TabIndex = 0;
            // 
            // tbCarDetails
            // 
            this.tbCarDetails.Location = new System.Drawing.Point(4, 22);
            this.tbCarDetails.Name = "tbCarDetails";
            this.tbCarDetails.Padding = new System.Windows.Forms.Padding(3);
            this.tbCarDetails.Size = new System.Drawing.Size(480, 202);
            this.tbCarDetails.TabIndex = 1;
            this.tbCarDetails.Text = "Car Details";
            this.tbCarDetails.UseVisualStyleBackColor = true;
            // 
            // tbRefuel
            // 
            this.tbRefuel.Location = new System.Drawing.Point(4, 22);
            this.tbRefuel.Name = "tbRefuel";
            this.tbRefuel.Padding = new System.Windows.Forms.Padding(3);
            this.tbRefuel.Size = new System.Drawing.Size(480, 202);
            this.tbRefuel.TabIndex = 2;
            this.tbRefuel.Text = "Refuel";
            this.tbRefuel.UseVisualStyleBackColor = true;
            // 
            // rbHonda
            // 
            this.rbHonda.AutoSize = true;
            this.rbHonda.Location = new System.Drawing.Point(12, 62);
            this.rbHonda.Name = "rbHonda";
            this.rbHonda.Size = new System.Drawing.Size(83, 17);
            this.rbHonda.TabIndex = 1;
            this.rbHonda.TabStop = true;
            this.rbHonda.Text = "Honda Civic";
            this.rbHonda.UseVisualStyleBackColor = true;
            this.rbHonda.CheckedChanged += new System.EventHandler(this.rbHonda_CheckedChanged);
            // 
            // rbPunto
            // 
            this.rbPunto.AutoSize = true;
            this.rbPunto.Location = new System.Drawing.Point(12, 99);
            this.rbPunto.Name = "rbPunto";
            this.rbPunto.Size = new System.Drawing.Size(73, 17);
            this.rbPunto.TabIndex = 2;
            this.rbPunto.TabStop = true;
            this.rbPunto.Text = "Fiat Punto";
            this.rbPunto.UseVisualStyleBackColor = true;
            this.rbPunto.CheckedChanged += new System.EventHandler(this.rbPunto_CheckedChanged);
            // 
            // lblCurrentCar
            // 
            this.lblCurrentCar.AutoSize = true;
            this.lblCurrentCar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentCar.Location = new System.Drawing.Point(245, 77);
            this.lblCurrentCar.Name = "lblCurrentCar";
            this.lblCurrentCar.Size = new System.Drawing.Size(140, 20);
            this.lblCurrentCar.TabIndex = 3;
            this.lblCurrentCar.Text = "Current Car: NULL";
            // 
            // lblCarChoice
            // 
            this.lblCarChoice.AutoSize = true;
            this.lblCarChoice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCarChoice.Location = new System.Drawing.Point(12, 9);
            this.lblCarChoice.Name = "lblCarChoice";
            this.lblCarChoice.Size = new System.Drawing.Size(90, 13);
            this.lblCarChoice.TabIndex = 4;
            this.lblCarChoice.Text = "Choose your car: ";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(513, 396);
            this.Controls.Add(this.lblCarChoice);
            this.Controls.Add(this.lblCurrentCar);
            this.Controls.Add(this.rbPunto);
            this.Controls.Add(this.rbHonda);
            this.Controls.Add(this.tbCar);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Worksheet 2";
            this.tbCar.ResumeLayout(false);
            this.drivePage.ResumeLayout(false);
            this.drivePage.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tbCar;
        private System.Windows.Forms.TabPage drivePage;
        private System.Windows.Forms.Button btnDrive;
        private System.Windows.Forms.Label lblDestination;
        private System.Windows.Forms.TextBox txtbxDestination;
        private System.Windows.Forms.TabPage tbCarDetails;
        private System.Windows.Forms.TabPage tbRefuel;
        private System.Windows.Forms.RadioButton rbHonda;
        private System.Windows.Forms.RadioButton rbPunto;
        private System.Windows.Forms.Label lblCurrentCar;
        private System.Windows.Forms.Label lblCarChoice;
    }
}

