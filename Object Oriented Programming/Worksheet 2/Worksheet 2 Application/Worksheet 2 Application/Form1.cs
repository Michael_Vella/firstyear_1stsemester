﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Worksheet_2_Application
{
    public partial class Form1 : Form
    {

        Car car1 = new Car();
        Car car2 = new Car();
        Car[] vroomer = new Car[2];// Have to find out how to make it into array with details 
        int index;

        public Form1()
        {
            index = 0;
            InitializeComponent();
            vroomer[0] = car1;
            vroomer[1] = car2;
        }

        

        private void btnDrive_Click(object sender, EventArgs e)
        {
            vroomer[index].drive(txtbxDestination.Text);
            txtbxDestination.Clear();
        }

        private void rbHonda_CheckedChanged(object sender, EventArgs e)
        {
            index = 0;
            lblCurrentCar.Text = "Current Car: Honda Civic";
        }

        private void rbPunto_CheckedChanged(object sender, EventArgs e)
        {
            index = 1;
            lblCurrentCar.Text = "Current Car: Fiat Punto";
        }

        
    }
}
