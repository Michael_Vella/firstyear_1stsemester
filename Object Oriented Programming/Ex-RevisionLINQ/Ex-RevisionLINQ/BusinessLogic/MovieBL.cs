﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ex_RevisionLINQ.DataAccess;

namespace Ex_RevisionLINQ.BusinessLogic
{
    class MovieBL
    {

        public List<Movie> GetMovies()
        {
            RentalDBEntities context = new RentalDBEntities();
            return context.Movies.ToList(); // SELECT * FROM MOVIES
        }


        public List<Movie> GetMoviesByDirector(int directorId)
        {
            RentalDBEntities context = new RentalDBEntities();
            return context.Movies.Where(m => m.DirectorID == directorId).ToList(); // SELECT * FROM MOVIES BY Director
        }

        public List<Movie> GetMoviesByDirectorOrderedByName(int directorId)
        {
            RentalDBEntities context = new RentalDBEntities();
            return context.Movies.Where(m => m.DirectorID == directorId).OrderBy(m => m.MovieName).ThenBy(m=> m.ReleaseDate).ToList(); // SELECT * FROM MOVIES
            //We are returning an ordered list of movies by director and sorted by name and then by release date
        }


        public int AddMovie(string name, int directorId, DateTime releaseDate)
        {
            RentalDBEntities context = new RentalDBEntities();
            Movie movieExists = context.Movies.Where(x => x.MovieName == name).FirstOrDefault();
            if (movieExists != null)
            {
                return -1; //Movie exists
            }
            else
            {
                context.Movies.Add(new Movie{ MovieName = name, DirectorID = directorId , ReleaseDate = releaseDate});
                return context.SaveChanges();
            }
        }

        public int EditMovie(int id, string name, int directorId, DateTime releaseDate)
        {
            RentalDBEntities context = new RentalDBEntities();
            //Check if director exists already with the submitted name and surname 
            Movie movieExists = context.Movies.Where(x => x.MovieName == name).FirstOrDefault();
            if (movieExists != null)
            {
                return -1; //Movie exists
            }
            else
            {
                Movie movieToEdit = context.Movies.Where(m => m.Id == id).FirstOrDefault();
                movieToEdit.MovieName = name;
                movieToEdit.ReleaseDate = releaseDate;
                movieToEdit.DirectorID = directorId;
                return context.SaveChanges(); //Saves to database!
            }
        }


        public int DeleteMovie(int id)
        {
            RentalDBEntities context = new RentalDBEntities();
            //Check if director exists already with the submitted name and surname 
            Movie movieExists = context.Movies.Where(x => x.Id == id).FirstOrDefault();
            if (movieExists == null)
            {
                return -1; //Movie not found
            }
            else
            {
                context.Movies.Remove(movieExists);
                return context.SaveChanges(); //Saves to database!
            }
        }
    }
}
