﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ex_RevisionLINQ.DataAccess; //To access the DAL

namespace Ex_RevisionLINQ.BusinessLogic
{
    public class DirectorBL
    {

        public List<Director>GetDirectors()
        {
            RentalDBEntities context = new RentalDBEntities();
            return context.Directors.ToList(); // SELECT * FROM DIRECTORS
        }

        public int AddDirector(string name, string surname)
        {
            RentalDBEntities context = new RentalDBEntities();
            Director directorExists = context.Directors.Where(x => x.DirectorName == name && x.DirectorSurname == surname).FirstOrDefault();
            if(directorExists!=null)
            {
                return -1; //Director exists
            }  else
            {
                context.Directors.Add(new Director { DirectorName = name, DirectorSurname = surname });
                return context.SaveChanges();
            }
        }

        public int EditDirector(int id, string name, string surname)
        {
            RentalDBEntities context = new RentalDBEntities();
            //Check if director exists already with the submitted name and surname 
            Director directorExists = context.Directors.Where(x => x.DirectorName == name && x.DirectorSurname == surname).FirstOrDefault();
            if (directorExists != null)
            {
                return -1; //Director exists
            }
            else
            {
                Director directorToEdit = context.Directors.Where(d => d.Id == id).FirstOrDefault();
                directorToEdit.DirectorName = name;
                directorToEdit.DirectorSurname = surname;
                return context.SaveChanges();
            }
        }


        public int DeleteDirector(int id)
        {
            RentalDBEntities context = new RentalDBEntities();
            //Check if director exists already with the submitted name and surname 
            Director directorExists = context.Directors.Where(x => x.Id  == id).FirstOrDefault();
            if (directorExists == null)
            {
                return -1; //Director does not exist
            }
            else
            {
                context.Directors.Remove(directorExists);
                return context.SaveChanges();
            }
        }

    }
}
