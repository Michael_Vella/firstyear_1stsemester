﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ex_RevisionLINQ.DataAccess;

namespace Ex_RevisionLINQ.BusinessLogic
{
    public class UserBL
    {

        public List<User> GetUsers()
        {
            RentalDBEntities context = new RentalDBEntities();
            return context.Users.ToList(); // SELECT * FROM DIRECTORS
        }

        public int AddUser(string username, string password, string town)
        {
            RentalDBEntities context = new RentalDBEntities();
            User userExists = context.Users.Where(x => x.Username == username).FirstOrDefault();
            if (userExists != null)
            {
                return -1; //User exists
            }
            else
            {
                context.Users.Add(new User{  Username = username, Password = password , Town = town });
                return context.SaveChanges();
            }
        }

        public int EditUser(int id, string username, string password, string town)
        {
            RentalDBEntities context = new RentalDBEntities();
            //Check if director exists already with the submitted name and surname 
            User userExists = context.Users.Where(x => x.Username == username).FirstOrDefault();
            if (userExists != null)
            {
                return -1; //User exists
            }
            else
            {
                User userToEdit = context.Users.Where(u => u.Id == id).FirstOrDefault();
                userToEdit.Username = username;
                userToEdit.Password = password;
                userToEdit.Town = town;
                return context.SaveChanges();
            }
        }


        public int DeleteUser(int id)
        {
            RentalDBEntities context = new RentalDBEntities();
            //Check if director exists already with the submitted name and surname 
            User userExists = context.Users.Where(x => x.Id == id).FirstOrDefault();
            if (userExists == null)
            {
                return -1; //User does not exist 
            }
            else
            {
                context.Users.Remove(userExists);
                return context.SaveChanges();
            }
        }

    }
}
