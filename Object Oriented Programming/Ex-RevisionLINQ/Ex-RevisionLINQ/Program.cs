﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ex_RevisionLINQ.BusinessLogic;


namespace Ex_RevisionLINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            //Creating new user using enum 
            UserBL ubl = new UserBL();

            ubl.AddUser("tony", "123", Town.Luqa.ToString());
            //Please remember that to add a town from combobox 
            //You need to populate combobox from Enum 
            //You need to get the selectedItem.ToString(); instead of the 3rd parameter up here...
            if(ubl.AddUser("tony", "123", Town.Luqa.ToString())==-1)
            {
                Console.WriteLine("User already exists!");
            } else 
            {
                Console.WriteLine("Successfully Added!");
            }
            Console.ReadKey();
        }
    }
}
