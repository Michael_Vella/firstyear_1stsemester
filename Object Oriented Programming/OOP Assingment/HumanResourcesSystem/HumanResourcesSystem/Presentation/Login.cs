﻿using HumanResourcesSystem.Buisness;
using HumanResourcesSystem.Phyiscal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HumanResourcesSystem.Presentation
{
    class Login
    {
        public static SystemDbContext db = new SystemDbContext();
        public static void LogIn()
        {

            bool isNotLogged = true;

            while (isNotLogged == true)
            {
                Console.Clear();
                string username;
                string password;

                Console.Write("Please Enter username:");
                username = Console.ReadLine();
                Console.WriteLine();
                Console.Write("Please Enter password:");
                password = Console.ReadLine();

                //searches if the employee from the list of employees exists by checking the username and password
                EmployeeBl emp = new EmployeeBl();
                Employee employee = new Employee();
                employee = emp.CheckIfEmpExists(username, password);
                if (employee != null && employee is HumanResourceEmployee) //if it exists ie not null and then if it is an HR Employee
                {
                    isNotLogged = false;
                    MainMenu.Menu();
                    break;
                }
                else if (employee != null && !(employee is HumanResourceEmployee)) // check if the employee exists and  if it isnt a Human Resources Employee
                {
                    Console.Clear();
                    EmployeeBl empBl = new EmployeeBl();
                    empBl.SearchEmployee(username,password);
                    Logout();
                    break;
                }
                else //if they entered incorrect details. This will cause a login method to repeat itself
                {
                    isNotLogged = true;
                    Console.Clear();
                    Console.WriteLine("Entered Username or Password is incorrect.\nPress any key to retry !!");
                    Console.ReadKey();
                }
            }
        }
        public static void Logout()
        {
            Console.WriteLine("Press enter to log out........");
            Console.ReadLine();
            Login.LogIn();
        }
    }
}
