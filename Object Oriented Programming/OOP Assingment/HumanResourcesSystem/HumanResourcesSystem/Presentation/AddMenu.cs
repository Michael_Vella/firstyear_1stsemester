﻿using HumanResourcesSystem.Buisness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HumanResourcesSystem.Presentation
{
    class AddMenu
    {
        /// <summary>
        /// This methods handles the add menu portion of the system
        /// </summary>
        public static void InsertMenu()
        {
            int usrSelection = 0;
            while (usrSelection > 6 || usrSelection < 1)
            {
                Console.Clear();
                Console.WriteLine("==============================================================================");
                Console.WriteLine("Press 1 to add a normal employee.");
                Console.WriteLine("Press 2 to add an academic employee.");
                Console.WriteLine("Press 3 to add an administrative employee.");
                Console.WriteLine("Press 4 to add a secretarial employee.");
                Console.WriteLine("Press 5 to add a human resources employee.");
                Console.WriteLine("Press 6 to add to the main menu");
                Console.WriteLine("===============================================================================");

                Console.Write("\nYour Selection: ");
                usrSelection = Convert.ToInt32(Console.ReadLine().ToString());

                EmployeeBL empBl = new EmployeeBL();
                switch (usrSelection)
                {
                    //Add a generic employee
                    case 1:
                        empBl.AddEmployee();
                        InsertMenu();
                        break;
                    //Add a lecturer
                    case 2:
                    //  AddAcademicEmployee(); break;
                    //Add an Admin
                    case 3:
                    //  AddAdminEmployee(); break;
                    //Add a Secretary
                    case 4:
                    //  AddSecretarialEmployee(); break;
                    //Add an HR Employee
                    case 5:
                    // AddHumanResourcesEmployee(); break;
                    //Takes User to the main menu
                    case 6:
                        MainMenu.CentralMenu(); break;
                    default:

                        usrSelection = 0;
                        Console.WriteLine("Input is not between 1 or 6. \n\nResetting. \nPress any key to continue");
                        Console.ReadKey();

                        break;
                }
            }
        }
    }
}
