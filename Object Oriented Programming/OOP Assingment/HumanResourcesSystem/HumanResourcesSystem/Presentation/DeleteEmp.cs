﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HumanResourcesSystem.Buisness;

namespace HumanResourcesSystem.Presentation
{
    public class DeleteEmp
    {
        static EmployeeBl emp = new EmployeeBl();

        public static void Delete()
        {
            Console.Clear();

            Console.Write("Please enter the username: ");
            string username = Console.ReadLine();
            emp.DeleteEmployee(username);
            Console.WriteLine("Press enter to go back to main menu.");
            Console.ReadLine();
            MainMenu.Menu();
        }
    }
}