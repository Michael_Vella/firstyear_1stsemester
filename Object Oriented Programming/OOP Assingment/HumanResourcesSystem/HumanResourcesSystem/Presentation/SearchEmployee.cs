﻿using HumanResourcesSystem.Buisness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HumanResourcesSystem.Presentation
{
    /// <summary>
    /// This class will handle all the searching of the employees 
    /// </summary>
    public class SearchEmployee
    {
        public static void Menu()
        {
            bool isSelected = false;
            int usrSelection = 0;
            while (isSelected == false)
            {
                Console.Clear();
                Console.WriteLine("====================================================================================================================");
                Console.WriteLine("Press 1 to display all employees.");
                Console.WriteLine("Press 2 to search for a sepcific employes.");
                Console.WriteLine("Press 3 to search for salary averages.");
                Console.WriteLine("Press 4 to search for total employees per department.");
                Console.WriteLine("Press 5 to display all employees by type (such as hr or academic).");
                Console.WriteLine("Press 6 to return to the main menu");
                Console.WriteLine("====================================================================================================================");

                Console.Write("\nYour Selection: ");
                usrSelection = Convert.ToInt32(Console.ReadLine().ToString());

                EmployeeBl empBl = new EmployeeBl();

                switch (usrSelection)
                {
                    //Display all Employees
                    case 1:
                        isSelected = true;
                        empBl.ListAllEmployees();
                        GoToSearch();
                        break;
                    //Search for a employee
                    case 2:
                        isSelected = true;
                        empBl.SearchEmployee();
                        GoToSearch();
                        break;
                    case 3:
                        DepartmentAverages.DepartmentAverage();
                        GoToSearch();
                        break;
                    case 4 :
                        empBl.EmployeeAmountPerDepart();
                        GoToSearch();
                        break;
                    case 5:
                        empBl.GetEmpByType();
                        GoToSearch();
                        break;
                    //Takes User to the main menu
                    case 6:
                        isSelected = true;
                        MainMenu.Menu();
                        break;
                    //Case Default, Reset and tell reinput selected value
                    default:
                        isSelected = false;
                        usrSelection = 0;
                        Console.WriteLine("Input is not between 1 or 3. \n\nResetting. \nPress any key to continue");
                        Console.ReadKey();

                        break;
                }
            }
        }

        /// <summary>
        /// Returns user to search menu
        /// </summary>
        public static void GoToSearch()
        {
            Console.WriteLine("\n\nPress the ENTER key to return to search menu....");
            Console.ReadLine();
            SearchEmployee.Menu();
        }
    }
}
