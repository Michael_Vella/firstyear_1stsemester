﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HumanResourcesSystem.Buisness;
namespace HumanResourcesSystem.Presentation
{
    public class UpdateEmp
    {
        static EmployeeBl empBl = new EmployeeBl();

        public static void UpdateMenu()
        {
            int usrSelection = 0;
            bool isSelected = false;
            while (isSelected == false)
            {
                Console.Clear();
                Console.WriteLine("====================================================================================================================");
                Console.WriteLine("Press 1 to update employee.");
                Console.WriteLine("Press 2 to update employee salaryies.");
                Console.WriteLine("Press 3 to Return to main menu.");
                Console.WriteLine("====================================================================================================================");

                Console.Write("\nYour Selection: ");
                usrSelection = Convert.ToInt32(Console.ReadLine().ToString());

                switch (usrSelection)
                {
                    //Begin update 
                    case 1:
                        empBl.UpdateEmployee();
                        isSelected = true;
                        GoToUpdateMenu();
                        break;
                    case 2:
                        UpdateSalary.SalaryUpdateDecider();
                        isSelected = true;
                        GoToUpdateMenu();
                        break;
                    case 3:
                        isSelected = true;
                        MainMenu.Menu();
                        break;
                    default:
                        isSelected = false;
                        usrSelection = 0;
                        Console.WriteLine("Input is not between 1 or 6. \n\nResetting. \nPress any key to continue");
                        Console.ReadKey();

                        break;
                }
            }
        }
        public static void GoToUpdateMenu()
        {
            Console.WriteLine("\n\nPress ENTER to return to Update menu...");
            Console.ReadLine();
            UpdateMenu();
        }
    }
}
