﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HumanResourcesSystem.Buisness;

namespace HumanResourcesSystem.Presentation
{
    class MainMenu
    {
        EmployeeBl emp = new EmployeeBl();
        public static void Menu()
        {
            bool isSelected = false;
            int usrSelection = 0;
            while (isSelected == false)
            {
                Console.Clear();
                Console.WriteLine("====================================================================================================================");
                Console.WriteLine("Press 1 to Search for a specific employee.");
                Console.WriteLine("Press 2 to Add a new Employee.");
                Console.WriteLine("Press 3 to Update an exististing Employee.");
                Console.WriteLine("Press 4 to Delete a an exisiting Employee.");
                Console.WriteLine("Press 5 to log out of system.");
                Console.WriteLine("Press 6 to exit application");
                Console.WriteLine("====================================================================================================================");

                Console.Write("\nYour Selection: ");
                usrSelection = Convert.ToInt32(Console.ReadLine().ToString());

                switch (usrSelection)
                {
                    //Opens up the search menu
                    case 1:
                        isSelected = true;
                        SearchEmployee.Menu();
                        break;
                    //Opens up the add menu
                    case 2:
                        isSelected = true;
                        AddEmployee.AddMenu();
                        break;
                    //Opens up the delete menu
                    case 3:
                        isSelected = true;
                        UpdateEmp.UpdateMenu();
                        break;
                    //Opens up the delete menu
                    case 4:
                        isSelected = true;
                        DeleteEmp.Delete();
                        break;
                    //Logs out the main user
                    case 5:
                        isSelected = true;
                        Login.LogIn();
                        break;
                    //Takes User to the main menu
                    case 6:
                        isSelected = true;
                        Exit();
                        break;
                    default:
                        isSelected = false;
                        usrSelection = 0;
                        Console.WriteLine("Input is not between 1 or 6. \n\nResetting. \nPress any key to continue");
                        Console.ReadKey();
                        break;
                }
            }
        }

        /// <summary>
        /// Cleanly ends the program
        /// </summary>
        private static void Exit()
        {
           Environment.Exit(0);
        }
    }
}
