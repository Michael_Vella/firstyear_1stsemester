﻿using HumanResourcesSystem.Buisness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HumanResourcesSystem.Presentation
{
    /// <summary>
    /// This class will handle the inserting of all the employees 
    /// </summary>
    public class AddEmployee
    {
        /// <summary>
        /// This methods handles the add menu portion of the system
        /// </summary>
        public static void AddMenu()
        {
            bool isSelected = false;
            int usrSelection = 0;
            while (isSelected == false)
            {
                Console.Clear();
                Console.WriteLine("====================================================================================================================");
                Console.WriteLine("Press 1 to add a generic employee.");
                Console.WriteLine("Press 2 to add an academic employee.");
                Console.WriteLine("Press 3 to add an administrative employee.");
                Console.WriteLine("Press 4 to add a secretarial employee.");
                Console.WriteLine("Press 5 to add a human resources employee.");
                Console.WriteLine("Press 6 to return to the main menu");
                Console.WriteLine("====================================================================================================================");

                Console.Write("\nYour Selection: ");
                usrSelection = Convert.ToInt32(Console.ReadLine().ToString());

                EmployeeBl empBl = new EmployeeBl();
                AcademicEmployeeBl acdBl = new AcademicEmployeeBl();
                AdministrativeEmployeeBl adminBl = new AdministrativeEmployeeBl();
                SecretarialEmployeeBl secBl = new SecretarialEmployeeBl();
                HumanResoureEmployeeBl hrBl = new HumanResoureEmployeeBl();

                switch (usrSelection)
                {
                    //Add a generic employee
                    case 1:
                        isSelected = true;
                        empBl.AddEmployee();
                        GoToAddMenu();
                        break;
                    //Add a lecturer
                    case 2:
                        isSelected = true;
                        acdBl.AddEmployee();
                        GoToAddMenu();
                        break;
                    //Add an Admin
                    case 3:
                        isSelected = true;
                        adminBl.AddEmployee();
                        GoToAddMenu();
                        break;
                    //Add a Secretary
                    case 4:
                        isSelected = true;
                        secBl.AddEmployee();
                        GoToAddMenu();
                        break;
                    //Add an HR Employee
                    case 5:
                        isSelected = true;
                        hrBl.AddEmployee();
                        GoToAddMenu();
                        break;
                    //Takes User to the main menu
                    case 6:
                        isSelected = true;
                        MainMenu.Menu();
                        break;
                    //Case Default, Reset and tell reinput selected value
                    default:
                        isSelected = false;
                        usrSelection = 0;
                        Console.WriteLine("Input is not between 1 or 6. \n\nResetting. \nPress any key to continue");
                        Console.ReadKey();

                        break;
                }
            }
        }

        public static void GoToAddMenu()
        {
            Console.WriteLine("Press enter to return to add menu.....");
            Console.ReadLine();
            MainMenu.Menu();
        }
    }
}
