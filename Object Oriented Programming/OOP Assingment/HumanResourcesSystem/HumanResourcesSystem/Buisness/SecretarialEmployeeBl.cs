﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HumanResourcesSystem.Phyiscal;

namespace HumanResourcesSystem.Buisness
{
    class SecretarialEmployeeBl : EmployeeBl
    {
        /// <summary>
        /// Adds an Secretarial employee
        /// </summary>
        /// <returns></returns>
        public override int AddEmployee()
        {
            string name = "";
            string surname = "";
            string address = "";
            float salary = 0;
            DateTime dateOfEmployement = DateTime.Today;
            string username = "";
            string password = "";
            int deptId = 0;

            Console.Clear();
            Console.Write("Please enter the intened username: ");
            username = Console.ReadLine();
            // Check if employee exists
            if (CheckIfEmpExists(username) != null)
            {
                Console.WriteLine("Employee Exists...\nPress any key to return to Add Menu");
                Console.ReadKey();
                return -1;// Employee already exists
            }
            else
            {
                Department dept = new Department();

                Console.WriteLine("Employee does not exist. \nAdd employee details below.");
                Console.Write("Enter Name: ");
                name = Console.ReadLine();
                Console.Write("Enter Surname: ");
                surname = Console.ReadLine();
                Console.Write("Enter Address: ");
                address = Console.ReadLine();
                Console.Write("Enter Salary: ");
                salary = float.Parse(Console.ReadLine());
                Console.Write("Enter Employement Date: ");
                dateOfEmployement = Convert.ToDateTime(Console.ReadLine());
                Console.Write("#Enter username: ");
                username = Console.ReadLine();
                Console.Write("Enter password: ");
                password = Console.ReadLine();

                bool isAddDep = false;
                while (isAddDep == false)
                {
                    Console.WriteLine("Add Department:\nPress 1 for IICT.\nPress 2 for Buisness.\nPress 3 for Applied Science.\nPress 4 for Art.");
                    Console.Write("Your selection: ");
                    deptId = Convert.ToInt32(Console.ReadLine());
                    switch (deptId)
                    {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                            dept = db.Departments.Where(x => x.DepartmentId == deptId).FirstOrDefault();
                            isAddDep = true;
                            break;
                        default:
                            Console.WriteLine("You did not input a correct value");
                            Console.WriteLine("Press enter to reset");
                            isAddDep = false;
                            Console.ReadLine();
                            break;
                    }

                }

                db.Employees.Add(new SecretarialEmployee // Find a way on how to make sure that it adds a new employee but that goes accodring its type
                {
                    EmployeeName = name,
                    EmployeeSurname = surname,
                    Address = address,
                    Salary = salary,
                    DateOfCommencement = dateOfEmployement,
                    Username = username,
                    Password = password,
                    DepartmentId = dept
                });
                return db.SaveChanges();//Due to SaveChanges() returning a value of type int it is viaible to be used as a return type.
            }
        }
    }
}
