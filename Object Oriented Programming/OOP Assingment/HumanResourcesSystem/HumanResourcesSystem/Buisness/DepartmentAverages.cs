﻿using HumanResourcesSystem.Phyiscal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HumanResourcesSystem.Buisness
{
    /// <summary>
    /// This class handles the creating the salary averages of each department
    /// </summary>
    public class DepartmentAverages
    {
        static SystemDbContext db = new SystemDbContext();
        /// <summary>
        /// Displays the salaries average of all employees
        /// </summary>
        public static void AllSalaryAverage()
        {
            Console.Clear();

            var AllAvg =
                (from emp in db.Employees
                 select emp.Salary).Average();

            Console.WriteLine("This is the average salary of all employees in MCAST: " +AllAvg);
        }

        /// <summary>
        /// Returns the avg of each department
        /// </summary>
        public static void DepartmentAverage()
        {
            Console.Clear();

            var deptAvg =
                        from emp in db.Employees
                        group emp by emp.DepartmentId.DepartmentName into g //this places the groupby into 'g'
                        select new //this is like a container 
                        {
                            g.Key, //contains emp.DepartmentId.DepartmentName
                            avg = g.Average(i => i.Salary) //calculates the salary
                        };


            foreach (var item in deptAvg) //goes through newly created array and displays items
            {
                Console.WriteLine("Departments: {0}\n   Avg Salary: {1}\n",item.Key, item.avg);
            }
            

            /*T-SQL Equivalent
             * 
                SELECT dep.DepartmentName,
                       AVERAGE(emp.Salary)
                FROM Employees emp
                     JOIN Departments dep
                     ON (emp.DepartmentId = dep.DepartmentId)
                 Group By dep.DepartmentName
             */

        }
    }
}
