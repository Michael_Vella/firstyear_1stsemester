﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HumanResourcesSystem.Phyiscal;
using HumanResourcesSystem.Presentation;

namespace HumanResourcesSystem.Buisness
{
    /// <summary>
    /// This class handles the searching, adding, updating, and deleteing of a generic employee. It is the base class for all employeeBL types.
    /// </summary>
    class EmployeeBl
    {
        public static SystemDbContext db = new SystemDbContext();

        Employee emp = new Employee();
        AcademicEmployee aca = new AcademicEmployee();
        /// <summary>
        /// Returns all total amount of employees and their respective department
        /// </summary>
        public void EmployeeAmountPerDepart()
        {
            Console.Clear();

            var empDepCount =
                        from emp in db.Employees
                        group emp by emp.DepartmentId.DepartmentName into g //this places the groupby into 'g'
                        select new //this is like a container 
                        {
                            g.Key, //contains emp.DepartmentId.DepartmentName
                            count = g.Count() //calculates the salary
                        };


            foreach (var item in empDepCount) //goes through newly created array and displays items
            {
                Console.WriteLine("Departments: {0}\n Employee Count: {1}\n", item.Key, item.count);
            }

        }

        /// <summary>
        /// Searches and displays a employee
        /// </summary>
        public void SearchEmployee()
        {
            Console.Write("Enter username: ");
            string username = Console.ReadLine();
            emp = CheckIfEmpExists(username);

            if ( emp != null && emp is AcademicEmployee)
            {
                aca = (AcademicEmployee)emp;
                Console.WriteLine("Name: " + aca.EmployeeName + " " + aca.EmployeeSurname);
                Console.WriteLine("Address: " + aca.Address);
                Console.WriteLine("Grade: " + aca.Grade);
                Console.WriteLine("Salary: " + aca.Salary);
                Console.WriteLine("Date of Employement: " + aca.DateOfCommencement);
                Console.WriteLine("Department: " + GetEmpDepName());
                Console.WriteLine("Username: " + aca.Username);
                Console.WriteLine("Password: " + aca.Password);
             }
             else if (emp != null)
             {

                Console.WriteLine("Name: " + emp.EmployeeName + " " + emp.EmployeeSurname);
                Console.WriteLine("Address: " + emp.Address);
                Console.WriteLine("Salary: " + emp.Salary);
                Console.WriteLine("Date of Employement: " + emp.DateOfCommencement);
                Console.WriteLine("Department: " + GetEmpDepName() );
                Console.WriteLine("Username: " + emp.Username);
                Console.WriteLine("Password: " + emp.Password);
             }               
        }

        private string GetEmpDepName()
        {
            string depName;
            var empDepNameGroup =
                        from employee in db.Employees
                        group employee by employee.DepartmentId.DepartmentName into g //this places the groupby into 'g'
                        select new //this is like a container 
                        {
                            g.Key, //contains emp.DepartmentId.DepartmentName
                            count = g.Count() //calculates the salary
                        };
            foreach (var item in empDepNameGroup)
            {
                 return depName = item.Key.ToString();
            }
            return null;
        }

        /// <summary>
        /// Displays the Employees ordered (alphabetically) by their type
        /// </summary>
        public void GetEmpByType()
        {
            Console.Clear();

            var AcademicType = db.Employees.OfType<AcademicEmployee>();

            foreach (var empType in AcademicType)
            {
                Console.WriteLine("====================================================================================================================");
                Console.WriteLine("Employee Type:" +empType.GetType().Name.ToString());
                Console.WriteLine("Name: " + empType.EmployeeName + " " + empType.EmployeeSurname);
                Console.WriteLine("Address: " + empType.Address);
                Console.WriteLine("Grade: " + empType.Grade);
                Console.WriteLine("Salary: " + empType.Salary);
                Console.WriteLine("Department: " + GetEmpDepName());
                Console.WriteLine("Username: " + empType.Username);
                Console.WriteLine("Password: " + empType.Password);
                Console.WriteLine("====================================================================================================================");
            }

            var HrType = db.Employees.OfType<HumanResourceEmployee>();

            foreach (var empType in HrType)
            {
                Console.WriteLine("====================================================================================================================");
                Console.WriteLine("Employee Type:" + empType.GetType().Name.ToString());
                Console.WriteLine("Name: " + empType.EmployeeName + " " + empType.EmployeeSurname);
                Console.WriteLine("Address: " + empType.Address);
                Console.WriteLine("Salary: " + empType.Salary);
                Console.WriteLine("Department: " + GetEmpDepName());
                Console.WriteLine("Username: " + empType.Username);
                Console.WriteLine("Password: " + empType.Password);
                Console.WriteLine("====================================================================================================================");
            }

            var SecretrailType = db.Employees.OfType<SecretarialEmployee>();

            foreach (var empType in SecretrailType)
            {
                Console.WriteLine("====================================================================================================================");
                Console.WriteLine("Employee Type:" + empType.GetType().Name.ToString());
                Console.WriteLine("Name: " + empType.EmployeeName + " " + empType.EmployeeSurname);
                Console.WriteLine("Address: " + empType.Address);
                Console.WriteLine("Salary: " + empType.Salary);
                Console.WriteLine("Department: " + GetEmpDepName());
                Console.WriteLine("Username: " + empType.Username);
                Console.WriteLine("Password: " + empType.Password);
                Console.WriteLine("====================================================================================================================");
            }
        }


        /// <summary>
        /// Searches for an Employee using their username and password
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        public virtual void SearchEmployee(string username, string password)
        {
            if ( emp != null )
            {
                
                Console.Clear();
                Console.WriteLine("Name: " + emp.EmployeeName + " " + emp.EmployeeSurname);
                Console.WriteLine("Address: " + emp.Address);
                Console.WriteLine("Salary: " + emp.Salary);
                Console.WriteLine("Department: " + GetEmpDepName());
                Console.WriteLine("Date of Employeement: " + emp.DateOfCommencement.ToShortDateString());
                Console.WriteLine("Username: " + emp.Username);
                Console.WriteLine("Password: " + emp.Password);
                Console.WriteLine("\n\nPress enter ket to return to search menu....");
                Console.ReadLine();
              
            }
            else if (emp is AcademicEmployee && emp !=null)
            {
                aca = (AcademicEmployee)emp;
                Console.Clear();
                Console.WriteLine("Name: " + aca.EmployeeName + " " + aca.EmployeeSurname);
                Console.WriteLine("Address: " + aca.Address);
                Console.WriteLine("Salary: " + aca.Salary);
                Console.WriteLine("Grade: " + aca.Grade);
                Console.WriteLine("Department: " + GetEmpDepName());
                Console.WriteLine("Date of Employeement: " + emp.DateOfCommencement.ToShortDateString());
                Console.WriteLine("Username: " + emp.Username);
                Console.WriteLine("Password: " + emp.Password);
            } else
            {
                ////DO NOTHING
            }
        }
    
        /// <summary>
        /// Displays all the Employees
        /// </summary>
        public virtual void ListAllEmployees()
        {
            Console.Clear();
            foreach (var employee in db.Employees)
            {
                if (employee is AcademicEmployee)
                {
                    aca = (AcademicEmployee)employee;
                    Console.WriteLine("====================================================================================================================");
                    Console.WriteLine("This employee is an: " + employee.GetType().Name.ToString());
                    Console.WriteLine("Name: " + employee.EmployeeName + " " + employee.EmployeeSurname);
                    Console.WriteLine("Address: " + employee.Address);
                    Console.WriteLine("Grade: " + aca.Grade);
                    Console.WriteLine("Salary: " + employee.Salary);
                    Console.WriteLine("Department: " + GetEmpDepName());
                    Console.WriteLine("Date of Employeement: " + employee.DateOfCommencement.ToShortDateString());
                    Console.WriteLine("Username: " + employee.Username);
                    Console.WriteLine("Password: " + employee.Password);
                    Console.WriteLine("====================================================================================================================");
                    Console.WriteLine();
                }
                else
                {
                    Console.WriteLine("====================================================================================================================");
                    Console.WriteLine("This employee is an: " + employee.GetType().Name.ToString());
                    Console.WriteLine("Name: " + employee.EmployeeName + " " + employee.EmployeeSurname);
                    Console.WriteLine("Address: " + employee.Address);
                    Console.WriteLine("Salary: " + employee.Salary);
                    Console.WriteLine("Department: " + GetEmpDepName());
                    Console.WriteLine("Date of Employeement: " + employee.DateOfCommencement.ToShortDateString());
                    Console.WriteLine("Username: " + employee.Username);
                    Console.WriteLine("Password: " + employee.Password);
                    Console.WriteLine("====================================================================================================================");
                    Console.WriteLine();
                }
               
            }
            //Counts all of the different types of employees 
            int totalAcademic = db.Employees.OfType<AcademicEmployee>().Count();
            int totalHr = db.Employees.OfType<HumanResourceEmployee>().Count();
            int totalSecretary = db.Employees.OfType<SecretarialEmployee>().Count();
            int totalAdmin = db.Employees.OfType<AdministrativeEmployee>().Count();

            Console.WriteLine("Total Academic Employees: " + totalAcademic);
            Console.WriteLine("Total Administration Employees: " + totalAdmin);
            Console.WriteLine("Total HR Employees: " + totalHr);
            Console.WriteLine("Total Secretary Employees: " + totalSecretary);
            Console.WriteLine("Total Amount of Employees: " + db.Employees.Count());
        }

        /// <summary>
        /// Adds Employee
        /// </summary>
        /// <returns></returns>
        public virtual int AddEmployee()
        {
            string name = "";
            string surname = "";
            string address = "";
            float salary = 0;
            DateTime dateOfEmployement = DateTime.Today;
            string username = "";
            string password = "";
            int deptId = 0;

            Console.Clear();
            Console.Write("Please enter the intened username: ");
            username = Console.ReadLine();
            
            if (CheckIfEmpExists(username) != null)
            {
                Console.WriteLine("Employee Exists...\nPress any key to return to Add Menu");
                Console.ReadKey();
                return -1;// Employee already exists
            }
            else
            {
                Department dept = new Department();

                Console.WriteLine("Employee does not exist. \nAdd employee details below.");
                Console.Write("\nEnter Name: ");
                name = Console.ReadLine();
                Console.Write("\nEnter Surname: ");
                surname = Console.ReadLine();
                Console.Write("\nEnter Address: ");
                address = Console.ReadLine();
                Console.Write("Enter Salary: ");

                salary = float.Parse(Console.ReadLine());
                Console.Write("\nEnter Employement Date: ");
                dateOfEmployement = Convert.ToDateTime(Console.ReadLine());
                Console.Write("\nEnter username: ");
                username = Console.ReadLine();
                Console.Write("\nEnter password: ");
                password = Console.ReadLine();

                bool isAddDep = false;
                while (isAddDep == false)
                {
                    Console.WriteLine("\nUpdate Department:\nPress 1 for IICT.\nPress 2 for Buisness.\nPress 3 for Applied Science.\nPress 4 for Art.\n");
                    Console.Write("Your selection: ");
                    deptId = Convert.ToInt32(Console.ReadLine());
                    switch (deptId)
                    {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                            dept = db.Departments.Where(x => x.DepartmentId == deptId).FirstOrDefault();
                            isAddDep = true;
                            break;
                        default:
                            Console.WriteLine("You did not input a correct value");
                            Console.WriteLine("Press enter to reset");
                            isAddDep = false;
                            Console.ReadLine();
                            break;
                    }
                }

                db.Employees.Add(new Employee 
                {   EmployeeName = name,
                    EmployeeSurname = surname,
                    Address = address,
                    Salary = salary,
                    DateOfCommencement = dateOfEmployement,
                    Username = username,
                    Password = password,
                    DepartmentId = dept
                });
                return db.SaveChanges();//Due to SaveChanges() returning a value of type int it is viaible to be used as a return type.
            }
        }

        /// <summary>
        /// Updates employees
        /// </summary>
        /// <returns></returns>
        public virtual int UpdateEmployee()
        {
            Console.Clear();
            string name;
            string surname;
            string address;
            float salary;
            DateTime dateOfEmployement;
            string username;
            string password;
            byte payGrade = 0;
            int deptId = 0 ;
            Console.Write("Enter user name: ");
            username = Console.ReadLine();
            emp = CheckIfEmpExists(username);
            if ( emp != null)//if it exists
            {

                Department dept = new Department();

                Console.WriteLine("Employee does exist.\nUpdate employee details below.");
                Console.Write("\nEnter Name: ");
                name = Console.ReadLine();
                Console.Write("\nEnter Surname: ");
                surname = Console.ReadLine();
                Console.Write("\nEnter Address: ");
                address = Console.ReadLine();
                Console.Write("\nEnter Salary: ");
                salary = float.Parse(Console.ReadLine());
                Console.Write("\nEnter Employement Date: ");
                dateOfEmployement = Convert.ToDateTime(Console.ReadLine());
                Console.Write("\nEnter username: ");
                username = Console.ReadLine();
                Console.Write("\nEnter password: ");
                password = Console.ReadLine();

                bool isAddDep = false;
                while (isAddDep == false)
                {
                    Console.WriteLine("\nUpdate Department:\nPress 1 for IICT.\nPress 2 for Buisness.\nPress 3 for Applied Science.\nPress 4 for Art.\n");
                    Console.Write("Your selection: ");
                    deptId = Convert.ToInt32(Console.ReadLine());
                    switch (deptId)
                    {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                            dept = db.Departments.Where(x => x.DepartmentId == deptId).FirstOrDefault();
                            isAddDep = true;
                            break;
                        default:
                            Console.WriteLine("You did not input a correct value");
                            isAddDep = false;
                            break;
                    }
                }
                emp.EmployeeName = name;
                emp.EmployeeSurname = surname;
                emp.Address = address;
                emp.Salary = salary;
                emp.DateOfCommencement = dateOfEmployement;
                emp.Username = username;
                emp.Password = password;
                emp.DepartmentId = dept;
                return db.SaveChanges();
            }
            else if (emp !=null && emp is AcademicEmployee)
            {
                Department dept = new Department();
                AcademicEmployee academic = new AcademicEmployee();

                Console.Write("\nEnter Name: ");
                name = Console.ReadLine();
                Console.Write("\nEnter Surname: ");
                surname = Console.ReadLine();
                Console.Write("\nEnter Address: ");
                address = Console.ReadLine();
                Console.Write("\nEnter Salary: ");
                salary = float.Parse(Console.ReadLine());
                Console.Write("\nEnter Employement Date: ");
                dateOfEmployement = Convert.ToDateTime(Console.ReadLine());
                Console.Write("\nEnter username: ");
                username = Console.ReadLine();
                Console.Write("\nEnter password: ");
                password = Console.ReadLine();

                bool isProper = false;
                    while (isProper == false)
                    {
                        Console.Write("Enter Grade: ");
                        payGrade = Convert.ToByte(Console.ReadLine());
                        switch (payGrade)
                        {
                            case 6:
                            case 7:
                            case 8:
                                isProper = true;
                                break;
                            default:
                                isProper = false;
                                Console.WriteLine("Entered Grade is wrong.\n Press Enter to reset.");
                                break;
                        }
                    }
                    
                    Console.Write("\nEnter Employement Date: ");
                    dateOfEmployement = Convert.ToDateTime(Console.ReadLine());
                    Console.Write("\nEnter username: ");
                    username = Console.ReadLine();
                    Console.Write("\nEnter password: ");
                    password = Console.ReadLine();

                    bool isAddDep = false;
                    while (isAddDep == false)
                    {
                    Console.WriteLine("\nUpdate Department:\nPress 1 for IICT.\nPress 2 for Buisness.\nPress 3 for Applied Science.\nPress 4 for Art.\n");
                    Console.Write("Your selection: ");
                        deptId = Convert.ToInt32(Console.ReadLine());
                        switch (deptId)
                        {
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                                dept = db.Departments.Where(x => x.DepartmentId == deptId).FirstOrDefault();
                                isAddDep = true;
                                break;
                            default:
                                Console.WriteLine("You did not input a correct value");
                                isAddDep = false;
                                break;
                        }
                    }
                    academic.EmployeeName = name;
                    academic.EmployeeSurname = surname;
                    academic.Address = address;
                    academic.Salary = salary;
                    academic.Grade = payGrade;
                    academic.DateOfCommencement = dateOfEmployement;
                    academic.Username = username;
                    academic.Password = password;
                    academic.DepartmentId = dept;
                    return db.SaveChanges();
                }
            else
            {
                return -1;//IE entry doesnt exist 
            }
        }

        /// <summary>
        /// Check if employee exists
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public Employee CheckIfEmpExists(string username)
        {
            Employee employee = db.Employees.Where(x => x.Username == username).FirstOrDefault();
            return employee;
        }
        
        /// <summary>
        /// Checks if employee exists
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public Employee CheckIfEmpExists(string username, string password)
        {
            Employee employee = db.Employees.Where(x => x.Username == username && x.Password == password).FirstOrDefault();
            return employee;
        }

        /// <summary>
        /// Delete employee
        /// </summary>
        /// <returns></returns>
        public virtual int DeleteEmployee(string username)
        {
            Employee employee = db.Employees.Where(x => x.Username == username).FirstOrDefault();
            if (employee !=null)
            {   
                db.Employees.Remove(employee);
                Console.WriteLine("User successfully removed");
                return db.SaveChanges();
            }
            else
            {
                Console.WriteLine("You have enter the wrong username or username does not exist.");
                return -1;
            }
        }
    }
}
