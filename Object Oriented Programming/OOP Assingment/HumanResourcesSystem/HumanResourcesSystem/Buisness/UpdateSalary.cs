﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HumanResourcesSystem.Phyiscal;
using HumanResourcesSystem.Presentation;

/// <summary>
/// This class is to handled the updating of the salary of all employees on a 10(ten) year basis. This update is meant to happen once a year.
/// </summary>
namespace HumanResourcesSystem.Buisness
{
    class UpdateSalary
    {
        static SystemDbContext db = new SystemDbContext();
        /// <summary>
        /// Calculates the normal yearly salary increase for employees
        /// </summary>
        public static void NonAcademicSalaryUpdate(Employee emp)
        {
            float oldSalary = emp.Salary; //Used for display purposes only.
            float updatedSalary;
            float salaryDifference;
            updatedSalary = YearlySalaryIncrease(emp);
            salaryDifference = updatedSalary - oldSalary;
            Console.WriteLine("Previous salary: " + oldSalary);
            Console.WriteLine("Added amount: " + salaryDifference);
            Console.WriteLine("Updated Salary: " + updatedSalary);
            emp.Salary = updatedSalary;
            db.SaveChanges();
        }

        /// <summary>
        /// Decides which Salary update method to use based on if the user is academic or not.
        /// </summary>
        public static void SalaryUpdateDecider()
        {
            Console.Write("Enter Username of Employee: ");
            string user = Console.ReadLine();
            EmployeeBl employee = new EmployeeBl();
            Employee emp = employee.CheckIfEmpExists(user);
            if (emp != null && !(emp is AcademicEmployee) )
            {
                NonAcademicSalaryUpdate(emp);
            }
            else if (emp != null && emp is AcademicEmployee)
            {
                AcademicSalaryUpdate(emp);
            }
            else
            {
                Console.WriteLine("Inputted User does not exist");
            }
        }

        public static void AcademicSalaryUpdate (Employee emp)
        {
            float oldSalary = emp.Salary; //Used for display purposes only.
            float newSalary;
            float salaryDifference;
            float academicIncrease;

            newSalary = YearlySalaryIncrease(emp);
            salaryDifference = newSalary - oldSalary;
            //displaying new and old salary pre academic increace 
            Console.WriteLine("Previous salary: " + oldSalary);
            Console.WriteLine("Added amount: " + salaryDifference);
            Console.WriteLine("New Salary: " + newSalary);

            AcademicEmployee aca = new AcademicEmployee();
            aca = (AcademicEmployee)emp;
            if (aca.Grade >= 8) //if Grade 8 or more
            {
                salaryDifference = 0; //just incase
                academicIncrease = YearlySalaryIncrease(aca);
                salaryDifference = academicIncrease - newSalary;
                Console.WriteLine("Newly updated salary: " +newSalary);
                Console.WriteLine("Added Ammount: " + salaryDifference);
                Console.WriteLine("Academic Bonous: " + academicIncrease);
                aca.Salary = academicIncrease;
                db.SaveChanges();
            }
            else if (aca.Grade == 7)
            {
                float baseIncrease = 250;
                for (int i = 0; i <= CalculateYearDiff(emp); i++)
                {
                    baseIncrease = baseIncrease + 50;
                }

                academicIncrease = newSalary + baseIncrease;
                Console.WriteLine("Newly updated salary: " + newSalary);
                Console.WriteLine("Added Ammount: " + baseIncrease);
                Console.WriteLine("Academic Bonous: " + academicIncrease);
                aca.Salary = academicIncrease;
                db.SaveChanges();
            }
            else if (aca.Grade <= 6)
            {
                float baseIncrease = 350;
                for (int i = 0; i <= CalculateYearDiff(emp); i++)
                {
                    baseIncrease = baseIncrease + 50;
                }

                academicIncrease = newSalary + baseIncrease;
                Console.WriteLine("Newly updated salary: " + newSalary);
                Console.WriteLine("Added Ammount: " + baseIncrease);
                Console.WriteLine("Academic Bonous: " + academicIncrease);
                aca.Salary = academicIncrease;
                db.SaveChanges();
            }
            else
            {
                //do nothing
            }
        }

        /// <summary>
        /// Calculates yearly increase in salary. 
        /// </summary>
        public static float YearlySalaryIncrease(Employee emp)
        {
            float newSalary;
            float updatedSalary;

            int totalYearDiff = CalculateYearDiff(emp);
            if (totalYearDiff >= 1 && totalYearDiff <= 5) //for the first 5 years the salary increase is 250 per year
            {
                newSalary = 250;
                updatedSalary = emp.Salary + newSalary;
                return updatedSalary;
            }
            else if (totalYearDiff >= 6 && totalYearDiff <= 10)
            {
                newSalary = 400;
                updatedSalary = emp.Salary + newSalary;
                return updatedSalary;
            }
            else
            {
                Console.WriteLine("Employee is no longer eldigible for wage increase.");
                return 0;
            }
        }

        /// <summary>
        /// Calculates total year difference 
        /// </summary>
        /// <param name="emp"></param>
        /// <returns>totalYearDiff</returns>
        public static int CalculateYearDiff(Employee emp)
        {
            DateTime today = DateTime.Today;
            int totalYearDiff = today.Year - emp.DateOfCommencement.Year;
            return totalYearDiff;
        }
    }     
}