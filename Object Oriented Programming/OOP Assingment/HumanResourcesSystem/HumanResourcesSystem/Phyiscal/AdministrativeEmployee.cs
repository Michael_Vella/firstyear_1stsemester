﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HumanResourcesSystem.Phyiscal
{
    public class AdministrativeEmployee : Employee
    {
        public int AdministrativeEmployeeId { get; set; }
    }
}
