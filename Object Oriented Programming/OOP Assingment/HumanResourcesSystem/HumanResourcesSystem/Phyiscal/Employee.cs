﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HumanResourcesSystem.Phyiscal
{
    public class Employee
    {
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeSurname { get; set; }
        public string Address { get; set; }
        public float Salary { get; set; }
        public DateTime DateOfCommencement { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public Department DepartmentId { get; set; }
       
    }
}
