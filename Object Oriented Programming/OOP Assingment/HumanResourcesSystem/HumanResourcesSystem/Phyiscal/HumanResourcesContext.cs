﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HumanResourcesSystem.Phyiscal
{
    public class SystenDbContext : DbContext
    {
        //Constructor
        public SystenDbContext() : base ("HumanResourcesDatabase")
        {
        
        }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Employee> Employees { get; set; }
    }
}
