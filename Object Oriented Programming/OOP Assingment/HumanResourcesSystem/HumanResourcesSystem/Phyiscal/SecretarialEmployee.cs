﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HumanResourcesSystem.Phyiscal
{
    class SecretarialEmployee : AdministrativeEmployee
    {
        public int SecretarialEmployeeId { get; set; }
    }
}
