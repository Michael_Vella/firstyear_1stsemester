﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HumanResourcesSystem.Phyiscal
{
    public class SystemDbContext : DbContext
    {
        //Constructor
        public SystemDbContext() : base ("HR_Database")
        {
        
        }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Employee> Employees { get; set; }
    }
}
