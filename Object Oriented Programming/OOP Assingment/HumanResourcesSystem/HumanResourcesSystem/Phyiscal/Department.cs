﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HumanResourcesSystem.Phyiscal
{
    public class Department
    {
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set;}
        public string Location { get; set; }
        public ICollection<Employee> employees { get; set; } 
    }
}
