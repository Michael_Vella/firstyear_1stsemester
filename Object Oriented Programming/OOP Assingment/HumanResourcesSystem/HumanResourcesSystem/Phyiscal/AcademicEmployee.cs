﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HumanResourcesSystem.Phyiscal
{
    public class AcademicEmployee : Employee
    {
        public int AcademicEmployeeId { get; set; }
        public double Grade { get; set; }
    }
}
